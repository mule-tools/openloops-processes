# McMule OpenLoops process libraries

[McMule](https://gitlab.com/mule-tools/mcmule) heavily relies on
OpenLoops for real-virtual matrix elements. The relevant process
libraries were kindly provided by [Max Zoller](https://gitlab.com/zoller).
Here, we collect we mirror the [official repository](https://www.physik.uzh.ch/data/openloops/repositories/McMule/)
for posterity and to avoid breakage.
