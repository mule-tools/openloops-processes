(* fermion loops are excluded *)

OpenLoopsModel = "SM";
Approximation = "nofermionloops";
SelectInterference = {gQCD -> 0};
OnTheFlyMode = 3;


lepqed01 = {
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}],
  SetExternalSubtrees -> {3},
  SelectLoopDiagrams -> ( Not[ContainsFermionLoop[##] && NLegsOnLoop[2][##]] && ParticlesInLoop[V[1]][##] &)
};

lepqed02 = {
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}],
  SetExternalSubtrees -> {3},
  SelectLoopDiagrams -> ( Not[ContainsFermionLoop[##] && NLegsOnLoop[2][##]] && ParticlesInLoop[V[1]][##] &)
};


AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1]}, Sequence @@ lepqed01];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1], V[1]}, Sequence @@ lepqed01];
AddReal[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1],V[1],V[1]}, Sequence @@ lepqed01];
