SelectInterference = {qQCD -> 0};

UnitaryGauge = False;
noQCD = True;

(* ew (QED=0) or pure qed (QED=2) or pure weak (QED=3, including Higgs) corrections, no tau and mu *)
(* ew includes photon self-energies *)
(* qed excludes photon self-energies *)
(* other fermion loop diagrams are included, e.g. light-by-light boxes *)

lepew01 = {
  QED -> 0,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

lepqed01 = {
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

lepweak01 = {
  QED -> 3,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

(* eexeex *)    AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepew01];
(* eexeexa *)   AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepew01];
(* eexeexaa *)  AddReal[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepew01];

(* eexeex *)    AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepqed01];
(* eexeexa *)   AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepqed01];
(* eexeexaa *)  AddReal[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepqed01];

(* eexeex *)    AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepweak01];
(* eexeexa *)   AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepweak01];
(* eexeexaa *)  AddReal[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepweak01];
