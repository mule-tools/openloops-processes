

SelectInterference = {qQCD -> 0};

qed01 = {
  InsertFieldsOptions -> {Restrictions -> {ExcludeParticles -> {V[2 | 3 | 4 | 5],U[__],S[1 | 2 | 3], F[1, __], F[2, {2}], F[2, {3}], F[4, __], F[3, __]}}},
  SelectLoopDiagrams -> ( ContainsFermionLoop[##] && NLegsOnLoop[4][##] && ParticlesOnLoop[V[1], 4][##] &), 
  QED -> 2,
  SetParameters -> JoinOptions[{ME -> ME, nf -> 0, nfl -> 1}]
};

qed02 = {
  InsertFieldsOptions -> {Restrictions -> {ExcludeParticles -> {V[2 | 3 | 4 | 5],U[__],S[1 | 2 | 3], F[1, __], F[2, {3}], F[4, __], F[3, __]}}},
  SelectLoopDiagrams -> ( ContainsFermionLoop[##] && NLegsOnLoop[4][##] && ParticlesOnLoop[V[1], 4][##] &), 
  QED -> 2,
  SetParameters -> JoinOptions[{ME -> ME, MM -> MM, nf -> 0, nfl -> 2}]
};


Approximation = "lblbox"

(* eexaaa *)   AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1], V[1], V[1]}, Sequence @@ qed01];
(* eexaaa *)   AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1], V[1], V[1]}, Sequence @@ qed02];
