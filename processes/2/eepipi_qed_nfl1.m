(* photon self-energies are excluded *)

OpenLoopsModel = "SMpions";

QED = 2;
SetParameters = JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}];

(* Approximation: L<power of Q_Pi in loop dia><power of Q_e in loop dia>T<power of Q_Pi in tree dia><power of Q_e in tree dia>N<number of loop propagators> (D<number of disperon loop propagators if not 0>) *)
(* e = pure electron loop, s = pure scalar loop *)

qed01 = {
  SelectInterference -> {gQCD -> 0},
  SelectLoopDiagrams -> ( Not[(ContainsFermionLoop[##]||ContainsScalarLoop[##]) && NLegsOnLoop[2][##]] &&                   (* No photon self-energy *)
                          Not[ParticlesInLoop[V[1]][##] && ParticlesInLoop[S[7]][##] && NLegsOnLoop[2][##]] &&              (* No pion self-energy *)
                          Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] &),
  Approximation      -> ""
};

(* ***************************************************************************************************** *)
(* Approximations for e+e- ->pi-pi+                                                                      *)
(* ***************************************************************************************************** *)

qed01L31T11N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##]  && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L31T11N3"
};
qed01L31T11N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##]  && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L31T11N3D1"
};


qed01L13T11N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] &),
  Approximation      -> "L13T11N3"
};
qed01L13T11N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] &),
  Approximation      -> "L13T11N3D1"
};


qed01L22T11N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L22T11N4"
};
qed01L22T11N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L22T11N4D1"
};
qed01L22T11N4D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[6]][##] && ParticlesInLoop[V[7]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L22T11N4D2"
};

(* ***************************************************************************************************** *)
(* Approximations for e+e- ->pi-pi+ a                                                                    *)
(* ***************************************************************************************************** *)

qed01a = {
  SelectInterference -> {gQCD -> 0},
  SelectLoopDiagrams -> ( Not[(ContainsFermionLoop[##]||ContainsScalarLoop[##]) && NLegsOnLoop[2][##]] &&                   (* No photon self-energy *)
                          Not[ParticlesInLoop[V[1]][##] && ParticlesInLoop[S[7]][##] && NLegsOnLoop[2][##]] &&              (* No pion self-energy *)
                          Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] &),
  Approximation      -> ""
};

(*
qed01L41T21N2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 6, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[2][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7]],2][##] &),
  Approximation      -> "L41T21N2"
};
qed01L41T12N2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[2][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7]],2][##] &),
  Approximation      -> "L41T12N2"
};
*)

qed01L14T21N2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[2][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],2][##] &),
  Approximation      -> "L14T21N2"
};
qed01L14T12N2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 6},
  SelectLoopDiagrams -> ( NLegsOnLoop[2][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],2][##] &),
  Approximation      -> "L14T12N2"
};


qed01L41T21N3s = {
  SelectInterference -> {gQCD -> 0, QPi -> 6, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T21N3s"
};
qed01L41T12N3s = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T12N3s"
};

qed01L14T21N3e = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L14T21N3e"
};
qed01L14T12N3e = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 6},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L14T12N3e"
};

qed01L41T21N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 6, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T21N3"
};
qed01L41T12N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T12N3"
};

qed01L32T21N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N3"
};
qed01L32T12N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N3"
};

qed01L23T21N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],0][##]  && NFieldPropagators[S[7],1][##] &),
  Approximation      -> "L23T21N3"
};
qed01L23T12N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],0][##]  && NFieldPropagators[S[7],1][##] &),
  Approximation      -> "L23T12N3"
};

qed01L14T21N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],1][##]  && NFieldPropagators[S[7],0][##] &),
  Approximation      -> "L14T21N3"
};
qed01L14T12N3 = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 6},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],1][##]  && NFieldPropagators[S[7],0][##] &),
  Approximation      -> "L14T12N3"
};

qed01L41T21N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 6, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T21N3D1"
};
qed01L41T12N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T12N3D1"
};

qed01L32T21N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N3D1"
};
qed01L32T12N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N3D1"
};

qed01L23T21N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],0][##]  && NFieldPropagators[S[7],1][##] &),
  Approximation      -> "L23T21N3D1"
};
qed01L23T12N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],0][##]  && NFieldPropagators[S[7],1][##] &),
  Approximation      -> "L23T12N3D1"
};

qed01L14T21N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],1][##]  && NFieldPropagators[S[7],0][##] &),
  Approximation      -> "L14T21N3D1"
};
qed01L14T12N3D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 6},
  SelectLoopDiagrams -> ( NLegsOnLoop[3][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[F[2,{1}],1][##]  && NFieldPropagators[S[7],0][##] &),
  Approximation      -> "L14T12N3D1"
};






qed01L23T21N5 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T21N5"
};
qed01L23T12N5 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T12N5"
};
qed01L32T21N5 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N5"
};
qed01L32T12N5 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N5"
};

qed01L23T21N5D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T21N5D1"
};
qed01L23T12N5D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T12N5D1"
};
qed01L32T21N5D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N5D1"
};
qed01L32T12N5D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N5D1"
};

qed01L23T21N5D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T21N5D2"
};
qed01L23T12N5D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T12N5D2"
};
qed01L32T21N5D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N5D2"
};
qed01L32T12N5D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[5][##] && ParticlesInLoop[V[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N5D2"
};





qed01L23T21N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[F[2,{1}]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T21N4"
};
qed01L23T12N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[F[2,{1}]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T12N4"
};
qed01L32T21N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[S[7]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N4"
};
qed01L32T12N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[S[7]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N4"
};


qed01L14T21N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[F[2,{1}]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L14T21N4"
};
qed01L14T12N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 6},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[F[2,{1}]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L14T12N4"
};
qed01L41T21N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 6, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[S[7]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T21N4"
};
qed01L41T12N4 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[S[7]][##] && Not[ParticlesInLoop[V[6|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T12N4"
};



qed01L23T21N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[F[2,{1}]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T21N4D1"
};
qed01L23T12N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[F[2,{1}]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T12N4D1"
};
qed01L32T21N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[S[7]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N4D1"
};
qed01L32T12N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[1]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[S[7]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N4D1"
};

qed01L23T21N4D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[6]][##] && ParticlesInLoop[V[7]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[F[2,{1}]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T21N4D2"
};
qed01L23T12N4D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[6]][##] && ParticlesInLoop[V[7]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[F[2,{1}]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L23T12N4D2"
};
qed01L32T21N4D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[6]][##] && ParticlesInLoop[V[7]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[S[7]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T21N4D2"
};
qed01L32T12N4D2 = {
  SelectInterference -> {gQCD -> 0, QPi -> 4, Qel -> 4},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[V[6]][##] && ParticlesInLoop[V[7]][##] && Not[ParticlesInLoop[V[1]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOutsideLoop[S[7]][##] &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L32T12N4D2"
};


qed01L14T21N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 3, Qel -> 5},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L14T21N4D1"
};
qed01L14T12N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 2, Qel -> 6},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[F[2,{1}]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] && Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L14T12N4D1"
};
qed01L41T21N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 6, Qel -> 2},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] &&
  Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],1][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T21N4D1"
};
qed01L41T12N4D1 = {
  SelectInterference -> {gQCD -> 0, QPi -> 5, Qel -> 3},
  SelectLoopDiagrams -> ( NLegsOnLoop[4][##] && ParticlesInLoop[S[7]][##] && ParticlesInLoop[V[6]][##] && Not[ParticlesInLoop[V[1|7]][##]] &&
  Not[ParticlesOutsideLoop[V[6|7]][##]] && ParticlesOnLoop[V[1],2][##]  &) ,
  SelectTreeDiagrams -> ( NFieldPropagators[V[6|7],0][##] && NFieldPropagators[S[7],0][##] &),
  SelectCTDiagrams   -> ( False &),
  Approximation      -> "L41T12N4D1"
};

(* ***************************************************************************************************** *)

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01];


AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01L31T11N3];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01L31T11N3D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01L13T11N3];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01L13T11N3D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01L22T11N4];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01L22T11N4D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7]}, Sequence @@ qed01L22T11N4D2];

(* ***************************************************************************************************** *)

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01];


(*
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T21N2];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T12N2];
*)

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T21N2];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T12N2];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T21N3s];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T12N3s];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T21N3e];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T12N3e];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T21N3];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T12N3];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N3];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N3];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N3];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N3];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T21N3];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T12N3];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T21N3D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T12N3D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N3D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N3D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N3D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N3D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T21N3D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T12N3D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N4];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N4];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N4];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N4];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N4D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N4D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N4D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N4D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N4D2];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N4D2];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N4D2];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N4D2];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T21N4];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T12N4];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T21N4];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T12N4];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T21N4D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L41T12N4D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T21N4D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L14T12N4D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N5];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N5];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N5];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N5];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N5D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N5D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N5D1];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N5D1];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T21N5D2];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L32T12N5D2];

AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T21N5D2];
AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {S[7], -S[7], V[1]}, Sequence @@ qed01L23T12N5D2];


