module ol_external_eeee_lew_eeexexa_1
  implicit none
  integer :: dummy_counter
  ! Permutation and inverse permutation of external particles
  integer, save :: external_perm_eeee_lew_eeexexa_1(5) = &
                     [ (dummy_counter, dummy_counter = 1, 5) ]
  integer, save :: external_perm_inv_eeee_lew_eeexexa_1(5) = &
                     [ (dummy_counter, dummy_counter = 1, 5) ]
  integer, save :: extcomb_perm_eeee_lew_eeexexa_1(0:16) = &
                     [ (dummy_counter, dummy_counter = 0, 16) ]
  ! Particle types (mapping of fields to integers is not fixed!)
  integer, save :: particle_types_eeee_lew_eeexexa_1(5) = &
                     [ 1, 1, 2, 2, 3 ]
  ! Colour and helicity average factors per particle
  integer, save :: average_factors_eeee_lew_eeexexa_1(5) = &
                     [ 2, 2, 2, 2, 2 ]
  ! Average factor; initialised to the identity permutation
  integer, save :: average_factor_eeee_lew_eeexexa_1 = &
                     8
  integer, save :: channel_number_eeee_lew_eeexexa_1 = -1
  ! external particle helicities
  logical, save :: hel_not_initialised = .true.
  integer, save :: H(5,32) ! H(i,la) = helicity of particle i in configuration la
  integer, save :: H_HC(32,5)
  integer, save :: POLSEL(5) = 0
  integer, save :: photonid_eeee_lew_eeexexa_1(5) = 0

  contains

  subroutine n_external(n) &
      & bind(c,name="ol_f_n_external_eeee_lew_eeexexa_1")
    ! Return the number of external particles
    implicit none
    integer, intent(out) :: n
    n = 5
  end subroutine n_external


  subroutine n_external_c(n) &
      & bind(c,name="ol_n_external_eeee_lew_eeexexa_1")
    use, intrinsic :: iso_c_binding, only: c_int
    implicit none
    integer(c_int) :: n
    n = 5
  end subroutine n_external_c

  subroutine averagefactor_c(avgf) &
      & bind(c,name="ol_averagefactor_eeee_lew_eeexexa_1")
    use KIND_TYPES, only: DREALKIND
    implicit none
    real(DREALKIND), intent(out) :: avgf
    avgf = average_factor_eeee_lew_eeexexa_1
  end subroutine averagefactor_c

  subroutine set_permutation(perm) &
      & bind(c,name="ol_f_set_permutation_eeee_lew_eeexexa_1")
    use ol_parameters_decl_/**/DREALKIND, only: out_symmetry_on
    use ol_external_decl_/**/DREALKIND, only: n_scatt
    use ol_generic, only: factorial
    implicit none
    integer, intent(in) :: perm(5)
    integer :: i, j, ii, jj
    integer :: particle_types_perm_eeee_lew_eeexexa_1(5)
    external_perm_eeee_lew_eeexexa_1 = perm
    do i = 1, 5
      external_perm_inv_eeee_lew_eeexexa_1( &
        external_perm_eeee_lew_eeexexa_1(i)) = i
      particle_types_perm_eeee_lew_eeexexa_1(i) = &
        particle_types_eeee_lew_eeexexa_1( &
        external_perm_eeee_lew_eeexexa_1(i))
    end do
    do i = 1, 5
      do j = 1, i
        if (external_perm_eeee_lew_eeexexa_1(i) >= &
          external_perm_eeee_lew_eeexexa_1(j)) then
          ii = external_perm_eeee_lew_eeexexa_1(i)
          jj = external_perm_eeee_lew_eeexexa_1(j)
        else
          ii = external_perm_eeee_lew_eeexexa_1(j)
          jj = external_perm_eeee_lew_eeexexa_1(i)
        end if
        extcomb_perm_eeee_lew_eeexexa_1((i*(i-1))/2 + j) = (ii*(ii-1))/2 + jj
      end do
    end do
    ! Colour and helicity average factor
    average_factor_eeee_lew_eeexexa_1 = 1
    do i = 1, n_scatt
      average_factor_eeee_lew_eeexexa_1 = &
        average_factor_eeee_lew_eeexexa_1 &
        * average_factors_eeee_lew_eeexexa_1( &
        external_perm_eeee_lew_eeexexa_1(i))
    end do
    ! Symmetry factor for outgoing particles
    if (out_symmetry_on /= 0) then
      do i = 1, 5
        average_factor_eeee_lew_eeexexa_1 = &
          average_factor_eeee_lew_eeexexa_1 &
          * factorial(count(particle_types_perm_eeee_lew_eeexexa_1(n_scatt+1:5) == i))
      end do
    end if
  end subroutine set_permutation


  subroutine set_permutation_c(perm) &
      & bind(c,name="ol_set_permutation_eeee_lew_eeexexa_1")
    use, intrinsic :: iso_c_binding, only: c_int
    implicit none
    integer(c_int), intent(in) :: perm(5)
    integer :: f_perm(5)
    f_perm = perm
    call set_permutation(f_perm)
  end subroutine set_permutation_c


  subroutine get_last_perm(perm) &
      & bind(c,name="ol_f_get_last_perm_eeee_lew_eeexexa_1")
    integer, intent(out) :: perm(5)
    perm = external_perm_eeee_lew_eeexexa_1
  end subroutine get_last_perm


  subroutine get_masses(m_ex) &
      & bind(c,name="ol_f_get_masses_eeee_lew_eeexexa_1")
    ! Return the masses of the external particles in the current permutation.
    use KIND_TYPES, only: DREALKIND
    use ol_parameters_decl_/**/DREALKIND
    implicit none
    real(DREALKIND), intent(out) :: m_ex(5)
    integer        :: i
    real(DREALKIND) :: m_ex_orig(5)
    ! External particle masses for in the identity permutation
    m_ex_orig = [ rME_unscaled, rME_unscaled, rME_unscaled, rME_unscaled, rZERO ]
    do i = 1, 5
      m_ex(i) = m_ex_orig(external_perm_eeee_lew_eeexexa_1(i))
    end do
  end subroutine get_masses


  subroutine get_masses_c(m_ex) &
      & bind(c,name="ol_get_masses_eeee_lew_eeexexa_1")
    use KIND_TYPES, only: DREALKIND
    use, intrinsic :: iso_c_binding, only: c_double
    implicit none
    real(c_double), intent(out) :: m_ex(5)
    real(DREALKIND) :: f_m_ex(5)
    call get_masses(f_m_ex)
    m_ex = f_m_ex
  end subroutine get_masses_c


  subroutine rambo(sqrt_s, p_rambo) &
      & bind(c,name="ol_f_rambo_eeee_lew_eeexexa_1")
    use KIND_TYPES, only: DREALKIND
    use ol_kinematics_/**/DREALKIND, only: rambo_generic => rambo
    implicit none
    real(DREALKIND), intent(in) :: sqrt_s
    real(DREALKIND), intent(out) :: p_rambo(0:3,5)
    real(DREALKIND) :: m_ex(5)
    call get_masses(m_ex)
    call rambo_generic(sqrt_s, m_ex, p_rambo)
  end subroutine rambo


  subroutine rambo_c(sqrt_s, p_rambo) &
      & bind(c,name="ol_rambo_eeee_lew_eeexexa_1")
    use KIND_TYPES, only: DREALKIND
    use, intrinsic :: iso_c_binding, only: c_double
    implicit none
    real(c_double), intent(in) :: sqrt_s
    real(c_double), intent(out) :: p_rambo(0:3,5)
    real(DREALKIND) :: f_sqrt_s
    real(DREALKIND) :: f_p_rambo(0:3,5)
    f_sqrt_s = sqrt_s
    call rambo(f_sqrt_s, f_p_rambo)
    p_rambo = f_p_rambo
  end subroutine rambo_c


  subroutine hel_init
    implicit none
    integer :: binpos, flip, binco
    hel_not_initialised = .false.
    ! helicity configurations for this process
  H(:, 1) = [ -1, -1, -1, -1, -1 ]
  H(:, 2) = [ -1, -1, -1, -1,  1 ]
  H(:, 3) = [ -1, -1, -1,  1, -1 ]
  H(:, 4) = [ -1, -1, -1,  1,  1 ]
  H(:, 5) = [ -1, -1,  1, -1, -1 ]
  H(:, 6) = [ -1, -1,  1, -1,  1 ]
  H(:, 7) = [ -1, -1,  1,  1, -1 ]
  H(:, 8) = [ -1, -1,  1,  1,  1 ]
  H(:, 9) = [ -1,  1, -1, -1, -1 ]
  H(:,10) = [ -1,  1, -1, -1,  1 ]
  H(:,11) = [ -1,  1, -1,  1, -1 ]
  H(:,12) = [ -1,  1, -1,  1,  1 ]
  H(:,13) = [ -1,  1,  1, -1, -1 ]
  H(:,14) = [ -1,  1,  1, -1,  1 ]
  H(:,15) = [ -1,  1,  1,  1, -1 ]
  H(:,16) = [ -1,  1,  1,  1,  1 ]
  H(:,17) = [  1, -1, -1, -1, -1 ]
  H(:,18) = [  1, -1, -1, -1,  1 ]
  H(:,19) = [  1, -1, -1,  1, -1 ]
  H(:,20) = [  1, -1, -1,  1,  1 ]
  H(:,21) = [  1, -1,  1, -1, -1 ]
  H(:,22) = [  1, -1,  1, -1,  1 ]
  H(:,23) = [  1, -1,  1,  1, -1 ]
  H(:,24) = [  1, -1,  1,  1,  1 ]
  H(:,25) = [  1,  1, -1, -1, -1 ]
  H(:,26) = [  1,  1, -1, -1,  1 ]
  H(:,27) = [  1,  1, -1,  1, -1 ]
  H(:,28) = [  1,  1, -1,  1,  1 ]
  H(:,29) = [  1,  1,  1, -1, -1 ]
  H(:,30) = [  1,  1,  1, -1,  1 ]
  H(:,31) = [  1,  1,  1,  1, -1 ]
  H(:,32) = [  1,  1,  1,  1,  1 ]

  H_HC(:,5) = [ ((((2*(binco-1)+flip)*1+binpos, flip = 0, 1), binpos = 1, 1), binco = 1, 32/1/2) ]
  end subroutine hel_init


  subroutine pol_init(pol) &
      & bind(c,name="ol_f_pol_init_eeee_lew_eeexexa_1")
    implicit none
    integer, intent(in) :: pol(5)
    POLSEL = pol
  end subroutine pol_init


  subroutine set_photons(photon_id) &
      & bind(c,name="ol_f_set_photons_eeee_lew_eeexexa_1")
    implicit none
    integer, intent(in) :: photon_id(5)
    photonid_eeee_lew_eeexexa_1 = photon_id
  end subroutine set_photons


end module ol_external_eeee_lew_eeexexa_1


module ol_settings_eeee_lew_eeexexa_1
  implicit none
  ! Activates optimized helicity bookkeeping. Default=false
    logical, save :: hel_mem_opt = .false.
  ! Calculate loop correlators
    logical, save :: loopcc = .false.

  contains

  subroutine set_hel_mem_opt(set) &
      & bind(c,name="ol_hel_mem_opt_eeee_lew_eeexexa_1")
    implicit none
    logical, intent(in) :: set
    hel_mem_opt = set
  end subroutine set_hel_mem_opt

  subroutine set_loopcc(set,was) &
      & bind(c,name="ol_loopcc_eeee_lew_eeexexa_1")
    implicit none
    logical, intent(in) :: set
    logical :: was
    was = loopcc
    loopcc = set
  end subroutine set_loopcc

end module ol_settings_eeee_lew_eeexexa_1

module colour_basis_eeee_lew_eeexexa_1
  implicit none
  ! tree colour basis
  integer, save :: extcolours(5) = [0,0,0,0,0]
  contains

  pure subroutine tree_colbasis_dim(extcols, ncolb, ncoupl, maxpows, nhel) &
    & bind(c, name="ol_tree_colbasis_dim_eeee_lew_eeexexa_1")
    implicit none
    ! colour representation of external particles: 0=neutral, 1=fundamental, 2=adjoint
    integer, intent(out) :: extcols(5)
    ! number of tree colour basis elements; number of selected couplings, number of selected powers per coupling
    integer, intent(out) :: ncolb, ncoupl, maxpows
    ! number of helicity configurations (all, not just non-vanishing)
    integer, intent(out) :: nhel
    extcols = extcolours
    ncolb = 1
    ncoupl = 1
    maxpows = 1
    nhel = 32
  end subroutine tree_colbasis_dim

  subroutine tree_colbasis(basis, powers) &
    & bind(c, name="ol_tree_colbasis_eeee_lew_eeexexa_1")
    implicit none
    integer, intent(out) :: powers(1,1)
    integer, intent(out) :: basis(3,1)
#if 1 > 0
    ! selected powers for each selected coupling
    powers = reshape([0], [1,1])
#endif
#if 1 > 0
    ! tree colour basis: [[composition_number, permutation_number, *coupling_powers], ...]
    basis = reshape( &
      [1,1,0], &
      [3,1])
#endif
  end subroutine tree_colbasis


  pure subroutine loop_colbasis_dim(extcols, ncolb, ncoupl, maxpows, nhel) &
    & bind(c, name="ol_loop_colbasis_dim_eeee_lew_eeexexa_1")
    implicit none
    ! colour representation of external particles: 0=neutral, 1=fundamental, 2=adjoint
    integer, intent(out) :: extcols(5)
    ! number of loop colour basis elements; number of selected couplings, number of selected powers per coupling
    integer, intent(out) :: ncolb, ncoupl, maxpows
    ! number of helicity configurations (all, not just non-vanishing)
    integer, intent(out) :: nhel
    extcols = extcolours
    ncolb = 1
    ncoupl = 1
    maxpows = 1
    nhel = 32
  end subroutine loop_colbasis_dim

  subroutine loop_colbasis(basis, powers) &
    & bind(c, name="ol_loop_colbasis_eeee_lew_eeexexa_1")
    implicit none
    integer, intent(out) :: powers(1,1)
    integer, intent(out) :: basis(3,1)
#if 1 > 0
    ! selected powers for each selected coupling
    powers = reshape([0], [1,1])
#endif
#if 1 > 0
    basis = 0 ! TODO
    ! loop colour basis: [[composition_number, permutation_number, *coupling_powers], ...]
!    basis = reshape( &
!<\*colourbasis\*>, &
!      [3,1])
#endif
  end subroutine loop_colbasis


end module colour_basis_eeee_lew_eeexexa_1

! **********************************************************************
module ol_heltables_OLR_eeee_lew_eeexexa_1
! **********************************************************************
  use KIND_TYPES, only: intkind2
  implicit none

  logical :: heltables_not_init = .true.

  ! helicity states of external particles
  ! integer, save :: &
  !   H1(2) = [-1,1], &
  !   H2(3) = [-1,0,1]
  !   ...
  integer, save :: &
    H1(2) = [-1,1], &
    H2(2) = [-1,1], &
    H3(2) = [-1,1], &
    H4(2) = [-1,1], &
    H5(2) = [-1,1]

  ! number of helicity states for wave functions returned by a propagator call: n2(sz)
  ! number of helicity states for wave functions in a v-point vertex call (v >= 3)
  ! or a contraction (v = 3): n<v>(v,sz)
  integer(intkind2), save :: n2(84), n3(3,430)

  ! helicity tables used in the construction of the h helicity states of a wave function (amplitude)
  ! from an v-point vertex (contraction): t<v>x<h>(v-1,h,sz)
  integer(intkind2), save :: t3x4(2,4,40), t3x8(2,8,138), t3x32(2,32,252)

  ! change of global-helicity state resulting from flip of individual-particle helicity
  integer(intkind2), save :: eflip(32,5)
  integer,           save :: exthel(32,5)
  integer,           save :: firstpol(5)

  contains

! **********************************************************************
subroutine init_heltables
! **********************************************************************
  use ol_helicity_init, only: heltable
  implicit none

  ! I/O helicity tables for vertices, propagators and contractions;
  ! helicity table for a vertex call: n_in/n_out are the number helicity states of the incoming/outgoing wave functions
  ! call heltable([<n_in1>, <n_in2>, ..., <n_out>], n, t)
  ! propagators only need the number of helicity configurations which is equal for the incoming and outgoing wave function
  ! n = <n>
  call heltable([2,2,4], n3(:,1), t3x4(:,:,1))
  call heltable([2,2,4], n3(:,2), t3x4(:,:,2))
  n2(1) = 4
  call heltable([4,2,8], n3(:,3), t3x8(:,:,1))
  call heltable([2,2,4], n3(:,4), t3x4(:,:,3))
  call heltable([4,2,8], n3(:,5), t3x8(:,:,2))
  call heltable([2,2,4], n3(:,6), t3x4(:,:,4))
  call heltable([2,4,8], n3(:,7), t3x8(:,:,3))
  call heltable([2,2,4], n3(:,8), t3x4(:,:,5))
  call heltable([2,4,8], n3(:,9), t3x8(:,:,4))
  call heltable([2,2,4], n3(:,10), t3x4(:,:,6))
  n2(2) = 4
  call heltable([2,4,8], n3(:,11), t3x8(:,:,5))
  call heltable([2,4,8], n3(:,12), t3x8(:,:,6))
  call heltable([4,2,8], n3(:,13), t3x8(:,:,7))
  call heltable([4,2,8], n3(:,14), t3x8(:,:,8))
  call heltable([2,2,4], n3(:,15), t3x4(:,:,7))
  call heltable([2,2,4], n3(:,16), t3x4(:,:,8))
  n2(3) = 4
  call heltable([2,4,8], n3(:,17), t3x8(:,:,9))
  call heltable([2,2,4], n3(:,18), t3x4(:,:,9))
  call heltable([2,4,8], n3(:,19), t3x8(:,:,10))
  call heltable([2,2,4], n3(:,20), t3x4(:,:,10))
  call heltable([4,2,8], n3(:,21), t3x8(:,:,11))
  call heltable([2,2,4], n3(:,22), t3x4(:,:,11))
  call heltable([4,2,8], n3(:,23), t3x8(:,:,12))
  call heltable([2,4,8], n3(:,24), t3x8(:,:,13))
  call heltable([2,4,8], n3(:,25), t3x8(:,:,14))
  call heltable([4,2,8], n3(:,26), t3x8(:,:,15))
  call heltable([4,2,8], n3(:,27), t3x8(:,:,16))
  call heltable([2,2,4], n3(:,28), t3x4(:,:,12))
  call heltable([4,2,8], n3(:,29), t3x8(:,:,17))
  call heltable([2,2,4], n3(:,30), t3x4(:,:,13))
  call heltable([4,2,8], n3(:,31), t3x8(:,:,18))
  call heltable([2,2,4], n3(:,32), t3x4(:,:,14))
  call heltable([2,4,8], n3(:,33), t3x8(:,:,19))
  call heltable([2,2,4], n3(:,34), t3x4(:,:,15))
  call heltable([2,4,8], n3(:,35), t3x8(:,:,20))
  call heltable([2,2,4], n3(:,36), t3x4(:,:,16))
  n2(4) = 4
  call heltable([2,4,8], n3(:,37), t3x8(:,:,21))
  call heltable([2,4,8], n3(:,38), t3x8(:,:,22))
  call heltable([4,2,8], n3(:,39), t3x8(:,:,23))
  call heltable([4,2,8], n3(:,40), t3x8(:,:,24))
  call heltable([2,2,4], n3(:,41), t3x4(:,:,17))
  call heltable([2,4,8], n3(:,42), t3x8(:,:,25))
  call heltable([2,2,4], n3(:,43), t3x4(:,:,18))
  call heltable([2,4,8], n3(:,44), t3x8(:,:,26))
  call heltable([2,2,4], n3(:,45), t3x4(:,:,19))
  call heltable([4,2,8], n3(:,46), t3x8(:,:,27))
  call heltable([2,2,4], n3(:,47), t3x4(:,:,20))
  call heltable([4,2,8], n3(:,48), t3x8(:,:,28))
  call heltable([2,4,8], n3(:,49), t3x8(:,:,29))
  call heltable([2,4,8], n3(:,50), t3x8(:,:,30))
  call heltable([4,2,8], n3(:,51), t3x8(:,:,31))
  call heltable([4,2,8], n3(:,52), t3x8(:,:,32))
  call heltable([4,2,8], n3(:,53), t3x8(:,:,33))
  call heltable([2,4,8], n3(:,54), t3x8(:,:,34))
  call heltable([4,2,8], n3(:,55), t3x8(:,:,35))
  call heltable([4,2,8], n3(:,56), t3x8(:,:,36))
  call heltable([4,2,8], n3(:,57), t3x8(:,:,37))
  call heltable([4,2,8], n3(:,58), t3x8(:,:,38))
  call heltable([4,2,8], n3(:,59), t3x8(:,:,39))
  call heltable([2,4,8], n3(:,60), t3x8(:,:,40))
  call heltable([2,4,8], n3(:,61), t3x8(:,:,41))
  call heltable([2,2,4], n3(:,62), t3x4(:,:,21))
  n2(5) = 4
  call heltable([4,2,8], n3(:,63), t3x8(:,:,42))
  call heltable([2,4,8], n3(:,64), t3x8(:,:,43))
  call heltable([4,2,8], n3(:,65), t3x8(:,:,44))
  call heltable([4,2,8], n3(:,66), t3x8(:,:,45))
  call heltable([4,2,8], n3(:,67), t3x8(:,:,46))
  call heltable([2,4,8], n3(:,68), t3x8(:,:,47))
  call heltable([2,4,8], n3(:,69), t3x8(:,:,48))
  call heltable([4,2,8], n3(:,70), t3x8(:,:,49))
  call heltable([4,2,8], n3(:,71), t3x8(:,:,50))
  call heltable([4,2,8], n3(:,72), t3x8(:,:,51))
  call heltable([4,2,8], n3(:,73), t3x8(:,:,52))
  call heltable([2,4,8], n3(:,74), t3x8(:,:,53))
  call heltable([2,4,8], n3(:,75), t3x8(:,:,54))
  call heltable([2,2,4], n3(:,76), t3x4(:,:,22))
  n2(6) = 4
  call heltable([2,4,8], n3(:,77), t3x8(:,:,55))
  call heltable([2,4,8], n3(:,78), t3x8(:,:,56))
  call heltable([4,2,8], n3(:,79), t3x8(:,:,57))
  call heltable([4,2,8], n3(:,80), t3x8(:,:,58))
  call heltable([2,4,8], n3(:,81), t3x8(:,:,59))
  call heltable([2,4,8], n3(:,82), t3x8(:,:,60))
  call heltable([4,2,8], n3(:,83), t3x8(:,:,61))
  call heltable([4,2,8], n3(:,84), t3x8(:,:,62))
  call heltable([2,2,4], n3(:,85), t3x4(:,:,23))
  n2(7) = 4
  call heltable([2,4,8], n3(:,86), t3x8(:,:,63))
  call heltable([2,4,8], n3(:,87), t3x8(:,:,64))
  call heltable([4,2,8], n3(:,88), t3x8(:,:,65))
  call heltable([4,2,8], n3(:,89), t3x8(:,:,66))
  call heltable([2,2,4], n3(:,90), t3x4(:,:,24))
  call heltable([2,2,4], n3(:,91), t3x4(:,:,25))
  call heltable([2,2,4], n3(:,92), t3x4(:,:,26))
  call heltable([2,2,4], n3(:,93), t3x4(:,:,27))
  call heltable([2,4,8], n3(:,94), t3x8(:,:,67))
  call heltable([2,4,8], n3(:,95), t3x8(:,:,68))
  call heltable([4,2,8], n3(:,96), t3x8(:,:,69))
  call heltable([4,2,8], n3(:,97), t3x8(:,:,70))
  call heltable([2,2,4], n3(:,98), t3x4(:,:,28))
  call heltable([2,2,4], n3(:,99), t3x4(:,:,29))
  call heltable([2,2,4], n3(:,100), t3x4(:,:,30))
  call heltable([2,2,4], n3(:,101), t3x4(:,:,31))
  call heltable([2,4,8], n3(:,102), t3x8(:,:,71))
  call heltable([2,4,8], n3(:,103), t3x8(:,:,72))
  call heltable([4,2,8], n3(:,104), t3x8(:,:,73))
  call heltable([4,2,8], n3(:,105), t3x8(:,:,74))
  call heltable([2,4,8], n3(:,106), t3x8(:,:,75))
  call heltable([2,4,8], n3(:,107), t3x8(:,:,76))
  call heltable([4,2,8], n3(:,108), t3x8(:,:,77))
  call heltable([4,2,8], n3(:,109), t3x8(:,:,78))
  call heltable([2,2,4], n3(:,110), t3x4(:,:,32))
  n2(8) = 4
  call heltable([2,4,8], n3(:,111), t3x8(:,:,79))
  call heltable([2,4,8], n3(:,112), t3x8(:,:,80))
  call heltable([4,2,8], n3(:,113), t3x8(:,:,81))
  call heltable([4,2,8], n3(:,114), t3x8(:,:,82))
  call heltable([2,4,8], n3(:,115), t3x8(:,:,83))
  call heltable([2,4,8], n3(:,116), t3x8(:,:,84))
  call heltable([4,2,8], n3(:,117), t3x8(:,:,85))
  call heltable([4,2,8], n3(:,118), t3x8(:,:,86))
  call heltable([2,4,8], n3(:,119), t3x8(:,:,87))
  call heltable([2,4,8], n3(:,120), t3x8(:,:,88))
  call heltable([4,2,8], n3(:,121), t3x8(:,:,89))
  call heltable([4,2,8], n3(:,122), t3x8(:,:,90))
  call heltable([2,2,4], n3(:,123), t3x4(:,:,33))
  call heltable([4,2,8], n3(:,124), t3x8(:,:,91))
  call heltable([2,2,4], n3(:,125), t3x4(:,:,34))
  call heltable([4,2,8], n3(:,126), t3x8(:,:,92))
  call heltable([2,2,4], n3(:,127), t3x4(:,:,35))
  call heltable([2,4,8], n3(:,128), t3x8(:,:,93))
  call heltable([2,2,4], n3(:,129), t3x4(:,:,36))
  call heltable([2,4,8], n3(:,130), t3x8(:,:,94))
  call heltable([2,4,8], n3(:,131), t3x8(:,:,95))
  call heltable([2,4,8], n3(:,132), t3x8(:,:,96))
  call heltable([4,2,8], n3(:,133), t3x8(:,:,97))
  call heltable([4,2,8], n3(:,134), t3x8(:,:,98))
  call heltable([2,2,4], n3(:,135), t3x4(:,:,37))
  call heltable([4,2,8], n3(:,136), t3x8(:,:,99))
  call heltable([2,2,4], n3(:,137), t3x4(:,:,38))
  call heltable([4,2,8], n3(:,138), t3x8(:,:,100))
  call heltable([2,2,4], n3(:,139), t3x4(:,:,39))
  call heltable([2,4,8], n3(:,140), t3x8(:,:,101))
  call heltable([2,2,4], n3(:,141), t3x4(:,:,40))
  call heltable([2,4,8], n3(:,142), t3x8(:,:,102))
  call heltable([2,4,8], n3(:,143), t3x8(:,:,103))
  call heltable([2,4,8], n3(:,144), t3x8(:,:,104))
  call heltable([4,2,8], n3(:,145), t3x8(:,:,105))
  call heltable([4,2,8], n3(:,146), t3x8(:,:,106))
  call heltable([2,4,8], n3(:,147), t3x8(:,:,107))
  n2(9) = 4
  call heltable([2,4,8], n3(:,148), t3x8(:,:,108))
  n2(10) = 4
  call heltable([4,2,8], n3(:,149), t3x8(:,:,109))
  n2(11) = 4
  call heltable([4,2,8], n3(:,150), t3x8(:,:,110))
  n2(12) = 4
  n2(13) = 4
  n2(14) = 4
  n2(15) = 4
  n2(16) = 4
  n2(17) = 4
  n2(18) = 4
  n2(19) = 4
  n2(20) = 8
  n2(21) = 8
  n2(22) = 8
  n2(23) = 8
  call heltable([4,2,8], n3(:,151), t3x8(:,:,111))
  call heltable([4,2,8], n3(:,152), t3x8(:,:,112))
  call heltable([2,4,8], n3(:,153), t3x8(:,:,113))
  call heltable([2,4,8], n3(:,154), t3x8(:,:,114))
  n2(24) = 4
  n2(25) = 8
  n2(26) = 8
  n2(27) = 8
  n2(28) = 8
  call heltable([4,2,8], n3(:,155), t3x8(:,:,115))
  n2(29) = 4
  n2(30) = 8
  call heltable([4,2,8], n3(:,156), t3x8(:,:,116))
  n2(31) = 8
  call heltable([2,4,8], n3(:,157), t3x8(:,:,117))
  n2(32) = 8
  call heltable([2,4,8], n3(:,158), t3x8(:,:,118))
  n2(33) = 8
  n2(34) = 4
  n2(35) = 4
  n2(36) = 4
  n2(37) = 4
  n2(38) = 4
  n2(39) = 4
  n2(40) = 4
  n2(41) = 4
  n2(42) = 4
  n2(43) = 4
  call heltable([4,2,8], n3(:,159), t3x8(:,:,119))
  call heltable([4,2,8], n3(:,160), t3x8(:,:,120))
  call heltable([2,4,8], n3(:,161), t3x8(:,:,121))
  call heltable([2,4,8], n3(:,162), t3x8(:,:,122))
  n2(44) = 8
  n2(45) = 8
  n2(46) = 8
  n2(47) = 8
  call heltable([2,4,8], n3(:,163), t3x8(:,:,123))
  n2(48) = 4
  call heltable([2,4,8], n3(:,164), t3x8(:,:,124))
  n2(49) = 4
  call heltable([4,2,8], n3(:,165), t3x8(:,:,125))
  n2(50) = 4
  call heltable([4,2,8], n3(:,166), t3x8(:,:,126))
  n2(51) = 4
  n2(52) = 4
  n2(53) = 4
  n2(54) = 4
  n2(55) = 4
  n2(56) = 4
  n2(57) = 4
  n2(58) = 8
  n2(59) = 8
  n2(60) = 8
  n2(61) = 8
  call heltable([4,2,8], n3(:,167), t3x8(:,:,127))
  call heltable([4,2,8], n3(:,168), t3x8(:,:,128))
  call heltable([2,4,8], n3(:,169), t3x8(:,:,129))
  call heltable([2,4,8], n3(:,170), t3x8(:,:,130))
  n2(62) = 4
  n2(63) = 8
  n2(64) = 8
  n2(65) = 8
  n2(66) = 8
  call heltable([4,2,8], n3(:,171), t3x8(:,:,131))
  n2(67) = 8
  call heltable([4,2,8], n3(:,172), t3x8(:,:,132))
  n2(68) = 8
  call heltable([2,4,8], n3(:,173), t3x8(:,:,133))
  n2(69) = 8
  call heltable([2,4,8], n3(:,174), t3x8(:,:,134))
  n2(70) = 8
  n2(71) = 4
  n2(72) = 4
  n2(73) = 4
  n2(74) = 4
  n2(75) = 4
  n2(76) = 4
  n2(77) = 4
  n2(78) = 4
  n2(79) = 4
  n2(80) = 4
  call heltable([4,2,8], n3(:,175), t3x8(:,:,135))
  call heltable([4,2,8], n3(:,176), t3x8(:,:,136))
  call heltable([2,4,8], n3(:,177), t3x8(:,:,137))
  call heltable([2,4,8], n3(:,178), t3x8(:,:,138))
  n2(81) = 8
  n2(82) = 8
  n2(83) = 8
  n2(84) = 8
  call heltable([4,8,32], n3(:,179), t3x32(:,:,1))
  call heltable([4,8,32], n3(:,180), t3x32(:,:,2))
  call heltable([4,8,32], n3(:,181), t3x32(:,:,3))
  call heltable([4,8,32], n3(:,182), t3x32(:,:,4))
  call heltable([4,8,32], n3(:,183), t3x32(:,:,5))
  call heltable([4,8,32], n3(:,184), t3x32(:,:,6))
  call heltable([4,8,32], n3(:,185), t3x32(:,:,7))
  call heltable([4,8,32], n3(:,186), t3x32(:,:,8))
  call heltable([4,8,32], n3(:,187), t3x32(:,:,9))
  call heltable([4,8,32], n3(:,188), t3x32(:,:,10))
  call heltable([4,8,32], n3(:,189), t3x32(:,:,11))
  call heltable([4,8,32], n3(:,190), t3x32(:,:,12))
  call heltable([4,8,32], n3(:,191), t3x32(:,:,13))
  call heltable([4,8,32], n3(:,192), t3x32(:,:,14))
  call heltable([4,8,32], n3(:,193), t3x32(:,:,15))
  call heltable([4,8,32], n3(:,194), t3x32(:,:,16))
  call heltable([4,8,32], n3(:,195), t3x32(:,:,17))
  call heltable([4,8,32], n3(:,196), t3x32(:,:,18))
  call heltable([4,8,32], n3(:,197), t3x32(:,:,19))
  call heltable([4,8,32], n3(:,198), t3x32(:,:,20))
  call heltable([4,8,32], n3(:,199), t3x32(:,:,21))
  call heltable([4,8,32], n3(:,200), t3x32(:,:,22))
  call heltable([4,8,32], n3(:,201), t3x32(:,:,23))
  call heltable([4,8,32], n3(:,202), t3x32(:,:,24))
  call heltable([4,8,32], n3(:,203), t3x32(:,:,25))
  call heltable([4,8,32], n3(:,204), t3x32(:,:,26))
  call heltable([4,8,32], n3(:,205), t3x32(:,:,27))
  call heltable([4,8,32], n3(:,206), t3x32(:,:,28))
  call heltable([4,8,32], n3(:,207), t3x32(:,:,29))
  call heltable([4,8,32], n3(:,208), t3x32(:,:,30))
  call heltable([4,8,32], n3(:,209), t3x32(:,:,31))
  call heltable([4,8,32], n3(:,210), t3x32(:,:,32))
  call heltable([4,8,32], n3(:,211), t3x32(:,:,33))
  call heltable([4,8,32], n3(:,212), t3x32(:,:,34))
  call heltable([4,8,32], n3(:,213), t3x32(:,:,35))
  call heltable([4,8,32], n3(:,214), t3x32(:,:,36))
  call heltable([4,8,32], n3(:,215), t3x32(:,:,37))
  call heltable([4,8,32], n3(:,216), t3x32(:,:,38))
  call heltable([4,8,32], n3(:,217), t3x32(:,:,39))
  call heltable([4,8,32], n3(:,218), t3x32(:,:,40))
  call heltable([4,8,32], n3(:,219), t3x32(:,:,41))
  call heltable([4,8,32], n3(:,220), t3x32(:,:,42))
  call heltable([8,4,32], n3(:,221), t3x32(:,:,43))
  call heltable([8,4,32], n3(:,222), t3x32(:,:,44))
  call heltable([8,4,32], n3(:,223), t3x32(:,:,45))
  call heltable([8,4,32], n3(:,224), t3x32(:,:,46))
  call heltable([4,8,32], n3(:,225), t3x32(:,:,47))
  call heltable([4,8,32], n3(:,226), t3x32(:,:,48))
  call heltable([4,8,32], n3(:,227), t3x32(:,:,49))
  call heltable([4,8,32], n3(:,228), t3x32(:,:,50))
  call heltable([4,8,32], n3(:,229), t3x32(:,:,51))
  call heltable([4,8,32], n3(:,230), t3x32(:,:,52))
  call heltable([4,8,32], n3(:,231), t3x32(:,:,53))
  call heltable([4,8,32], n3(:,232), t3x32(:,:,54))
  call heltable([4,8,32], n3(:,233), t3x32(:,:,55))
  call heltable([4,8,32], n3(:,234), t3x32(:,:,56))
  call heltable([8,4,32], n3(:,235), t3x32(:,:,57))
  call heltable([8,4,32], n3(:,236), t3x32(:,:,58))
  call heltable([8,4,32], n3(:,237), t3x32(:,:,59))
  call heltable([8,4,32], n3(:,238), t3x32(:,:,60))
  call heltable([4,8,32], n3(:,239), t3x32(:,:,61))
  call heltable([4,8,32], n3(:,240), t3x32(:,:,62))
  call heltable([4,8,32], n3(:,241), t3x32(:,:,63))
  call heltable([4,8,32], n3(:,242), t3x32(:,:,64))
  call heltable([8,4,32], n3(:,243), t3x32(:,:,65))
  call heltable([8,4,32], n3(:,244), t3x32(:,:,66))
  call heltable([8,4,32], n3(:,245), t3x32(:,:,67))
  call heltable([8,4,32], n3(:,246), t3x32(:,:,68))
  call heltable([4,8,32], n3(:,247), t3x32(:,:,69))
  call heltable([4,8,32], n3(:,248), t3x32(:,:,70))
  call heltable([4,8,32], n3(:,249), t3x32(:,:,71))
  call heltable([4,8,32], n3(:,250), t3x32(:,:,72))
  call heltable([8,4,32], n3(:,251), t3x32(:,:,73))
  call heltable([8,4,32], n3(:,252), t3x32(:,:,74))
  call heltable([8,4,32], n3(:,253), t3x32(:,:,75))
  call heltable([8,4,32], n3(:,254), t3x32(:,:,76))
  call heltable([4,8,32], n3(:,255), t3x32(:,:,77))
  call heltable([4,8,32], n3(:,256), t3x32(:,:,78))
  call heltable([4,8,32], n3(:,257), t3x32(:,:,79))
  call heltable([4,8,32], n3(:,258), t3x32(:,:,80))
  call heltable([8,4,32], n3(:,259), t3x32(:,:,81))
  call heltable([8,4,32], n3(:,260), t3x32(:,:,82))
  call heltable([8,4,32], n3(:,261), t3x32(:,:,83))
  call heltable([8,4,32], n3(:,262), t3x32(:,:,84))
  call heltable([4,8,32], n3(:,263), t3x32(:,:,85))
  call heltable([4,8,32], n3(:,264), t3x32(:,:,86))
  call heltable([4,8,32], n3(:,265), t3x32(:,:,87))
  call heltable([4,8,32], n3(:,266), t3x32(:,:,88))
  call heltable([8,4,32], n3(:,267), t3x32(:,:,89))
  call heltable([8,4,32], n3(:,268), t3x32(:,:,90))
  call heltable([8,4,32], n3(:,269), t3x32(:,:,91))
  call heltable([8,4,32], n3(:,270), t3x32(:,:,92))
  call heltable([8,4,32], n3(:,271), t3x32(:,:,93))
  call heltable([8,4,32], n3(:,272), t3x32(:,:,94))
  call heltable([8,4,32], n3(:,273), t3x32(:,:,95))
  call heltable([8,4,32], n3(:,274), t3x32(:,:,96))
  call heltable([4,8,32], n3(:,275), t3x32(:,:,97))
  call heltable([4,8,32], n3(:,276), t3x32(:,:,98))
  call heltable([4,8,32], n3(:,277), t3x32(:,:,99))
  call heltable([4,8,32], n3(:,278), t3x32(:,:,100))
  call heltable([8,4,32], n3(:,279), t3x32(:,:,101))
  call heltable([8,4,32], n3(:,280), t3x32(:,:,102))
  call heltable([8,4,32], n3(:,281), t3x32(:,:,103))
  call heltable([8,4,32], n3(:,282), t3x32(:,:,104))
  call heltable([4,8,32], n3(:,283), t3x32(:,:,105))
  call heltable([4,8,32], n3(:,284), t3x32(:,:,106))
  call heltable([4,8,32], n3(:,285), t3x32(:,:,107))
  call heltable([4,8,32], n3(:,286), t3x32(:,:,108))
  call heltable([4,8,32], n3(:,287), t3x32(:,:,109))
  call heltable([4,8,32], n3(:,288), t3x32(:,:,110))
  call heltable([4,8,32], n3(:,289), t3x32(:,:,111))
  call heltable([4,8,32], n3(:,290), t3x32(:,:,112))
  call heltable([4,8,32], n3(:,291), t3x32(:,:,113))
  call heltable([4,8,32], n3(:,292), t3x32(:,:,114))
  call heltable([4,8,32], n3(:,293), t3x32(:,:,115))
  call heltable([4,8,32], n3(:,294), t3x32(:,:,116))
  call heltable([4,8,32], n3(:,295), t3x32(:,:,117))
  call heltable([4,8,32], n3(:,296), t3x32(:,:,118))
  call heltable([4,8,32], n3(:,297), t3x32(:,:,119))
  call heltable([4,8,32], n3(:,298), t3x32(:,:,120))
  call heltable([4,8,32], n3(:,299), t3x32(:,:,121))
  call heltable([4,8,32], n3(:,300), t3x32(:,:,122))
  call heltable([4,8,32], n3(:,301), t3x32(:,:,123))
  call heltable([4,8,32], n3(:,302), t3x32(:,:,124))
  call heltable([4,8,32], n3(:,303), t3x32(:,:,125))
  call heltable([4,8,32], n3(:,304), t3x32(:,:,126))
  call heltable([4,8,32], n3(:,305), t3x32(:,:,127))
  call heltable([4,8,32], n3(:,306), t3x32(:,:,128))
  call heltable([4,8,32], n3(:,307), t3x32(:,:,129))
  call heltable([4,8,32], n3(:,308), t3x32(:,:,130))
  call heltable([4,8,32], n3(:,309), t3x32(:,:,131))
  call heltable([4,8,32], n3(:,310), t3x32(:,:,132))
  call heltable([4,8,32], n3(:,311), t3x32(:,:,133))
  call heltable([4,8,32], n3(:,312), t3x32(:,:,134))
  call heltable([4,8,32], n3(:,313), t3x32(:,:,135))
  call heltable([4,8,32], n3(:,314), t3x32(:,:,136))
  call heltable([4,8,32], n3(:,315), t3x32(:,:,137))
  call heltable([4,8,32], n3(:,316), t3x32(:,:,138))
  call heltable([4,8,32], n3(:,317), t3x32(:,:,139))
  call heltable([4,8,32], n3(:,318), t3x32(:,:,140))
  call heltable([8,4,32], n3(:,319), t3x32(:,:,141))
  call heltable([8,4,32], n3(:,320), t3x32(:,:,142))
  call heltable([8,4,32], n3(:,321), t3x32(:,:,143))
  call heltable([8,4,32], n3(:,322), t3x32(:,:,144))
  call heltable([8,4,32], n3(:,323), t3x32(:,:,145))
  call heltable([8,4,32], n3(:,324), t3x32(:,:,146))
  call heltable([8,4,32], n3(:,325), t3x32(:,:,147))
  call heltable([8,4,32], n3(:,326), t3x32(:,:,148))
  call heltable([8,4,32], n3(:,327), t3x32(:,:,149))
  call heltable([8,4,32], n3(:,328), t3x32(:,:,150))
  call heltable([4,8,32], n3(:,329), t3x32(:,:,151))
  call heltable([4,8,32], n3(:,330), t3x32(:,:,152))
  call heltable([4,8,32], n3(:,331), t3x32(:,:,153))
  call heltable([4,8,32], n3(:,332), t3x32(:,:,154))
  call heltable([4,8,32], n3(:,333), t3x32(:,:,155))
  call heltable([4,8,32], n3(:,334), t3x32(:,:,156))
  call heltable([4,8,32], n3(:,335), t3x32(:,:,157))
  call heltable([4,8,32], n3(:,336), t3x32(:,:,158))
  call heltable([4,8,32], n3(:,337), t3x32(:,:,159))
  call heltable([4,8,32], n3(:,338), t3x32(:,:,160))
  call heltable([4,8,32], n3(:,339), t3x32(:,:,161))
  call heltable([4,8,32], n3(:,340), t3x32(:,:,162))
  call heltable([4,8,32], n3(:,341), t3x32(:,:,163))
  call heltable([4,8,32], n3(:,342), t3x32(:,:,164))
  call heltable([4,8,32], n3(:,343), t3x32(:,:,165))
  call heltable([4,8,32], n3(:,344), t3x32(:,:,166))
  call heltable([4,8,32], n3(:,345), t3x32(:,:,167))
  call heltable([4,8,32], n3(:,346), t3x32(:,:,168))
  call heltable([4,8,32], n3(:,347), t3x32(:,:,169))
  call heltable([4,8,32], n3(:,348), t3x32(:,:,170))
  call heltable([4,8,32], n3(:,349), t3x32(:,:,171))
  call heltable([4,8,32], n3(:,350), t3x32(:,:,172))
  call heltable([8,4,32], n3(:,351), t3x32(:,:,173))
  call heltable([8,4,32], n3(:,352), t3x32(:,:,174))
  call heltable([8,4,32], n3(:,353), t3x32(:,:,175))
  call heltable([8,4,32], n3(:,354), t3x32(:,:,176))
  call heltable([8,4,32], n3(:,355), t3x32(:,:,177))
  call heltable([8,4,32], n3(:,356), t3x32(:,:,178))
  call heltable([8,4,32], n3(:,357), t3x32(:,:,179))
  call heltable([8,4,32], n3(:,358), t3x32(:,:,180))
  call heltable([8,4,32], n3(:,359), t3x32(:,:,181))
  call heltable([8,4,32], n3(:,360), t3x32(:,:,182))
  call heltable([4,8,32], n3(:,361), t3x32(:,:,183))
  call heltable([4,8,32], n3(:,362), t3x32(:,:,184))
  call heltable([4,8,32], n3(:,363), t3x32(:,:,185))
  call heltable([4,8,32], n3(:,364), t3x32(:,:,186))
  call heltable([4,8,32], n3(:,365), t3x32(:,:,187))
  call heltable([4,8,32], n3(:,366), t3x32(:,:,188))
  call heltable([4,8,32], n3(:,367), t3x32(:,:,189))
  call heltable([4,8,32], n3(:,368), t3x32(:,:,190))
  call heltable([4,8,32], n3(:,369), t3x32(:,:,191))
  call heltable([4,8,32], n3(:,370), t3x32(:,:,192))
  call heltable([4,8,32], n3(:,371), t3x32(:,:,193))
  call heltable([4,8,32], n3(:,372), t3x32(:,:,194))
  call heltable([4,8,32], n3(:,373), t3x32(:,:,195))
  call heltable([4,8,32], n3(:,374), t3x32(:,:,196))
  call heltable([8,4,32], n3(:,375), t3x32(:,:,197))
  call heltable([8,4,32], n3(:,376), t3x32(:,:,198))
  call heltable([8,4,32], n3(:,377), t3x32(:,:,199))
  call heltable([8,4,32], n3(:,378), t3x32(:,:,200))
  call heltable([8,4,32], n3(:,379), t3x32(:,:,201))
  call heltable([8,4,32], n3(:,380), t3x32(:,:,202))
  call heltable([8,4,32], n3(:,381), t3x32(:,:,203))
  call heltable([8,4,32], n3(:,382), t3x32(:,:,204))
  call heltable([8,4,32], n3(:,383), t3x32(:,:,205))
  call heltable([8,4,32], n3(:,384), t3x32(:,:,206))
  call heltable([4,8,32], n3(:,385), t3x32(:,:,207))
  call heltable([4,8,32], n3(:,386), t3x32(:,:,208))
  call heltable([4,8,32], n3(:,387), t3x32(:,:,209))
  call heltable([4,8,32], n3(:,388), t3x32(:,:,210))
  call heltable([4,8,32], n3(:,389), t3x32(:,:,211))
  call heltable([4,8,32], n3(:,390), t3x32(:,:,212))
  call heltable([4,8,32], n3(:,391), t3x32(:,:,213))
  call heltable([4,8,32], n3(:,392), t3x32(:,:,214))
  call heltable([4,8,32], n3(:,393), t3x32(:,:,215))
  call heltable([4,8,32], n3(:,394), t3x32(:,:,216))
  call heltable([4,8,32], n3(:,395), t3x32(:,:,217))
  call heltable([4,8,32], n3(:,396), t3x32(:,:,218))
  call heltable([4,8,32], n3(:,397), t3x32(:,:,219))
  call heltable([4,8,32], n3(:,398), t3x32(:,:,220))
  call heltable([4,8,32], n3(:,399), t3x32(:,:,221))
  call heltable([4,8,32], n3(:,400), t3x32(:,:,222))
  call heltable([4,8,32], n3(:,401), t3x32(:,:,223))
  call heltable([4,8,32], n3(:,402), t3x32(:,:,224))
  call heltable([4,8,32], n3(:,403), t3x32(:,:,225))
  call heltable([4,8,32], n3(:,404), t3x32(:,:,226))
  call heltable([4,8,32], n3(:,405), t3x32(:,:,227))
  call heltable([4,8,32], n3(:,406), t3x32(:,:,228))
  call heltable([8,4,32], n3(:,407), t3x32(:,:,229))
  call heltable([8,4,32], n3(:,408), t3x32(:,:,230))
  call heltable([8,4,32], n3(:,409), t3x32(:,:,231))
  call heltable([8,4,32], n3(:,410), t3x32(:,:,232))
  call heltable([8,4,32], n3(:,411), t3x32(:,:,233))
  call heltable([8,4,32], n3(:,412), t3x32(:,:,234))
  call heltable([8,4,32], n3(:,413), t3x32(:,:,235))
  call heltable([8,4,32], n3(:,414), t3x32(:,:,236))
  call heltable([8,4,32], n3(:,415), t3x32(:,:,237))
  call heltable([8,4,32], n3(:,416), t3x32(:,:,238))
  call heltable([4,8,32], n3(:,417), t3x32(:,:,239))
  call heltable([4,8,32], n3(:,418), t3x32(:,:,240))
  call heltable([4,8,32], n3(:,419), t3x32(:,:,241))
  call heltable([4,8,32], n3(:,420), t3x32(:,:,242))
  call heltable([4,8,32], n3(:,421), t3x32(:,:,243))
  call heltable([4,8,32], n3(:,422), t3x32(:,:,244))
  call heltable([4,8,32], n3(:,423), t3x32(:,:,245))
  call heltable([4,8,32], n3(:,424), t3x32(:,:,246))
  call heltable([4,8,32], n3(:,425), t3x32(:,:,247))
  call heltable([4,8,32], n3(:,426), t3x32(:,:,248))
  call heltable([4,8,32], n3(:,427), t3x32(:,:,249))
  call heltable([4,8,32], n3(:,428), t3x32(:,:,250))
  call heltable([4,8,32], n3(:,429), t3x32(:,:,251))
  call heltable([4,8,32], n3(:,430), t3x32(:,:,252))

  heltables_not_init = .false.

end subroutine init_heltables

end module ol_heltables_OLR_eeee_lew_eeexexa_1
