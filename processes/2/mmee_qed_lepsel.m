(* fermion loops are excluded *)

 OpenLoopsModel = "SM_lepsel";

InsertFieldsOptions = {Restrictions -> {ExcludeParticles -> {V[2 | 3 | 4 | 5],U[__],S[1 | 2 | 3], F[1, __], F[2, {3}], F[4, __], F[3, __]}}};
SelectLoopDiagrams = ( Not[ContainsFermionLoop[##] && NLegsOnLoop[2][##]] && ParticlesInLoop[V[1]][##] &);
QED = 2;
SetParameters = JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}];

qed02mixed2 = {
  SelectInterference -> {gQCD -> 0, Qmu -> 2},
  Approximation -> "nofermionloops_Qmu_2"
};

qed02mixed3 = {
  SelectInterference -> {gQCD -> 0, Qmu -> 3},
  Approximation -> "nofermionloops_Qmu_3"
};

qed02mixed4 = {
  SelectInterference -> {gQCD -> 0, Qmu -> 4},
  Approximation -> "nofermionloops_Qmu_4"
};

qed02mixed5 = {
  SelectInterference -> {gQCD -> 0, Qmu -> 5},
  Approximation -> "nofermionloops_Qmu_5"
};

qed02mixed6 = {
  SelectInterference -> {gQCD -> 0, Qmu -> 6},
  Approximation -> "nofermionloops_Qmu_6"
};

qed02mixed7 = {
  SelectInterference -> {gQCD -> 0, Qmu -> 7},
  Approximation -> "nofermionloops_Qmu_7"
};

qed02mixed8 = {
  SelectInterference -> {gQCD -> 0, Qmu -> 8},
  Approximation -> "nofermionloops_Qmu_8"
};



(* mumuxeex *)    

AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ qed02mixed2];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ qed02mixed3];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ qed02mixed4];

(* mumuxeexa *)   

AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ qed02mixed2];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ qed02mixed3];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ qed02mixed4];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ qed02mixed5];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ qed02mixed6];

(*
(* mumuxeexaa *)  

AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02mixed2];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02mixed3];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02mixed4];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02mixed5];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02mixed6];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02mixed7];
AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02mixed8];
*)
