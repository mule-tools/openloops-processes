
module ol_vamp_1_eepipi_qed_nfl1_eexpipixa_12_/**/REALKIND
contains

! **********************************************************************
subroutine vamp_1(M)
! P(0:3,nlegs) = incoming external momenta
! Uses tree structures 'wf', factors 'c', and denominators 'den' from loop_eepipi_qed_nfl1_eexpipixa_12.
! Sets colour stripped amplitudes A from the module loop_amplitudes_eepipi_qed_nfl1_eexpipixa_12.
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_parameters_decl_/**/DREALKIND, only: l_switch !, kloopmax
  use ol_parameters_decl_/**/QREALKIND ! masses
  use ol_vert_interface_/**/REALKIND
  use ol_prop_interface_/**/REALKIND
  use ol_last_step_/**/REALKIND
  use ol_tables_storage_eepipi_qed_nfl1_eexpipixa_12_/**/DREALKIND
  use ol_tensor_sum_storage_eepipi_qed_nfl1_eexpipixa_12_/**/REALKIND
  use ol_loop_handling_/**/REALKIND
  use ofred_reduction_/**/REALKIND, only: Hotf_4pt_reduction, Hotf_4pt_reduction_last
  use ofred_reduction_/**/REALKIND, only: Hotf_5pt_reduction, Hotf_5pt_reduction_last
  use ol_loop_reduction_/**/REALKIND, only: TI_bubble_red, TI_triangle_red

  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_12_/**/REALKIND
#ifndef PRECISION_dp
  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_12_/**/DREALKIND, only: &
    & p_switch, Hel, merge_step, merge_mism, merge_tables, merge_hels, ntryL, hel_states
#endif
  use hol_initialisation_/**/REALKIND, only: G0_hol_initialisation
  use ol_h_vert_interface_/**/REALKIND
  use ol_h_prop_interface_/**/REALKIND
  use ol_h_last_step_/**/REALKIND
  use ol_merging_/**/REALKIND, only: ol_merge, ol_merge_tensors, ol_merge_last

  implicit none

  type(Hpolcont) :: Gcoeff(hel_states)
  type(Hpolcont), intent(in) :: M(2,hel_states)
  integer :: kloop


#ifndef PRECISION_dp
  if (ntryL==1 .OR. p_switch == 1) Gcoeff(:)%hf = Hel(1:hel_states)
#else
  if (ntryL==1 .OR. p_switch == 2) Gcoeff(:)%hf = Hel(1:hel_states)
#endif

! do kloop = 1, kloopmax
  ! =============================================


! Dressing, otf merging and otf reduction calls to build loop structures

  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(18)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(1),h0tab(:,1),[17,2,12],[0,nME,nME],3,2,wf4(:,9),wf1(:,1))
  call Hloop_AQ_V(ntryL,G0H8(1),wf4(:,9),G0H2(1),m3h4x2(:,1),heltab2x8(:,:,1))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(19)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(2),h0tab(:,2),[18,1,12],[0,nME,nME],3,2,wf4(:,11),wf1(:,1))
  call Hloop_QA_V(ntryL,G0H8(1),wf4(:,11),G0H2(2),m3h4x2(:,2),heltab2x8(:,:,2))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(15)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(3),h0tab(:,3),[16,2,13],[nME,0,nME],3,1,wf2(:,6))
  call Hloop_QV_A(ntryL,G0H8(1),ex5(:),G0H4(1),m3h2x4(:,1),heltab2x8(:,:,3))
  call Hloop_Q_A(ntryL,G0H4(1),16,nME,G1H4(1),n2h4(1))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(17)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(4),h0tab(:,4),[16,1,14],[nME,0,nME],3,1,wf2(:,8))
  call Hloop_AV_Q(ntryL,G0H8(1),ex5(:),G0H4(1),m3h2x4(:,2),heltab2x8(:,:,4))
  call Hloop_A_Q(ntryL,G0H4(1),16,nME,G1H4(2),n2h4(2))
  Gcoeff(:)%j = (c(1)*M(2,:)%j) * den(7)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(5),h0tab(:,5),[28,1,2],[nME,0,nME],3,1,wf2(:,3))
  call Hloop_AV_Q(ntryL,G0H8(1),wf2(:,3),G0H4(1),m3h2x4(:,3),heltab2x8(:,:,5))
  call Hloop_A_Q(ntryL,G0H4(1),28,nME,G1H4(3),n2h4(3))
  Gcoeff(:)%j = (c(1)*M(2,:)%j) * den(9)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(6),h0tab(:,6),[28,1,2],[nME,0,nME],3,1,wf2(:,4))
  call Hloop_AV_Q(ntryL,G0H8(1),wf2(:,4),G0H4(1),m3h2x4(:,4),heltab2x8(:,:,6))
  call Hloop_A_Q(ntryL,G0H4(1),28,nME,G1H4(4),n2h4(4))
  call ol_merge(ntryL,merge_step,merge_mism,merge_tables,merge_hels,G1H4(4),[G1H4(3)])
  call Hloop_VA_Q(ntryL,G0H2(1),ex2(:),G0H1(1),m3h2x1(:,1),heltab2x2(:,:,1))
  call Hloop_A_Q(ntryL,G0H1(1),19,nME,G1H1(1),n2h1(1))
  call Hloop_VQ_A(ntryL,G0H2(2),ex1(:),G0H1(1),m3h2x1(:,2),heltab2x2(:,:,2))
  call Hloop_Q_A(ntryL,G0H1(1),19,nME,G1H1(2),n2h1(2))
  call Hloop_QA_V(ntryL,G1H4(1),ex2(:),G1H2(1),m3h2x2(:,1),heltab2x4(:,:,1))
  call Hloop_AQ_V(ntryL,G1H4(2),ex1(:),G1H2(2),m3h2x2(:,2),heltab2x4(:,:,2))
  call Hloop_AQ_V(ntryL,G1H4(4),ex1(:),G1H2(3),m3h2x2(:,3),heltab2x4(:,:,3))
  call Hloop_AV_Q(ntryL,G1H1(1),wf1(:,1),G1H1(3),m3h1x1(:,1),heltab2x1(:,:,1))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(3),31,nME,G2tensor(1),n2h1(3))
  call Hloop_QV_A(ntryL,G1H1(2),wf1(:,1),G1H1(1),m3h1x1(:,2),heltab2x1(:,:,2))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(1),31,nME,G2tensor(2),n2h1(4))
  call Hloop_VQ_A(ntryL,G1H2(1),wf2(:,6),G1H1(3),m3h2x1(:,3),heltab2x2(:,:,3))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(3),31,nME,G2tensor(3),n2h1(5))
  call Hloop_VA_Q(ntryL,G1H2(2),wf2(:,8),G1H1(2),m3h2x1(:,4),heltab2x2(:,:,4))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(2),31,nME,G2tensor(4),n2h1(6))
  call Hloop_VA_Q(ntryL,G1H2(3),ex2(:),G1H1(1),m3h2x1(:,5),heltab2x2(:,:,5))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(1),31,nME,G2tensor(5),n2h1(7))
  call TI_triangle_red(G2tensor(1),RedBasis(1),mass3set(:,1),G0tensor(1),G0tensor(2),G0tensor(3),G0tensor(4),M2L1R1,[nME], &
    G0tensor(5))
  call TI_triangle_red(G2tensor(2),RedBasis(2),mass3set(:,1),G0tensor(6),G0tensor(7),G0tensor(8),G0tensor(9),M2L1R1,[nME], &
    G0tensor(10))
  call TI_triangle_red(G2tensor(3),RedBasis(3),mass3set(:,2),G0tensor(11),G0tensor(12),G0tensor(13),G0tensor(14),M2L1R1,[nME], &
    G0tensor(15))
  call TI_triangle_red(G2tensor(4),RedBasis(4),mass3set(:,2),G0tensor(16),G0tensor(17),G0tensor(18),G0tensor(19),M2L1R1,[nME], &
    G0tensor(20))
  call TI_triangle_red(G2tensor(5),RedBasis(5),mass3set(:,2),G0tensor(21),G0tensor(22),G0tensor(23),G0tensor(24),M2L1R1,[nME], &
    G0tensor(25))
  call ol_merge_tensors(T0sum(1),[G0tensor(1)])
  call ol_merge_tensors(T0sum(2),[G0tensor(6)])
  call ol_merge_tensors(T0sum(3),[G0tensor(11)])
  call ol_merge_tensors(T0sum(4),[G0tensor(16)])
  call ol_merge_tensors(T0sum(5),[G0tensor(21)])
  call ol_merge_tensors(G0tensor(23),[G0tensor(2)])
  call ol_merge_tensors(G0tensor(8),[G0tensor(3)])
  call ol_merge_tensors(G0tensor(18),[G0tensor(4)])
  call ol_merge_tensors(G0tensor(25),[G0tensor(20),G0tensor(15),G0tensor(10),G0tensor(5)])
  call ol_merge_tensors(G0tensor(13),[G0tensor(9)])
  call ol_merge_tensors(G0tensor(19),[G0tensor(14)])
  call ol_merge_tensors(G0tensor(22),[G0tensor(17)])
  call ol_merge_tensors(T0sum(6),[G0tensor(23)])
  call ol_merge_tensors(T0sum(7),[G0tensor(8)])
  call ol_merge_tensors(T0sum(8),[G0tensor(18)])
  call ol_merge_tensors(T0sum(9),[G0tensor(25)])
  call ol_merge_tensors(T0sum(10),[G0tensor(7)])
  call ol_merge_tensors(T0sum(11),[G0tensor(13)])
  call ol_merge_tensors(T0sum(12),[G0tensor(12)])
  call ol_merge_tensors(T0sum(13),[G0tensor(19)])
  call ol_merge_tensors(T0sum(14),[G0tensor(22)])
  call ol_merge_tensors(T0sum(15),[G0tensor(24)])
! end of process

! end do

end subroutine vamp_1

end module ol_vamp_1_eepipi_qed_nfl1_eexpipixa_12_/**/REALKIND
