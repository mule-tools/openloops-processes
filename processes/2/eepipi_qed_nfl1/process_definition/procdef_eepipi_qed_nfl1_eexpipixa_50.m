
FeynArtsProcess = {F[2, {1}], -F[2, {1}]} -> {-S[7], S[7], V[1]};

SortExternal = True;

OpenLoopsModel = "SMpions";

CreateTopologiesOptions = {
  ExcludeTopologies -> {Snails, WFCorrectionCTs, TadpoleCTs},
  Adjacencies -> {3, 4}
};

InsertFieldsOptions = {
  Model -> {"SMpions", "SMpionsR2"},
  GenericModel -> "Lorentz",
  InsertionLevel -> {Particles},
  Restrictions -> {ExcludeParticles -> {F[3, __], F[4, __], F[2, {3}], F[2, {2}], F[1, {3}], F[1, {2}], V[2 | 3 | 4], U[1 | 2 | 3 | 4], S[1 | 2 | 3]}, NoQuarkMixing}
};

UnitaryGauge = True;

ColourCorrelations = Automatic;

OTFColourCorrelations = Automatic;

SpinCorrelatedHardFactor = Automatic;

SubProcessName = Automatic;

SelectCoupling = MemberQ[{0}, Exponent[#1, gQCD]] & ;

SelectInterference = {
  gQCD -> {0},
  QPi -> {4},
  Qel -> {4}
};

SelectTreeDiagrams = NFieldPropagators[V[6 | 7], 0][##1] && NFieldPropagators[S[7], 1][##1] & ;

SelectLoopDiagrams = NLegsOnLoop[5][##1] && ParticlesInLoop[V[1]][##1] && ParticlesInLoop[V[6]][##1] &&  !ParticlesInLoop[V[7]][##1] &&  !ParticlesOutsideLoop[V[6 | 7]][##1] & ;

SelectCTDiagrams = False & ;

ReplaceOSw = False;

SetParameters = {
  nf -> 0,
  nfl -> 1,
  CKMORDER -> 0,
  nc -> 3,
  MU -> 0,
  MD -> 0,
  MS -> 0,
  MC -> 0,
  LeadingColour -> 0,
  POLSEL -> 1
};

ChannelMap = {};

Approximation = "L23T21N5D1";

QED = 2;

ForceLoops = Automatic;

ForceLoopsInclude = Automatic;

NonZeroHels = Null;

OnTheFlyMode = Automatic;

noQCD = False;

noEW = False;

SetExternalSubtrees = {};
