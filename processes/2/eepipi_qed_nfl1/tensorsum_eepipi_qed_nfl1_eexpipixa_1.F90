
! **********************************************************************
module ol_tables_storage_eepipi_qed_nfl1_eexpipixa_1_/**/REALKIND
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_data_types_/**/REALKIND, only: hol
  use ol_data_types_/**/REALKIND, only: basis, redset4, redset5
 use ol_data_types_/**/REALKIND, only: scalarbox 

  implicit none

  ! helicity tables for the 1-loop recursion
integer(intkind2), save :: h0tab(8,34)
integer(intkind2), save :: heltab2x1(2,1,102)
integer(intkind2), save :: heltab2x2(2,2,80)
integer(intkind2), save :: heltab2x4(2,4,44)
integer(intkind2), save :: heltab2x8(2,8,34)


  ! number of helicity states for openloops recursion steps
integer(intkind2), save :: m0h(34)
integer(intkind2), save :: m3h1x1(3,102)
integer(intkind2), save :: m3h2x1(3,68)
integer(intkind2), save :: m3h4x1(3,10)
integer(intkind2), save :: m3h8x1(3,2)
integer(intkind2), save :: m3h1x2(3,12)
integer(intkind2), save :: m3h2x2(3,29)
integer(intkind2), save :: m3h4x2(3,8)
integer(intkind2), save :: m3h1x4(3,5)
integer(intkind2), save :: m3h2x4(3,24)

integer(intkind2), save :: n2h1(72)
integer(intkind2), save :: n2h2(27)
integer(intkind2), save :: n2h4(14)


contains

!**********************************************************************
subroutine HOL_m3_init()
!**********************************************************************
! initialize m3 arrays for helicity summation
!**********************************************************************
  use KIND_TYPES, only: REALKIND, intkind2

m3h1x1(1,:)=1
m3h1x1(2,:)=1
m3h1x1(3,:)=1
m3h2x1(1,:)=2
m3h2x1(2,:)=1
m3h2x1(3,:)=2
m3h4x1(1,:)=4
m3h4x1(2,:)=1
m3h4x1(3,:)=4
m3h8x1(1,:)=8
m3h8x1(2,:)=1
m3h8x1(3,:)=8
m3h1x2(1,:)=1
m3h1x2(2,:)=2
m3h1x2(3,:)=2
m3h2x2(1,:)=2
m3h2x2(2,:)=2
m3h2x2(3,:)=4
m3h4x2(1,:)=4
m3h4x2(2,:)=2
m3h4x2(3,:)=8
m3h1x4(1,:)=1
m3h1x4(2,:)=4
m3h1x4(3,:)=4
m3h2x4(1,:)=2
m3h2x4(2,:)=4
m3h2x4(3,:)=8

n2h1(:)=1
n2h2(:)=2
n2h4(:)=4


end subroutine HOL_m3_init

!**********************************************************************

end module ol_tables_storage_eepipi_qed_nfl1_eexpipixa_1_/**/REALKIND

! **********************************************************************
module ol_tensor_sum_storage_eepipi_qed_nfl1_eexpipixa_1_/**/REALKIND
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_data_types_/**/REALKIND, only: hol, hcl, met
  use ol_data_types_/**/REALKIND, only: basis, redset4, redset5
 use ol_data_types_/**/REALKIND, only: scalarbox 

  implicit none

  type(met), save :: M2L1R1

  ! Declarations of loop wave function tensors

  type(hol), save :: G0H1(2)
  type(hol), save :: G1H1(78)
  type(hol), save :: G2H1(24)
  type(hol), save :: G0H2(2)
  type(hol), save :: G1H2(29)
  type(hol), save :: G2H2(12)
  type(hol), save :: G0H4(1)
  type(hol), save :: G1H4(14)
  type(hol), save :: G2H4(2)
  type(hol), save :: G0H8(1)
  type(hcl), save, dimension(268) :: G0tensor
  type(hcl), save, dimension(138) :: G1tensor
  type(hcl), save, dimension(88) :: G2tensor
  type(hcl), save, dimension(13) :: G3tensor



  ! Declarations for on-the-fly tensor reduction
type (basis),      save :: RedBasis(49)
type (redset4),    save :: RedSet_4(30)
type (redset5),    save :: RedSet_5(4)
integer, save :: mass2set(0:1,2)
integer, save :: mass3set(0:2,20)
integer, save :: mass4set(0:3,12)
integer, save :: mass5set(0:4,2)



  ! Declarations for TI calls

  integer, save :: momenta_1(2)
  integer, save :: momenta_2(2)
  integer, save :: momenta_3(2)
  integer, save :: momenta_4(2)
  integer, save :: momenta_5(2)
  integer, save :: momenta_6(2)
  integer, save :: momenta_7(2)
  integer, save :: momenta_8(2)
  integer, save :: momenta_9(2)
  integer, save :: momenta_10(2)
  integer, save :: momenta_11(2)
  integer, save :: momenta_12(2)
  integer, save :: momenta_13(2)
  integer, save :: momenta_14(2)
  integer, save :: momenta_15(2)
  integer, save :: momenta_16(2)
  integer, save :: momenta_17(3)
  integer, save :: momenta_18(3)
  integer, save :: momenta_19(3)
  integer, save :: momenta_20(3)
  integer, save :: momenta_21(3)
  integer, save :: momenta_22(3)
  integer, save :: momenta_23(3)
  integer, save :: momenta_24(3)
  integer, save :: momenta_25(3)
  integer, save :: momenta_26(3)
  integer, save :: momenta_27(3)
  integer, save :: momenta_28(3)
  integer, save :: momenta_29(3)
  integer, save :: momenta_30(3)
  integer, save :: momenta_31(3)
  integer, save :: momenta_32(3)
  integer, save :: momenta_33(3)
  integer, save :: momenta_34(3)
  integer, save :: momenta_35(3)
  integer, save :: momenta_36(3)
  integer, save :: momenta_37(3)
  integer, save :: momenta_38(3)
  integer, save :: momenta_39(3)
  integer, save :: momenta_40(3)
  integer, save :: momenta_41(3)
  integer, save :: momenta_42(3)
  integer, save :: momenta_43(3)
  integer, save :: momenta_44(3)
  integer, save :: momenta_45(3)
  integer, save :: momenta_46(3)
  integer, save :: momenta_47(3)
  integer, save :: momenta_48(3)
  integer, save :: momenta_49(3)
  integer, save :: momenta_50(3)
  integer, save :: momenta_51(3)
  integer, save :: momenta_52(3)
  integer, save :: momenta_53(3)
  integer, save :: momenta_54(3)
  integer, save :: momenta_55(3)
  integer, save :: momenta_56(4)
  integer, save :: momenta_57(4)
  integer, save :: momenta_58(4)
  integer, save :: momenta_59(4)
  integer, save :: momenta_60(4)
  integer, save :: momenta_61(4)
  integer, save :: momenta_62(4)
  integer, save :: momenta_63(4)
  integer, save :: momenta_64(4)
  integer, save :: momenta_65(4)
  integer, save :: momenta_66(4)
  integer, save :: momenta_67(4)
  integer, save :: momenta_68(4)
  integer, save :: momenta_69(4)
  integer, save :: momenta_70(4)
  integer, save :: momenta_71(4)
  integer, save :: momenta_72(4)
  integer, save :: momenta_73(4)
  integer, save :: momenta_74(4)
  integer, save :: momenta_75(4)
  integer, save :: momenta_76(4)
  integer, save :: momenta_77(4)
  integer, save :: momenta_78(4)
  integer, save :: momenta_79(4)
  integer, save :: momenta_80(4)
  integer, save :: momenta_81(4)
  integer, save :: momenta_82(5)
  integer, save :: momenta_83(5)
  integer, save :: momenta_84(5)
  integer, save :: momenta_85(5)

  integer, save :: masses2_1(2)
  integer, save :: masses2_2(2)
  integer, save :: masses2_3(2)
  integer, save :: masses2_4(2)
  integer, save :: masses2_5(2)
  integer, save :: masses2_6(2)
  integer, save :: masses2_7(2)
  integer, save :: masses2_8(2)
  integer, save :: masses2_9(2)
  integer, save :: masses2_10(3)
  integer, save :: masses2_11(3)
  integer, save :: masses2_12(3)
  integer, save :: masses2_13(3)
  integer, save :: masses2_14(3)
  integer, save :: masses2_15(3)
  integer, save :: masses2_16(3)
  integer, save :: masses2_17(3)
  integer, save :: masses2_18(3)
  integer, save :: masses2_19(3)
  integer, save :: masses2_20(3)
  integer, save :: masses2_21(3)
  integer, save :: masses2_22(3)
  integer, save :: masses2_23(3)
  integer, save :: masses2_24(3)
  integer, save :: masses2_25(3)
  integer, save :: masses2_26(3)
  integer, save :: masses2_27(3)
  integer, save :: masses2_28(3)
  integer, save :: masses2_29(3)
  integer, save :: masses2_30(4)
  integer, save :: masses2_31(4)
  integer, save :: masses2_32(4)
  integer, save :: masses2_33(4)
  integer, save :: masses2_34(4)
  integer, save :: masses2_35(4)
  integer, save :: masses2_36(4)
  integer, save :: masses2_37(4)
  integer, save :: masses2_38(4)
  integer, save :: masses2_39(4)
  integer, save :: masses2_40(4)
  integer, save :: masses2_41(4)
  integer, save :: masses2_42(5)
  integer, save :: masses2_43(5)

type(scalarbox), save :: ScalarBoxes(18)

integer, save :: sbarray_1(5)
integer, save :: sbarray_2(5)
integer, save :: sbarray_3(5)
integer, save :: sbarray_4(5)

  type(hcl), save, dimension(110) :: T0sum
  type(hcl), save, dimension(11) :: T1sum


  contains

!**********************************************************************
subroutine HOL_memory_allocation_full()
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_1_/**/DREALKIND, only: nhel
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_allocation
  implicit none

    call hol_allocation(4,1,4,1,G0H1,2)
  call hol_allocation(4,5,4,1,G1H1,78)
  call hol_allocation(4,15,4,1,G2H1,24)
  call hol_allocation(4,1,4,2,G0H2,2)
  call hol_allocation(4,5,4,2,G1H2,29)
  call hol_allocation(4,15,4,2,G2H2,12)
  call hol_allocation(4,1,4,4,G0H4,1)
  call hol_allocation(4,5,4,4,G1H4,14)
  call hol_allocation(4,15,4,4,G2H4,2)
  call hol_allocation(4,1,4,8,G0H8,1)


end subroutine HOL_memory_allocation_full

!**********************************************************************
subroutine HOL_memory_allocation_optimized()
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_1_/**/DREALKIND, only: nhel
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_allocation
  implicit none

    call hol_allocation(4,1,4,min(nhel,1),G0H1,2)
  call hol_allocation(4,5,4,min(nhel,1),G1H1,78)
  call hol_allocation(4,15,4,min(nhel,1),G2H1,24)
  call hol_allocation(4,1,4,min(nhel,2),G0H2,2)
  call hol_allocation(4,5,4,min(nhel,2),G1H2,29)
  call hol_allocation(4,15,4,min(nhel,2),G2H2,12)
  call hol_allocation(4,1,4,min(nhel,4),G0H4,1)
  call hol_allocation(4,5,4,min(nhel,4),G1H4,14)
  call hol_allocation(4,15,4,min(nhel,4),G2H4,2)
  call hol_allocation(4,1,4,min(nhel,8),G0H8,1)


end subroutine HOL_memory_allocation_optimized

!**********************************************************************
subroutine HOL_memory_deallocation_/**/REALKIND(dmode)
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_deallocation
  implicit none
  integer,   intent(in)    :: dmode

    call hol_deallocation(G0H1,2,dmode)
  call hol_deallocation(G1H1,78,dmode)
  call hol_deallocation(G2H1,24,dmode)
  call hol_deallocation(G0H2,2,dmode)
  call hol_deallocation(G1H2,29,dmode)
  call hol_deallocation(G2H2,12,dmode)
  call hol_deallocation(G0H4,1,dmode)
  call hol_deallocation(G1H4,14,dmode)
  call hol_deallocation(G2H4,2,dmode)
  call hol_deallocation(G0H8,1,dmode)


end subroutine HOL_memory_deallocation_/**/REALKIND

!**********************************************************************
subroutine HCL_memory_allocation()
!**********************************************************************
! allocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  implicit none

  call hcl_allocation(1,G0tensor, 268)
call hcl_allocation(5,G1tensor, 138)
call hcl_allocation(15,G2tensor, 88)
call hcl_allocation(35,G3tensor, 13)


end subroutine HCL_memory_allocation


!**********************************************************************
subroutine HCL_memory_deallocation_/**/REALKIND(dmode)
!**********************************************************************
! deallocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_deallocation
  implicit none
  integer,   intent(in)    :: dmode

  call hcl_deallocation(G0tensor, 268,dmode)
call hcl_deallocation(G1tensor, 138,dmode)
call hcl_deallocation(G2tensor, 88,dmode)
call hcl_deallocation(G3tensor, 13,dmode)

    call hcl_deallocation(T0sum,110,dmode)
  call hcl_deallocation(T1sum,11,dmode)


end subroutine HCL_memory_deallocation_/**/REALKIND


!**********************************************************************
subroutine Tsum_memory_allocation()
!**********************************************************************
! allocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  implicit none

    call hcl_allocation(1,T0sum,110)
  call hcl_allocation(5,T1sum,11)


end subroutine Tsum_memory_allocation


#ifdef PRECISION_dp
subroutine max_point(r) &
    & bind(c,name="ol_f_max_point_eepipi_qed_nfl1_eexpipixa_1")
  ! Return the number maximal tensor rank
  implicit none
  integer, intent(out) :: r
  r = 5
end subroutine max_point

subroutine tensor_rank(r) &
    & bind(c,name="ol_f_tensor_rank_eepipi_qed_nfl1_eexpipixa_1")
  ! Return the number maximal tensor rank
  implicit none
  integer, intent(out) :: r
  r = 1
end subroutine tensor_rank
#endif

subroutine reset_tensor_sum()
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  use ol_parameters_init_/**/REALKIND, only: init_hcl
  implicit none
  integer :: i

  do i = 1,110
    call init_hcl(T0sum(i))
  end do
  do i = 1,11
    call init_hcl(T1sum(i))
  end do

end subroutine reset_tensor_sum


subroutine scale_one_tsum(tsum, spow)
  use ol_parameters_decl_/**/REALKIND, only: scalefactor
  implicit none
  complex(REALKIND), intent(inout) :: tsum(:)
  integer, intent(in) :: spow ! rank 0 scale power
  real(REALKIND) :: sfinv, sfac
  integer :: sz
  sfinv = 1/scalefactor
  sfac = scalefactor**spow
  sz = size(tsum)
  tsum(1) = sfac*tsum(1)
  if (sz > 1) then ! rank 1
    sfac = sfac*sfinv
    tsum(2:5) = sfac*tsum(2:5)
  end if
  if (sz > 5) then ! rank 2
    sfac = sfac*sfinv
    tsum(6:15) = sfac*tsum(6:15)
  end if
  if (sz > 15) then ! rank 3
    sfac = sfac*sfinv
    tsum(16:35) = sfac*tsum(16:35)
  end if
  if (sz > 35) then ! rank 4
    sfac = sfac*sfinv
    tsum(36:70) = sfac*tsum(36:70)
  end if
  if (sz > 70) then ! rank 5
    sfac = sfac*sfinv
    tsum(71:126) = sfac*tsum(71:126)
  end if
  if (sz > 126) then ! rank 6
    sfac = sfac*sfinv
    tsum(127:210) = sfac*tsum(127:210)
  end if
  if (sz > 210) then ! rank 7
    sfac = sfac*sfinv
    tsum(211:330) = sfac*tsum(211:330)
  end if
end subroutine scale_one_tsum


subroutine scale_tensor_sum()
  implicit none
  call scale_one_tsum(T1sum(1)%cmp, 4)
  call scale_one_tsum(T1sum(2)%cmp, 4)
  call scale_one_tsum(T1sum(3)%cmp, 4)
  call scale_one_tsum(T1sum(4)%cmp, 4)
  call scale_one_tsum(T0sum(1)%cmp, 2)
  call scale_one_tsum(T0sum(2)%cmp, 2)
  call scale_one_tsum(T0sum(3)%cmp, 2)
  call scale_one_tsum(T0sum(4)%cmp, 2)
  call scale_one_tsum(T0sum(5)%cmp, 2)
  call scale_one_tsum(T0sum(6)%cmp, 2)
  call scale_one_tsum(T0sum(7)%cmp, 2)
  call scale_one_tsum(T0sum(8)%cmp, 2)
  call scale_one_tsum(T0sum(9)%cmp, 2)
  call scale_one_tsum(T0sum(10)%cmp, 2)
  call scale_one_tsum(T0sum(11)%cmp, 2)
  call scale_one_tsum(T0sum(12)%cmp, 2)
  call scale_one_tsum(T0sum(13)%cmp, 2)
  call scale_one_tsum(T0sum(14)%cmp, 2)
  call scale_one_tsum(T0sum(15)%cmp, 2)
  call scale_one_tsum(T0sum(16)%cmp, 2)
  call scale_one_tsum(T0sum(17)%cmp, 2)
  call scale_one_tsum(T0sum(18)%cmp, 2)
  call scale_one_tsum(T0sum(19)%cmp, 2)
  call scale_one_tsum(T0sum(20)%cmp, 2)
  call scale_one_tsum(T0sum(21)%cmp, 2)
  call scale_one_tsum(T0sum(22)%cmp, 2)
  call scale_one_tsum(T0sum(23)%cmp, 2)
  call scale_one_tsum(T0sum(24)%cmp, 2)
  call scale_one_tsum(T0sum(25)%cmp, 2)
  call scale_one_tsum(T0sum(26)%cmp, 2)
  call scale_one_tsum(T0sum(27)%cmp, 2)
  call scale_one_tsum(T0sum(28)%cmp, 2)
  call scale_one_tsum(T0sum(29)%cmp, 0)
  call scale_one_tsum(T0sum(30)%cmp, 0)
  call scale_one_tsum(T0sum(31)%cmp, 0)
  call scale_one_tsum(T0sum(32)%cmp, 0)
  call scale_one_tsum(T0sum(33)%cmp, 0)
  call scale_one_tsum(T0sum(34)%cmp, 0)
  call scale_one_tsum(T0sum(35)%cmp, 0)
  call scale_one_tsum(T0sum(36)%cmp, 0)
  call scale_one_tsum(T0sum(37)%cmp, 0)
  call scale_one_tsum(T0sum(38)%cmp, 0)
  call scale_one_tsum(T0sum(39)%cmp, 0)
  call scale_one_tsum(T0sum(40)%cmp, 0)
  call scale_one_tsum(T0sum(41)%cmp, 0)
  call scale_one_tsum(T0sum(42)%cmp, 0)
  call scale_one_tsum(T0sum(43)%cmp, 0)
  call scale_one_tsum(T0sum(44)%cmp, 0)
  call scale_one_tsum(T0sum(45)%cmp, 0)
  call scale_one_tsum(T0sum(46)%cmp, 0)
  call scale_one_tsum(T0sum(47)%cmp, 0)
  call scale_one_tsum(T0sum(48)%cmp, 0)
  call scale_one_tsum(T0sum(49)%cmp, 0)
  call scale_one_tsum(T0sum(50)%cmp, 0)
  call scale_one_tsum(T0sum(51)%cmp, 0)
  call scale_one_tsum(T0sum(52)%cmp, 0)
  call scale_one_tsum(T0sum(53)%cmp, 0)
  call scale_one_tsum(T0sum(54)%cmp, 0)
  call scale_one_tsum(T0sum(55)%cmp, 0)
  call scale_one_tsum(T0sum(56)%cmp, 0)
  call scale_one_tsum(T0sum(57)%cmp, 0)
  call scale_one_tsum(T0sum(58)%cmp, 0)
  call scale_one_tsum(T0sum(59)%cmp, 0)
  call scale_one_tsum(T0sum(60)%cmp, 0)
  call scale_one_tsum(T0sum(61)%cmp, 0)
  call scale_one_tsum(T0sum(62)%cmp, 0)
  call scale_one_tsum(T0sum(63)%cmp, 0)
  call scale_one_tsum(T0sum(64)%cmp, 0)
  call scale_one_tsum(T0sum(65)%cmp, 0)
  call scale_one_tsum(T0sum(66)%cmp, 0)
  call scale_one_tsum(T0sum(67)%cmp, 0)
  call scale_one_tsum(T0sum(68)%cmp, 0)
  call scale_one_tsum(T1sum(5)%cmp, 0)
  call scale_one_tsum(T1sum(6)%cmp, 0)
  call scale_one_tsum(T1sum(7)%cmp, 0)
  call scale_one_tsum(T1sum(8)%cmp, 0)
  call scale_one_tsum(T1sum(9)%cmp, 0)
  call scale_one_tsum(T1sum(10)%cmp, 0)
  call scale_one_tsum(T1sum(11)%cmp, 0)
  call scale_one_tsum(T0sum(69)%cmp, 0)
  call scale_one_tsum(T0sum(70)%cmp, 0)
  call scale_one_tsum(T0sum(71)%cmp, 0)
  call scale_one_tsum(T0sum(72)%cmp, 0)
  call scale_one_tsum(T0sum(73)%cmp, 0)
  call scale_one_tsum(T0sum(74)%cmp, 0)
  call scale_one_tsum(T0sum(75)%cmp, 0)
  call scale_one_tsum(T0sum(76)%cmp, 0)
  call scale_one_tsum(T0sum(77)%cmp, 0)
  call scale_one_tsum(T0sum(78)%cmp, -2)
  call scale_one_tsum(T0sum(79)%cmp, -2)
  call scale_one_tsum(T0sum(80)%cmp, -2)
  call scale_one_tsum(T0sum(81)%cmp, -2)
  call scale_one_tsum(T0sum(82)%cmp, -2)
  call scale_one_tsum(T0sum(83)%cmp, -2)
  call scale_one_tsum(T0sum(84)%cmp, -2)
  call scale_one_tsum(T0sum(85)%cmp, -2)
  call scale_one_tsum(T0sum(86)%cmp, -2)
  call scale_one_tsum(T0sum(87)%cmp, -2)
  call scale_one_tsum(T0sum(88)%cmp, -2)
  call scale_one_tsum(T0sum(89)%cmp, -2)
  call scale_one_tsum(T0sum(90)%cmp, -2)
  call scale_one_tsum(T0sum(91)%cmp, -2)
  call scale_one_tsum(T0sum(92)%cmp, -2)
  call scale_one_tsum(T0sum(93)%cmp, -2)
  call scale_one_tsum(T0sum(94)%cmp, -2)
  call scale_one_tsum(T0sum(95)%cmp, -2)
  call scale_one_tsum(T0sum(96)%cmp, -2)
  call scale_one_tsum(T0sum(97)%cmp, -2)
  call scale_one_tsum(T0sum(98)%cmp, -2)
  call scale_one_tsum(T0sum(99)%cmp, -2)
  call scale_one_tsum(T0sum(100)%cmp, -2)
  call scale_one_tsum(T0sum(101)%cmp, -2)
  call scale_one_tsum(T0sum(102)%cmp, -2)
  call scale_one_tsum(T0sum(103)%cmp, -2)
  call scale_one_tsum(T0sum(104)%cmp, -2)
  call scale_one_tsum(T0sum(105)%cmp, -2)
  call scale_one_tsum(T0sum(106)%cmp, -2)
  call scale_one_tsum(T0sum(107)%cmp, -2)
  call scale_one_tsum(T0sum(108)%cmp, -2)
  call scale_one_tsum(T0sum(109)%cmp, -2)
  call scale_one_tsum(T0sum(110)%cmp, -2)

end subroutine scale_tensor_sum

! **********************************************************************
subroutine set_integral_masses_and_momenta()
! **********************************************************************

  use ol_parameters_decl_/**/REALKIND
  momenta_1 = [ 0, 0 ]
  momenta_2 = [ 16, 15 ]
  momenta_3 = [ 17, 14 ]
  momenta_4 = [ 18, 13 ]
  momenta_5 = [ 19, 12 ]
  momenta_6 = [ 20, 11 ]
  momenta_7 = [ 21, 10 ]
  momenta_8 = [ 22, 9 ]
  momenta_9 = [ 23, 8 ]
  momenta_10 = [ 24, 7 ]
  momenta_11 = [ 25, 6 ]
  momenta_12 = [ 26, 5 ]
  momenta_13 = [ 27, 4 ]
  momenta_14 = [ 28, 3 ]
  momenta_15 = [ 29, 2 ]
  momenta_16 = [ 30, 1 ]
  momenta_17 = [ 16, 1, 14 ]
  momenta_18 = [ 16, 2, 13 ]
  momenta_19 = [ 16, 3, 12 ]
  momenta_20 = [ 16, 4, 11 ]
  momenta_21 = [ 16, 5, 10 ]
  momenta_22 = [ 16, 6, 9 ]
  momenta_23 = [ 16, 7, 8 ]
  momenta_24 = [ 16, 9, 6 ]
  momenta_25 = [ 16, 11, 4 ]
  momenta_26 = [ 16, 13, 2 ]
  momenta_27 = [ 17, 2, 12 ]
  momenta_28 = [ 17, 4, 10 ]
  momenta_29 = [ 17, 6, 8 ]
  momenta_30 = [ 17, 8, 6 ]
  momenta_31 = [ 17, 10, 4 ]
  momenta_32 = [ 17, 12, 2 ]
  momenta_33 = [ 18, 1, 12 ]
  momenta_34 = [ 18, 5, 8 ]
  momenta_35 = [ 18, 9, 4 ]
  momenta_36 = [ 19, 4, 8 ]
  momenta_37 = [ 19, 8, 4 ]
  momenta_38 = [ 20, 1, 10 ]
  momenta_39 = [ 20, 2, 9 ]
  momenta_40 = [ 20, 3, 8 ]
  momenta_41 = [ 21, 2, 8 ]
  momenta_42 = [ 21, 8, 2 ]
  momenta_43 = [ 22, 1, 8 ]
  momenta_44 = [ 24, 1, 6 ]
  momenta_45 = [ 24, 2, 5 ]
  momenta_46 = [ 24, 3, 4 ]
  momenta_47 = [ 24, 4, 3 ]
  momenta_48 = [ 24, 5, 2 ]
  momenta_49 = [ 24, 6, 1 ]
  momenta_50 = [ 25, 2, 4 ]
  momenta_51 = [ 25, 4, 2 ]
  momenta_52 = [ 26, 1, 4 ]
  momenta_53 = [ 26, 4, 1 ]
  momenta_54 = [ 28, 1, 2 ]
  momenta_55 = [ 28, 2, 1 ]
  momenta_56 = [ 16, 1, 2, 12 ]
  momenta_57 = [ 16, 1, 4, 10 ]
  momenta_58 = [ 16, 1, 8, 6 ]
  momenta_59 = [ 16, 1, 12, 2 ]
  momenta_60 = [ 16, 2, 1, 12 ]
  momenta_61 = [ 16, 3, 4, 8 ]
  momenta_62 = [ 16, 3, 8, 4 ]
  momenta_63 = [ 16, 4, 1, 10 ]
  momenta_64 = [ 16, 4, 2, 9 ]
  momenta_65 = [ 16, 4, 3, 8 ]
  momenta_66 = [ 16, 5, 2, 8 ]
  momenta_67 = [ 16, 5, 8, 2 ]
  momenta_68 = [ 16, 6, 1, 8 ]
  momenta_69 = [ 16, 9, 4, 2 ]
  momenta_70 = [ 17, 2, 4, 8 ]
  momenta_71 = [ 17, 2, 8, 4 ]
  momenta_72 = [ 17, 4, 8, 2 ]
  momenta_73 = [ 17, 8, 4, 2 ]
  momenta_74 = [ 18, 1, 4, 8 ]
  momenta_75 = [ 18, 1, 8, 4 ]
  momenta_76 = [ 20, 1, 2, 8 ]
  momenta_77 = [ 20, 2, 1, 8 ]
  momenta_78 = [ 24, 1, 2, 4 ]
  momenta_79 = [ 24, 2, 1, 4 ]
  momenta_80 = [ 24, 4, 1, 2 ]
  momenta_81 = [ 24, 4, 2, 1 ]
  momenta_82 = [ 16, 1, 4, 8, 2 ]
  momenta_83 = [ 16, 1, 8, 4, 2 ]
  momenta_84 = [ 16, 4, 1, 2, 8 ]
  momenta_85 = [ 16, 4, 2, 1, 8 ]

  masses2_1 = [ 0, 0 ]
  masses2_2 = [ nME, 0 ]
  masses2_3 = [ nMPIpm, 0 ]
  masses2_4 = [ 0, nME ]
  masses2_5 = [ nME, nME ]
  masses2_6 = [ nMPIpm, nME ]
  masses2_7 = [ 0, nMPIpm ]
  masses2_8 = [ nME, nMPIpm ]
  masses2_9 = [ nMPIpm, nMPIpm ]
  masses2_10 = [ nME, 0, 0 ]
  masses2_11 = [ nMPIpm, 0, 0 ]
  masses2_12 = [ 0, 0, nME ]
  masses2_13 = [ nME, 0, nME ]
  masses2_14 = [ nMPIpm, 0, nME ]
  masses2_15 = [ 0, 0, nMPIpm ]
  masses2_16 = [ nME, 0, nMPIpm ]
  masses2_17 = [ 0, nME, 0 ]
  masses2_18 = [ nME, nME, 0 ]
  masses2_19 = [ nMPIpm, nME, 0 ]
  masses2_20 = [ nME, nME, nME ]
  masses2_21 = [ 0, nME, nMPIpm ]
  masses2_22 = [ nME, nME, nMPIpm ]
  masses2_23 = [ 0, nMPIpm, 0 ]
  masses2_24 = [ nME, nMPIpm, 0 ]
  masses2_25 = [ nMPIpm, nMPIpm, 0 ]
  masses2_26 = [ 0, nMPIpm, nME ]
  masses2_27 = [ nMPIpm, nMPIpm, nME ]
  masses2_28 = [ 0, nMPIpm, nMPIpm ]
  masses2_29 = [ nMPIpm, nMPIpm, nMPIpm ]
  masses2_30 = [ nMPIpm, 0, nME, 0 ]
  masses2_31 = [ nME, 0, nMPIpm, 0 ]
  masses2_32 = [ nME, nME, 0, 0 ]
  masses2_33 = [ nME, nME, 0, nME ]
  masses2_34 = [ 0, nME, 0, nMPIpm ]
  masses2_35 = [ nME, nME, 0, nMPIpm ]
  masses2_36 = [ nME, nME, nMPIpm, 0 ]
  masses2_37 = [ nMPIpm, nMPIpm, 0, 0 ]
  masses2_38 = [ 0, nMPIpm, 0, nME ]
  masses2_39 = [ nMPIpm, nMPIpm, 0, nME ]
  masses2_40 = [ nMPIpm, nMPIpm, nME, 0 ]
  masses2_41 = [ nMPIpm, nMPIpm, nMPIpm, 0 ]
  masses2_42 = [ nME, nME, 0, nMPIpm, 0 ]
  masses2_43 = [ nMPIpm, nMPIpm, 0, nME, 0 ]


end subroutine  set_integral_masses_and_momenta

! **********************************************************************
subroutine integrate_tensor_sum(M2out)
! **********************************************************************
  use ol_parameters_decl_/**/REALKIND ! only: ZERO, masses
#ifndef PRECISION_dp
  use ol_parameters_decl_/**/DREALKIND, only: a_switch
#endif
  use ol_parameters_init_/**/REALKIND, only: init_met, add_met, met_to_real
  use ol_loop_routines_/**/REALKIND, only: TI_call_OL
  implicit none
  real(REALKIND), intent(out) :: M2out
  type(met) :: M2
  call init_met(M2)

sbarray_1 = [1, 2, 3, 4, 5]
sbarray_2 = [6, 7, 3, 8, 9]
sbarray_3 = [10, 11, 12, 13, 14]
sbarray_4 = [15, 16, 12, 17, 18]


#ifdef LOOPSQUARED
  if (a_switch == 1 .or. a_switch == 7) then
#endif
  call TI_call_OL(0,0, momenta_80, masses2_38, T0sum(1), M2,[5], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_63, masses2_39, T0sum(2), M2,[4], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_81, masses2_38, T0sum(3), M2,[9], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_64, masses2_39, T0sum(4), M2,[8], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_74, masses2_34, T0sum(5), M2,[14], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_57, masses2_35, T0sum(6), M2,[13], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_75, masses2_34, T0sum(7), M2,[18], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_58, masses2_35, T0sum(8), M2,[17], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_76, masses2_30, T0sum(9), M2,[1], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_66, masses2_40, T0sum(10), M2,[2], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_65, masses2_37, T0sum(11), M2,[3], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_77, masses2_30, T0sum(12), M2,[6], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_68, masses2_40, T0sum(13), M2,[7], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_72, masses2_31, T0sum(14), M2,[10], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_67, masses2_36, T0sum(15), M2,[11], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_59, masses2_32, T0sum(16), M2,[12], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_73, masses2_31, T0sum(17), M2,[15], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_69, masses2_36, T0sum(18), M2,[16], ScalarBoxes(:))
  call TI_call_OL(2,0, momenta_62, masses2_41, T0sum(19), M2)
  call TI_call_OL(2,0, momenta_61, masses2_41, T0sum(20), M2)
  call TI_call_OL(0,0, momenta_56, masses2_33, T0sum(21), M2)
  call TI_call_OL(0,0, momenta_60, masses2_33, T0sum(22), M2)
  call TI_call_OL(0,0, momenta_70, masses2_34, T0sum(23), M2,[15], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_71, masses2_34, T0sum(24), M2,[10], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_79, masses2_30, T0sum(25), M2,[5], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_78, masses2_30, T0sum(26), M2,[9], ScalarBoxes(:))
  call TI_call_OL(0,0, momenta_62, masses2_41, T0sum(27), M2)
  call TI_call_OL(0,0, momenta_61, masses2_41, T0sum(28), M2)
  call TI_call_OL(0,0, momenta_53, masses2_24, T0sum(29), M2)
  call TI_call_OL(0,0, momenta_51, masses2_24, T0sum(30), M2)
  call TI_call_OL(0,0, momenta_52, masses2_19, T0sum(31), M2)
  call TI_call_OL(0,0, momenta_43, masses2_19, T0sum(32), M2)
  call TI_call_OL(0,0, momenta_38, masses2_14, T0sum(33), M2)
  call TI_call_OL(0,0, momenta_21, masses2_27, T0sum(34), M2)
  call TI_call_OL(0,0, momenta_20, masses2_25, T0sum(35), M2)
  call TI_call_OL(0,0, momenta_39, masses2_14, T0sum(36), M2)
  call TI_call_OL(0,0, momenta_22, masses2_27, T0sum(37), M2)
  call TI_call_OL(0,0, momenta_28, masses2_16, T0sum(38), M2)
  call TI_call_OL(0,0, momenta_21, masses2_22, T0sum(39), M2)
  call TI_call_OL(0,0, momenta_17, masses2_18, T0sum(40), M2)
  call TI_call_OL(0,0, momenta_30, masses2_16, T0sum(41), M2)
  call TI_call_OL(0,0, momenta_24, masses2_22, T0sum(42), M2)
  call TI_call_OL(0,0, momenta_47, masses2_23, T0sum(43), M2)
  call TI_call_OL(0,0, momenta_19, masses2_29, T0sum(44), M2)
  call TI_call_OL(0,0, momenta_33, masses2_17, T0sum(45), M2)
  call TI_call_OL(0,0, momenta_54, masses2_12, T0sum(46), M2)
  call TI_call_OL(0,0, momenta_48, masses2_26, T0sum(47), M2)
  call TI_call_OL(0,0, momenta_55, masses2_12, T0sum(48), M2)
  call TI_call_OL(0,0, momenta_49, masses2_26, T0sum(49), M2)
  call TI_call_OL(0,0, momenta_36, masses2_15, T0sum(50), M2)
  call TI_call_OL(0,0, momenta_34, masses2_21, T0sum(51), M2)
  call TI_call_OL(0,0, momenta_37, masses2_15, T0sum(52), M2)
  call TI_call_OL(0,0, momenta_35, masses2_21, T0sum(53), M2)
  call TI_call_OL(0,0, momenta_54, masses2_18, T0sum(54), M2)
  call TI_call_OL(0,0, momenta_55, masses2_18, T0sum(55), M2)
  call TI_call_OL(0,0, momenta_18, masses2_18, T0sum(56), M2)
  call TI_call_OL(0,0, momenta_50, masses2_19, T0sum(57), M2)
  call TI_call_OL(0,0, momenta_27, masses2_17, T0sum(58), M2)
  call TI_call_OL(0,0, momenta_41, masses2_19, T0sum(59), M2)
  call TI_call_OL(0,0, momenta_45, masses2_14, T0sum(60), M2)
  call TI_call_OL(0,0, momenta_44, masses2_14, T0sum(61), M2)
  call TI_call_OL(0,0, momenta_40, masses2_28, T0sum(62), M2)
  call TI_call_OL(0,0, momenta_46, masses2_28, T0sum(63), M2)
  call TI_call_OL(0,0, momenta_40, masses2_11, T0sum(64), M2)
  call TI_call_OL(0,0, momenta_23, masses2_25, T0sum(65), M2)
  call TI_call_OL(0,0, momenta_42, masses2_24, T0sum(66), M2)
  call TI_call_OL(0,0, momenta_32, masses2_10, T0sum(67), M2)
  call TI_call_OL(0,0, momenta_26, masses2_18, T0sum(68), M2)
  call TI_call_OL(0,0, momenta_27, masses2_13, T0sum(69), M2)
  call TI_call_OL(0,0, momenta_19, masses2_20, T0sum(70), M2)
  call TI_call_OL(0,0, momenta_33, masses2_13, T0sum(71), M2)
  call TI_call_OL(0,0, momenta_29, masses2_21, T0sum(72), M2)
  call TI_call_OL(0,0, momenta_31, masses2_21, T0sum(73), M2)
  call TI_call_OL(0,0, momenta_46, masses2_11, T0sum(74), M2)
  call TI_call_OL(0,0, momenta_37, masses2_25, T0sum(75), M2)
  call TI_call_OL(0,0, momenta_25, masses2_25, T0sum(76), M2)
  call TI_call_OL(0,0, momenta_36, masses2_25, T0sum(77), M2)
  call TI_call_OL(0,0, momenta_13, masses2_7, T0sum(78), M2)
  call TI_call_OL(0,0, momenta_16, masses2_2, T0sum(79), M2)
  call TI_call_OL(0,0, momenta_12, masses2_8, T0sum(80), M2)
  call TI_call_OL(0,0, momenta_1, masses2_2, T0sum(81), M2)
  call TI_call_OL(0,0, momenta_1, masses2_3, T0sum(82), M2)
  call TI_call_OL(0,0, momenta_15, masses2_2, T0sum(83), M2)
  call TI_call_OL(0,0, momenta_11, masses2_8, T0sum(84), M2)
  call TI_call_OL(0,0, momenta_16, masses2_4, T0sum(85), M2)
  call TI_call_OL(0,0, momenta_13, masses2_3, T0sum(86), M2)
  call TI_call_OL(0,0, momenta_12, masses2_6, T0sum(87), M2)
  call TI_call_OL(0,0, momenta_9, masses2_3, T0sum(88), M2)
  call TI_call_OL(0,0, momenta_8, masses2_6, T0sum(89), M2)
  call TI_call_OL(0,0, momenta_7, masses2_6, T0sum(90), M2)
  call TI_call_OL(0,0, momenta_6, masses2_3, T0sum(91), M2)
  call TI_call_OL(0,0, momenta_2, masses2_9, T0sum(92), M2)
  call TI_call_OL(0,0, momenta_7, masses2_8, T0sum(93), M2)
  call TI_call_OL(0,0, momenta_3, masses2_2, T0sum(94), M2)
  call TI_call_OL(0,0, momenta_2, masses2_5, T0sum(95), M2)
  call TI_call_OL(0,0, momenta_14, masses2_1, T0sum(96), M2)
  call TI_call_OL(0,0, momenta_10, masses2_7, T0sum(97), M2)
  call TI_call_OL(0,0, momenta_14, masses2_9, T0sum(98), M2)
  call TI_call_OL(0,0, momenta_5, masses2_9, T0sum(99), M2)
  call TI_call_OL(0,0, momenta_5, masses2_1, T0sum(100), M2)
  call TI_call_OL(0,0, momenta_4, masses2_4, T0sum(101), M2)
  call TI_call_OL(0,0, momenta_15, masses2_4, T0sum(102), M2)
  call TI_call_OL(0,0, momenta_9, masses2_7, T0sum(103), M2)
  call TI_call_OL(0,0, momenta_14, masses2_5, T0sum(104), M2)
  call TI_call_OL(0,0, momenta_4, masses2_2, T0sum(105), M2)
  call TI_call_OL(0,0, momenta_11, masses2_6, T0sum(106), M2)
  call TI_call_OL(0,0, momenta_3, masses2_4, T0sum(107), M2)
  call TI_call_OL(0,0, momenta_10, masses2_3, T0sum(108), M2)
  call TI_call_OL(0,0, momenta_6, masses2_7, T0sum(109), M2)
  call TI_call_OL(0,0, momenta_5, masses2_5, T0sum(110), M2)

  call TI_call_OL(0,1, momenta_84, masses2_43, T1sum(1), M2,sbarray_1, ScalarBoxes(:))
  call TI_call_OL(0,1, momenta_85, masses2_43, T1sum(2), M2,sbarray_2, ScalarBoxes(:))
  call TI_call_OL(0,1, momenta_82, masses2_42, T1sum(3), M2,sbarray_3, ScalarBoxes(:))
  call TI_call_OL(0,1, momenta_83, masses2_42, T1sum(4), M2,sbarray_4, ScalarBoxes(:))
  call TI_call_OL(1,1, momenta_40, masses2_28, T1sum(5), M2)
  call TI_call_OL(1,1, momenta_37, masses2_25, T1sum(6), M2)
  call TI_call_OL(1,1, momenta_25, masses2_25, T1sum(7), M2)
  call TI_call_OL(1,1, momenta_19, masses2_29, T1sum(8), M2)
  call TI_call_OL(1,1, momenta_46, masses2_28, T1sum(9), M2)
  call TI_call_OL(1,1, momenta_36, masses2_25, T1sum(10), M2)
  call TI_call_OL(1,1, momenta_23, masses2_25, T1sum(11), M2)

  call add_met(M2,M2L1R1)

#ifdef LOOPSQUARED
  end if
#endif

  call met_to_real(M2out,M2)

#ifdef PRECISION_dp
  call HOL_memory_deallocation_/**/REALKIND(1)
  call HCL_memory_deallocation_/**/REALKIND(1)
#endif

end subroutine integrate_tensor_sum

end module ol_tensor_sum_storage_eepipi_qed_nfl1_eexpipixa_1_/**/REALKIND
