
! **********************************************************************
module ol_tables_storage_eepipi_qed_nfl1_eexpipixa_26_/**/REALKIND
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_data_types_/**/REALKIND, only: hol
  use ol_data_types_/**/REALKIND, only: basis, redset4, redset5
  implicit none

  ! helicity tables for the 1-loop recursion
integer(intkind2), save :: h0tab(8,4)
integer(intkind2), save :: heltab2x1(2,1,22)
integer(intkind2), save :: heltab2x2(2,2,4)
integer(intkind2), save :: heltab2x8(2,8,4)


  ! number of helicity states for openloops recursion steps
integer(intkind2), save :: m0h(4)
integer(intkind2), save :: m3h1x1(3,22)
integer(intkind2), save :: m3h2x1(3,4)
integer(intkind2), save :: m3h4x2(3,4)

integer(intkind2), save :: n2h1(4)
integer(intkind2), save :: n2h2(4)


contains

!**********************************************************************
subroutine HOL_m3_init()
!**********************************************************************
! initialize m3 arrays for helicity summation
!**********************************************************************
  use KIND_TYPES, only: REALKIND, intkind2

m3h1x1(1,:)=1
m3h1x1(2,:)=1
m3h1x1(3,:)=1
m3h2x1(1,:)=2
m3h2x1(2,:)=1
m3h2x1(3,:)=2
m3h4x2(1,:)=4
m3h4x2(2,:)=2
m3h4x2(3,:)=8

n2h1(:)=1
n2h2(:)=2


end subroutine HOL_m3_init

!**********************************************************************

end module ol_tables_storage_eepipi_qed_nfl1_eexpipixa_26_/**/REALKIND

! **********************************************************************
module ol_tensor_sum_storage_eepipi_qed_nfl1_eexpipixa_26_/**/REALKIND
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_data_types_/**/REALKIND, only: hol, hcl, met
  use ol_data_types_/**/REALKIND, only: basis, redset4, redset5
  implicit none

  type(met), save :: M2L1R1

  ! Declarations of loop wave function tensors

  type(hol), save :: G1H1(20)
  type(hol), save :: G2H1(4)
  type(hol), save :: G0H2(1)
  type(hol), save :: G1H2(4)
  type(hol), save :: G0H8(1)
  type(hcl), save, dimension(68) :: G0tensor
  type(hcl), save, dimension(20) :: G1tensor
  type(hcl), save, dimension(14) :: G2tensor



  ! Declarations for on-the-fly tensor reduction
type (basis),      save :: RedBasis(12)
type (redset4),    save :: RedSet_4(4)
integer, save :: mass3set(0:2,4)
integer, save :: mass4set(0:3,1)



  ! Declarations for TI calls

  integer, save :: momenta_1(2)
  integer, save :: momenta_2(2)
  integer, save :: momenta_3(2)
  integer, save :: momenta_4(2)
  integer, save :: momenta_5(2)
  integer, save :: momenta_6(2)
  integer, save :: momenta_7(2)
  integer, save :: momenta_8(2)
  integer, save :: momenta_9(2)
  integer, save :: momenta_10(2)
  integer, save :: momenta_11(2)
  integer, save :: momenta_12(2)
  integer, save :: momenta_13(3)
  integer, save :: momenta_14(3)
  integer, save :: momenta_15(3)
  integer, save :: momenta_16(3)
  integer, save :: momenta_17(3)
  integer, save :: momenta_18(3)
  integer, save :: momenta_19(3)
  integer, save :: momenta_20(3)
  integer, save :: momenta_21(3)
  integer, save :: momenta_22(3)
  integer, save :: momenta_23(3)
  integer, save :: momenta_24(3)
  integer, save :: momenta_25(4)
  integer, save :: momenta_26(4)
  integer, save :: momenta_27(4)
  integer, save :: momenta_28(4)

  integer, save :: masses2_1(2)
  integer, save :: masses2_2(2)
  integer, save :: masses2_3(2)
  integer, save :: masses2_4(2)
  integer, save :: masses2_5(2)
  integer, save :: masses2_6(2)
  integer, save :: masses2_7(3)
  integer, save :: masses2_8(3)
  integer, save :: masses2_9(3)
  integer, save :: masses2_10(3)
  integer, save :: masses2_11(4)



  type(hcl), save, dimension(31) :: T0sum


  contains

!**********************************************************************
subroutine HOL_memory_allocation_full()
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_26_/**/DREALKIND, only: nhel
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_allocation
  implicit none

    call hol_allocation(4,5,4,1,G1H1,20)
  call hol_allocation(4,15,4,1,G2H1,4)
  call hol_allocation(4,1,4,2,G0H2,1)
  call hol_allocation(4,5,4,2,G1H2,4)
  call hol_allocation(4,1,4,8,G0H8,1)


end subroutine HOL_memory_allocation_full

!**********************************************************************
subroutine HOL_memory_allocation_optimized()
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_26_/**/DREALKIND, only: nhel
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_allocation
  implicit none

    call hol_allocation(4,5,4,min(nhel,1),G1H1,20)
  call hol_allocation(4,15,4,min(nhel,1),G2H1,4)
  call hol_allocation(4,1,4,min(nhel,2),G0H2,1)
  call hol_allocation(4,5,4,min(nhel,2),G1H2,4)
  call hol_allocation(4,1,4,min(nhel,8),G0H8,1)


end subroutine HOL_memory_allocation_optimized

!**********************************************************************
subroutine HOL_memory_deallocation_/**/REALKIND(dmode)
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_deallocation
  implicit none
  integer,   intent(in)    :: dmode

    call hol_deallocation(G1H1,20,dmode)
  call hol_deallocation(G2H1,4,dmode)
  call hol_deallocation(G0H2,1,dmode)
  call hol_deallocation(G1H2,4,dmode)
  call hol_deallocation(G0H8,1,dmode)


end subroutine HOL_memory_deallocation_/**/REALKIND

!**********************************************************************
subroutine HCL_memory_allocation()
!**********************************************************************
! allocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  implicit none

  call hcl_allocation(1,G0tensor, 68)
call hcl_allocation(5,G1tensor, 20)
call hcl_allocation(15,G2tensor, 14)


end subroutine HCL_memory_allocation


!**********************************************************************
subroutine HCL_memory_deallocation_/**/REALKIND(dmode)
!**********************************************************************
! deallocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_deallocation
  implicit none
  integer,   intent(in)    :: dmode

  call hcl_deallocation(G0tensor, 68,dmode)
call hcl_deallocation(G1tensor, 20,dmode)
call hcl_deallocation(G2tensor, 14,dmode)

    call hcl_deallocation(T0sum,31,dmode)


end subroutine HCL_memory_deallocation_/**/REALKIND


!**********************************************************************
subroutine Tsum_memory_allocation()
!**********************************************************************
! allocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  implicit none

    call hcl_allocation(1,T0sum,31)


end subroutine Tsum_memory_allocation


#ifdef PRECISION_dp
subroutine max_point(r) &
    & bind(c,name="ol_f_max_point_eepipi_qed_nfl1_eexpipixa_26")
  ! Return the number maximal tensor rank
  implicit none
  integer, intent(out) :: r
  r = 4
end subroutine max_point

subroutine tensor_rank(r) &
    & bind(c,name="ol_f_tensor_rank_eepipi_qed_nfl1_eexpipixa_26")
  ! Return the number maximal tensor rank
  implicit none
  integer, intent(out) :: r
  r = 0
end subroutine tensor_rank
#endif

subroutine reset_tensor_sum()
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  use ol_parameters_init_/**/REALKIND, only: init_hcl
  implicit none
  integer :: i

  do i = 1,31
    call init_hcl(T0sum(i))
  end do

end subroutine reset_tensor_sum


subroutine scale_one_tsum(tsum, spow)
  use ol_parameters_decl_/**/REALKIND, only: scalefactor
  implicit none
  complex(REALKIND), intent(inout) :: tsum(:)
  integer, intent(in) :: spow ! rank 0 scale power
  real(REALKIND) :: sfinv, sfac
  integer :: sz
  sfinv = 1/scalefactor
  sfac = scalefactor**spow
  sz = size(tsum)
  tsum(1) = sfac*tsum(1)
  if (sz > 1) then ! rank 1
    sfac = sfac*sfinv
    tsum(2:5) = sfac*tsum(2:5)
  end if
  if (sz > 5) then ! rank 2
    sfac = sfac*sfinv
    tsum(6:15) = sfac*tsum(6:15)
  end if
  if (sz > 15) then ! rank 3
    sfac = sfac*sfinv
    tsum(16:35) = sfac*tsum(16:35)
  end if
  if (sz > 35) then ! rank 4
    sfac = sfac*sfinv
    tsum(36:70) = sfac*tsum(36:70)
  end if
  if (sz > 70) then ! rank 5
    sfac = sfac*sfinv
    tsum(71:126) = sfac*tsum(71:126)
  end if
  if (sz > 126) then ! rank 6
    sfac = sfac*sfinv
    tsum(127:210) = sfac*tsum(127:210)
  end if
  if (sz > 210) then ! rank 7
    sfac = sfac*sfinv
    tsum(211:330) = sfac*tsum(211:330)
  end if
end subroutine scale_one_tsum


subroutine scale_tensor_sum()
  implicit none
  call scale_one_tsum(T0sum(1)%cmp, 2)
  call scale_one_tsum(T0sum(2)%cmp, 2)
  call scale_one_tsum(T0sum(3)%cmp, 2)
  call scale_one_tsum(T0sum(4)%cmp, 2)
  call scale_one_tsum(T0sum(5)%cmp, 0)
  call scale_one_tsum(T0sum(6)%cmp, 0)
  call scale_one_tsum(T0sum(7)%cmp, 0)
  call scale_one_tsum(T0sum(8)%cmp, 0)
  call scale_one_tsum(T0sum(9)%cmp, 0)
  call scale_one_tsum(T0sum(10)%cmp, 0)
  call scale_one_tsum(T0sum(11)%cmp, 0)
  call scale_one_tsum(T0sum(12)%cmp, 0)
  call scale_one_tsum(T0sum(13)%cmp, 0)
  call scale_one_tsum(T0sum(14)%cmp, 0)
  call scale_one_tsum(T0sum(15)%cmp, 0)
  call scale_one_tsum(T0sum(16)%cmp, 0)
  call scale_one_tsum(T0sum(17)%cmp, -2)
  call scale_one_tsum(T0sum(18)%cmp, -2)
  call scale_one_tsum(T0sum(19)%cmp, -2)
  call scale_one_tsum(T0sum(20)%cmp, -2)
  call scale_one_tsum(T0sum(21)%cmp, -2)
  call scale_one_tsum(T0sum(22)%cmp, -2)
  call scale_one_tsum(T0sum(23)%cmp, -2)
  call scale_one_tsum(T0sum(24)%cmp, -2)
  call scale_one_tsum(T0sum(25)%cmp, -2)
  call scale_one_tsum(T0sum(26)%cmp, -2)
  call scale_one_tsum(T0sum(27)%cmp, -2)
  call scale_one_tsum(T0sum(28)%cmp, -2)
  call scale_one_tsum(T0sum(29)%cmp, -2)
  call scale_one_tsum(T0sum(30)%cmp, -2)
  call scale_one_tsum(T0sum(31)%cmp, -2)

end subroutine scale_tensor_sum

! **********************************************************************
subroutine set_integral_masses_and_momenta()
! **********************************************************************

  use ol_parameters_decl_/**/REALKIND
  momenta_1 = [ 0, 0 ]
  momenta_2 = [ 17, 14 ]
  momenta_3 = [ 18, 13 ]
  momenta_4 = [ 19, 12 ]
  momenta_5 = [ 21, 10 ]
  momenta_6 = [ 22, 9 ]
  momenta_7 = [ 23, 8 ]
  momenta_8 = [ 25, 6 ]
  momenta_9 = [ 26, 5 ]
  momenta_10 = [ 27, 4 ]
  momenta_11 = [ 29, 2 ]
  momenta_12 = [ 30, 1 ]
  momenta_13 = [ 17, 2, 12 ]
  momenta_14 = [ 17, 6, 8 ]
  momenta_15 = [ 17, 10, 4 ]
  momenta_16 = [ 18, 1, 12 ]
  momenta_17 = [ 18, 5, 8 ]
  momenta_18 = [ 18, 9, 4 ]
  momenta_19 = [ 19, 4, 8 ]
  momenta_20 = [ 19, 8, 4 ]
  momenta_21 = [ 21, 2, 8 ]
  momenta_22 = [ 22, 1, 8 ]
  momenta_23 = [ 25, 2, 4 ]
  momenta_24 = [ 26, 1, 4 ]
  momenta_25 = [ 17, 2, 4, 8 ]
  momenta_26 = [ 17, 2, 8, 4 ]
  momenta_27 = [ 18, 1, 4, 8 ]
  momenta_28 = [ 18, 1, 8, 4 ]

  masses2_1 = [ 0, 0 ]
  masses2_2 = [ nME, 0 ]
  masses2_3 = [ nMPIpm, 0 ]
  masses2_4 = [ 0, nME ]
  masses2_5 = [ nMPIpm, nME ]
  masses2_6 = [ 0, nMPIpm ]
  masses2_7 = [ 0, 0, nMPIpm ]
  masses2_8 = [ 0, nME, 0 ]
  masses2_9 = [ nMPIpm, nME, 0 ]
  masses2_10 = [ 0, nME, nMPIpm ]
  masses2_11 = [ 0, nME, 0, nMPIpm ]


end subroutine  set_integral_masses_and_momenta

! **********************************************************************
subroutine integrate_tensor_sum(M2out)
! **********************************************************************
  use ol_parameters_decl_/**/REALKIND ! only: ZERO, masses
#ifndef PRECISION_dp
  use ol_parameters_decl_/**/DREALKIND, only: a_switch
#endif
  use ol_parameters_init_/**/REALKIND, only: init_met, add_met, met_to_real
  use ol_loop_routines_/**/REALKIND, only: TI_call_OL
  implicit none
  real(REALKIND), intent(out) :: M2out
  type(met) :: M2
  call init_met(M2)



#ifdef LOOPSQUARED
  if (a_switch == 1 .or. a_switch == 7) then
#endif
  call TI_call_OL(0,0, momenta_25, masses2_11, T0sum(1), M2)
  call TI_call_OL(0,0, momenta_26, masses2_11, T0sum(2), M2)
  call TI_call_OL(0,0, momenta_27, masses2_11, T0sum(3), M2)
  call TI_call_OL(0,0, momenta_28, masses2_11, T0sum(4), M2)
  call TI_call_OL(0,0, momenta_23, masses2_9, T0sum(5), M2)
  call TI_call_OL(0,0, momenta_13, masses2_8, T0sum(6), M2)
  call TI_call_OL(0,0, momenta_21, masses2_9, T0sum(7), M2)
  call TI_call_OL(0,0, momenta_24, masses2_9, T0sum(8), M2)
  call TI_call_OL(0,0, momenta_16, masses2_8, T0sum(9), M2)
  call TI_call_OL(0,0, momenta_22, masses2_9, T0sum(10), M2)
  call TI_call_OL(0,0, momenta_19, masses2_7, T0sum(11), M2)
  call TI_call_OL(0,0, momenta_14, masses2_10, T0sum(12), M2)
  call TI_call_OL(0,0, momenta_20, masses2_7, T0sum(13), M2)
  call TI_call_OL(0,0, momenta_15, masses2_10, T0sum(14), M2)
  call TI_call_OL(0,0, momenta_17, masses2_10, T0sum(15), M2)
  call TI_call_OL(0,0, momenta_18, masses2_10, T0sum(16), M2)
  call TI_call_OL(0,0, momenta_11, masses2_4, T0sum(17), M2)
  call TI_call_OL(0,0, momenta_10, masses2_3, T0sum(18), M2)
  call TI_call_OL(0,0, momenta_8, masses2_5, T0sum(19), M2)
  call TI_call_OL(0,0, momenta_1, masses2_3, T0sum(20), M2)
  call TI_call_OL(0,0, momenta_1, masses2_2, T0sum(21), M2)
  call TI_call_OL(0,0, momenta_4, masses2_1, T0sum(22), M2)
  call TI_call_OL(0,0, momenta_2, masses2_4, T0sum(23), M2)
  call TI_call_OL(0,0, momenta_7, masses2_3, T0sum(24), M2)
  call TI_call_OL(0,0, momenta_5, masses2_5, T0sum(25), M2)
  call TI_call_OL(0,0, momenta_12, masses2_4, T0sum(26), M2)
  call TI_call_OL(0,0, momenta_9, masses2_5, T0sum(27), M2)
  call TI_call_OL(0,0, momenta_3, masses2_4, T0sum(28), M2)
  call TI_call_OL(0,0, momenta_6, masses2_5, T0sum(29), M2)
  call TI_call_OL(0,0, momenta_7, masses2_6, T0sum(30), M2)
  call TI_call_OL(0,0, momenta_10, masses2_6, T0sum(31), M2)

  call add_met(M2,M2L1R1)

#ifdef LOOPSQUARED
  end if
#endif

  call met_to_real(M2out,M2)

#ifdef PRECISION_dp
  call HOL_memory_deallocation_/**/REALKIND(1)
  call HCL_memory_deallocation_/**/REALKIND(1)
#endif

end subroutine integrate_tensor_sum

end module ol_tensor_sum_storage_eepipi_qed_nfl1_eexpipixa_26_/**/REALKIND
