
module ol_vamp_1_eepipi_qed_nfl1_eexpipixa_32_/**/REALKIND
contains

! **********************************************************************
subroutine vamp_1(M)
! P(0:3,nlegs) = incoming external momenta
! Uses tree structures 'wf', factors 'c', and denominators 'den' from loop_eepipi_qed_nfl1_eexpipixa_32.
! Sets colour stripped amplitudes A from the module loop_amplitudes_eepipi_qed_nfl1_eexpipixa_32.
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_parameters_decl_/**/DREALKIND, only: l_switch !, kloopmax
  use ol_parameters_decl_/**/QREALKIND ! masses
  use ol_vert_interface_/**/REALKIND
  use ol_prop_interface_/**/REALKIND
  use ol_last_step_/**/REALKIND
  use ol_tables_storage_eepipi_qed_nfl1_eexpipixa_32_/**/DREALKIND
  use ol_tensor_sum_storage_eepipi_qed_nfl1_eexpipixa_32_/**/REALKIND
  use ol_loop_handling_/**/REALKIND
  use ofred_reduction_/**/REALKIND, only: Hotf_4pt_reduction, Hotf_4pt_reduction_last
  use ofred_reduction_/**/REALKIND, only: Hotf_5pt_reduction, Hotf_5pt_reduction_last
  use ol_loop_reduction_/**/REALKIND, only: TI_bubble_red, TI_triangle_red

  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_32_/**/REALKIND
#ifndef PRECISION_dp
  use ol_loop_storage_eepipi_qed_nfl1_eexpipixa_32_/**/DREALKIND, only: &
    & p_switch, Hel, merge_step, merge_mism, merge_tables, merge_hels, ntryL, hel_states
#endif
  use hol_initialisation_/**/REALKIND, only: G0_hol_initialisation
  use ol_h_vert_interface_/**/REALKIND
  use ol_h_prop_interface_/**/REALKIND
  use ol_h_last_step_/**/REALKIND
  use ol_merging_/**/REALKIND, only: ol_merge, ol_merge_tensors, ol_merge_last

  implicit none

  type(Hpolcont) :: Gcoeff(hel_states)
  type(Hpolcont), intent(in) :: M(1,hel_states)
  integer :: kloop


#ifndef PRECISION_dp
  if (ntryL==1 .OR. p_switch == 1) Gcoeff(:)%hf = Hel(1:hel_states)
#else
  if (ntryL==1 .OR. p_switch == 2) Gcoeff(:)%hf = Hel(1:hel_states)
#endif

! do kloop = 1, kloopmax
  ! =============================================


! Dressing, otf merging and otf reduction calls to build loop structures

  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(2)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(1),h0tab(:,1),[20,2,1,8],[nMgam2,nME,nMgam1,nMPIpm],4,1,wf2(:,1))
  call Hloop_TS_V(ntryL,G0H8(1),0,wf2(:,1),20,G1H4(1),m3h2x4(:,1),heltab2x8(:,:,1))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(2)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(2),h0tab(:,2),[20,2,1,8],[nMgam1,nME,nMgam2,nMPIpm],4,1,wf2(:,1))
  call Hloop_TS_V(ntryL,G0H8(1),0,wf2(:,1),20,G1H4(2),m3h2x4(:,2),heltab2x8(:,:,2))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(2)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(3),h0tab(:,3),[20,1,2,8],[nMgam1,nME,nMgam2,nMPIpm],4,1,wf2(:,1))
  call Hloop_TS_V(ntryL,G0H8(1),0,wf2(:,1),20,G1H4(3),m3h2x4(:,3),heltab2x8(:,:,3))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(2)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(4),h0tab(:,4),[20,1,2,8],[nMgam2,nME,nMgam1,nMPIpm],4,1,wf2(:,1))
  call Hloop_TS_V(ntryL,G0H8(1),0,wf2(:,1),20,G1H4(4),m3h2x4(:,4),heltab2x8(:,:,4))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(4)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(5),h0tab(:,5),[24,2,1,4],[nMgam2,nME,nMgam1,nMPIpm],4,1,wf2(:,2))
  call Hloop_ST_V(ntryL,G0H8(1),0,wf2(:,2),24,G1H4(5),m3h2x4(:,5),heltab2x8(:,:,5))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(4)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(6),h0tab(:,6),[24,2,1,4],[nMgam1,nME,nMgam2,nMPIpm],4,1,wf2(:,2))
  call Hloop_ST_V(ntryL,G0H8(1),0,wf2(:,2),24,G1H4(6),m3h2x4(:,6),heltab2x8(:,:,6))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(4)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(7),h0tab(:,7),[24,1,2,4],[nMgam1,nME,nMgam2,nMPIpm],4,1,wf2(:,2))
  call Hloop_ST_V(ntryL,G0H8(1),0,wf2(:,2),24,G1H4(7),m3h2x4(:,7),heltab2x8(:,:,7))
  Gcoeff(:)%j = (-(c(1)*M(1,:)%j)) * den(4)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(8),h0tab(:,8),[24,1,2,4],[nMgam2,nME,nMgam1,nMPIpm],4,1,wf2(:,2))
  call Hloop_ST_V(ntryL,G0H8(1),0,wf2(:,2),24,G1H4(8),m3h2x4(:,8),heltab2x8(:,:,8))
  call Hloop_VA_Q(ntryL,G1H4(1),ex2(:),G1H2(1),m3h2x2(:,1),heltab2x4(:,:,1))
  call Hloop_A_Q(ntryL,G1H2(1),22,nME,G2H2(1),n2h2(1))
  call Hloop_VA_Q(ntryL,G1H4(2),ex2(:),G1H2(1),m3h2x2(:,2),heltab2x4(:,:,2))
  call Hloop_A_Q(ntryL,G1H2(1),22,nME,G2H2(2),n2h2(2))
  call Hloop_VQ_A(ntryL,G1H4(3),ex1(:),G1H2(1),m3h2x2(:,3),heltab2x4(:,:,3))
  call Hloop_Q_A(ntryL,G1H2(1),21,nME,G2H2(3),n2h2(3))
  call Hloop_VQ_A(ntryL,G1H4(4),ex1(:),G1H2(1),m3h2x2(:,4),heltab2x4(:,:,4))
  call Hloop_Q_A(ntryL,G1H2(1),21,nME,G2H2(4),n2h2(4))
  call Hloop_VA_Q(ntryL,G1H4(5),ex2(:),G1H2(1),m3h2x2(:,5),heltab2x4(:,:,5))
  call Hloop_A_Q(ntryL,G1H2(1),26,nME,G2H2(5),n2h2(5))
  call Hloop_VA_Q(ntryL,G1H4(6),ex2(:),G1H2(1),m3h2x2(:,6),heltab2x4(:,:,6))
  call Hloop_A_Q(ntryL,G1H2(1),26,nME,G2H2(6),n2h2(6))
  call Hloop_VQ_A(ntryL,G1H4(7),ex1(:),G1H2(1),m3h2x2(:,7),heltab2x4(:,:,7))
  call Hloop_Q_A(ntryL,G1H2(1),25,nME,G2H2(7),n2h2(7))
  call Hloop_VQ_A(ntryL,G1H4(8),ex1(:),G1H2(1),m3h2x2(:,8),heltab2x4(:,:,8))
  call Hloop_Q_A(ntryL,G1H2(1),25,nME,G2H2(8),n2h2(8))
  call Hloop_AQ_V(ntryL,G2H2(1),ex1(:),G2H1(1),m3h2x1(:,1),heltab2x2(:,:,1))
  call Hloop_AQ_V(ntryL,G2H2(2),ex1(:),G2H1(2),m3h2x1(:,2),heltab2x2(:,:,2))
  call Hloop_QA_V(ntryL,G2H2(3),ex2(:),G2H1(3),m3h2x1(:,3),heltab2x2(:,:,3))
  call Hloop_QA_V(ntryL,G2H2(4),ex2(:),G2H1(4),m3h2x1(:,4),heltab2x2(:,:,4))
  call Hloop_AQ_V(ntryL,G2H2(5),ex1(:),G2H1(5),m3h2x1(:,5),heltab2x2(:,:,5))
  call Hloop_AQ_V(ntryL,G2H2(6),ex1(:),G2H1(6),m3h2x1(:,6),heltab2x2(:,:,6))
  call Hloop_QA_V(ntryL,G2H2(7),ex2(:),G2H1(7),m3h2x1(:,7),heltab2x2(:,:,7))
  call Hloop_QA_V(ntryL,G2H2(8),ex2(:),G2H1(8),m3h2x1(:,8),heltab2x2(:,:,8))
  call Hotf_4pt_reduction(G2H1(1),RedSet_4(1),mass4set(:,1),  & 
G1H1(1),G1H1(2),G1H1(3),G1H1(4),G1H1(5),1)
  call HG1shiftOLR(G1H1(2),8,1)
  call Hotf_4pt_reduction(G2H1(2),RedSet_4(1),mass4set(:,2),  & 
G1H1(6),G1H1(7),G1H1(8),G1H1(9),G1H1(10),1)
  call HG1shiftOLR(G1H1(7),8,1)
  call Hotf_4pt_reduction(G2H1(3),RedSet_4(2),mass4set(:,2),  & 
G1H1(11),G1H1(12),G1H1(13),G1H1(14),G1H1(15),1)
  call HG1shiftOLR(G1H1(12),8,1)
  call Hotf_4pt_reduction(G2H1(4),RedSet_4(2),mass4set(:,1),  & 
G1H1(16),G1H1(17),G1H1(18),G1H1(19),G1H1(20),1)
  call HG1shiftOLR(G1H1(17),8,1)
  call Hotf_4pt_reduction(G2H1(5),RedSet_4(3),mass4set(:,1),  & 
G1H1(21),G1H1(22),G1H1(23),G1H1(24),G1H1(25),1)
  call HG1shiftOLR(G1H1(22),4,1)
  call Hotf_4pt_reduction(G2H1(6),RedSet_4(3),mass4set(:,2),  & 
G1H1(26),G1H1(27),G1H1(28),G1H1(29),G1H1(30),1)
  call HG1shiftOLR(G1H1(27),4,1)
  call Hotf_4pt_reduction(G2H1(7),RedSet_4(4),mass4set(:,2),  & 
G1H1(31),G1H1(32),G1H1(33),G1H1(34),G1H1(35),1)
  call HG1shiftOLR(G1H1(32),4,1)
  call Hotf_4pt_reduction(G2H1(8),RedSet_4(4),mass4set(:,1),  & 
G1H1(36),G1H1(37),G1H1(38),G1H1(39),G1H1(40),1)
  call HG1shiftOLR(G1H1(37),4,1)
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(1),23,ex4(:),8,G2tensor(1),m3h1x1(:,1),heltab2x1(:,:,1))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(2),0,ex4(:),8,G2tensor(2),m3h1x1(:,2),heltab2x1(:,:,2))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(5),23,ex4(:),8,G2tensor(3),m3h1x1(:,3),heltab2x1(:,:,3))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(6),23,ex4(:),8,G2tensor(4),m3h1x1(:,4),heltab2x1(:,:,4))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(7),0,ex4(:),8,G2tensor(5),m3h1x1(:,5),heltab2x1(:,:,5))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(10),23,ex4(:),8,G2tensor(6),m3h1x1(:,6),heltab2x1(:,:,6))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(11),23,ex4(:),8,G2tensor(7),m3h1x1(:,7),heltab2x1(:,:,7))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(12),0,ex4(:),8,G2tensor(8),m3h1x1(:,8),heltab2x1(:,:,8))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(15),23,ex4(:),8,G2tensor(9),m3h1x1(:,9),heltab2x1(:,:,9))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(16),23,ex4(:),8,G2tensor(10),m3h1x1(:,10),heltab2x1(:,:,10))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(17),0,ex4(:),8,G2tensor(11),m3h1x1(:,11),heltab2x1(:,:,11))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(20),23,ex4(:),8,G2tensor(12),m3h1x1(:,12),heltab2x1(:,:,12))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(21),27,ex3(:),4,G2tensor(13),m3h1x1(:,13),heltab2x1(:,:,13))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(22),0,ex3(:),4,G2tensor(14),m3h1x1(:,14),heltab2x1(:,:,14))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(25),27,ex3(:),4,G2tensor(15),m3h1x1(:,15),heltab2x1(:,:,15))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(26),27,ex3(:),4,G2tensor(16),m3h1x1(:,16),heltab2x1(:,:,16))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(27),0,ex3(:),4,G2tensor(17),m3h1x1(:,17),heltab2x1(:,:,17))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(30),27,ex3(:),4,G2tensor(18),m3h1x1(:,18),heltab2x1(:,:,18))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(31),27,ex3(:),4,G2tensor(19),m3h1x1(:,19),heltab2x1(:,:,19))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(32),0,ex3(:),4,G2tensor(20),m3h1x1(:,20),heltab2x1(:,:,20))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(35),27,ex3(:),4,G2tensor(21),m3h1x1(:,21),heltab2x1(:,:,21))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(36),27,ex3(:),4,G2tensor(22),m3h1x1(:,22),heltab2x1(:,:,22))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(37),0,ex3(:),4,G2tensor(23),m3h1x1(:,23),heltab2x1(:,:,23))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(40),27,ex3(:),4,G2tensor(24),m3h1x1(:,24),heltab2x1(:,:,24))
  call Hotf_4pt_reduction_last(G2tensor(1),RedSet_4(1),mass4set(:,1),  & 
G1tensor(1),G1tensor(2),G1tensor(3),G1tensor(4),G1tensor(5))
  call G_TensorShift(G1tensor(2),8)
  call Hotf_4pt_reduction_last(G2tensor(4),RedSet_4(1),mass4set(:,2),  & 
G1tensor(6),G1tensor(7),G1tensor(8),G1tensor(9),G1tensor(10))
  call G_TensorShift(G1tensor(7),8)
  call Hotf_4pt_reduction_last(G2tensor(7),RedSet_4(2),mass4set(:,2),  & 
G1tensor(11),G1tensor(12),G1tensor(13),G1tensor(14),G1tensor(15))
  call G_TensorShift(G1tensor(12),8)
  call Hotf_4pt_reduction_last(G2tensor(10),RedSet_4(2),mass4set(:,1),  & 
G1tensor(16),G1tensor(17),G1tensor(18),G1tensor(19),G1tensor(20))
  call G_TensorShift(G1tensor(17),8)
  call Hotf_4pt_reduction_last(G2tensor(13),RedSet_4(3),mass4set(:,1),  & 
G1tensor(21),G1tensor(22),G1tensor(23),G1tensor(24),G1tensor(25))
  call G_TensorShift(G1tensor(22),4)
  call Hotf_4pt_reduction_last(G2tensor(16),RedSet_4(3),mass4set(:,2),  & 
G1tensor(26),G1tensor(27),G1tensor(28),G1tensor(29),G1tensor(30))
  call G_TensorShift(G1tensor(27),4)
  call Hotf_4pt_reduction_last(G2tensor(19),RedSet_4(4),mass4set(:,2),  & 
G1tensor(31),G1tensor(32),G1tensor(33),G1tensor(34),G1tensor(35))
  call G_TensorShift(G1tensor(32),4)
  call Hotf_4pt_reduction_last(G2tensor(22),RedSet_4(4),mass4set(:,1),  & 
G1tensor(36),G1tensor(37),G1tensor(38),G1tensor(39),G1tensor(40))
  call G_TensorShift(G1tensor(37),4)
  call Hotf_4pt_reduction_last(G1tensor(1),RedSet_4(1),mass4set(:,1),  & 
G0tensor(1),G0tensor(2),G0tensor(3),G0tensor(4),G0tensor(5))
  call Hotf_4pt_reduction_last(G1tensor(6),RedSet_4(1),mass4set(:,2),  & 
G0tensor(6),G0tensor(7),G0tensor(8),G0tensor(9),G0tensor(10))
  call Hotf_4pt_reduction_last(G1tensor(11),RedSet_4(2),mass4set(:,2),  & 
G0tensor(11),G0tensor(12),G0tensor(13),G0tensor(14),G0tensor(15))
  call Hotf_4pt_reduction_last(G1tensor(16),RedSet_4(2),mass4set(:,1),  & 
G0tensor(16),G0tensor(17),G0tensor(18),G0tensor(19),G0tensor(20))
  call Hotf_4pt_reduction_last(G1tensor(21),RedSet_4(3),mass4set(:,1),  & 
G0tensor(21),G0tensor(22),G0tensor(23),G0tensor(24),G0tensor(25))
  call Hotf_4pt_reduction_last(G1tensor(26),RedSet_4(3),mass4set(:,2),  & 
G0tensor(26),G0tensor(27),G0tensor(28),G0tensor(29),G0tensor(30))
  call Hotf_4pt_reduction_last(G1tensor(31),RedSet_4(4),mass4set(:,2),  & 
G0tensor(31),G0tensor(32),G0tensor(33),G0tensor(34),G0tensor(35))
  call Hotf_4pt_reduction_last(G1tensor(36),RedSet_4(4),mass4set(:,1),  & 
G0tensor(36),G0tensor(37),G0tensor(38),G0tensor(39),G0tensor(40))
  call ol_merge_tensors(T0sum(1),[G0tensor(1)])
  call ol_merge_tensors(T0sum(2),[G0tensor(6)])
  call ol_merge_tensors(T0sum(3),[G0tensor(11)])
  call ol_merge_tensors(T0sum(4),[G0tensor(16)])
  call ol_merge_tensors(T0sum(5),[G0tensor(21)])
  call ol_merge_tensors(T0sum(6),[G0tensor(26)])
  call ol_merge_tensors(T0sum(7),[G0tensor(31)])
  call ol_merge_tensors(T0sum(8),[G0tensor(36)])
  call ol_merge(ntryL,merge_step,merge_mism,merge_tables,merge_hels,G1H1(19),[G1H1(4)])
  call ol_merge(ntryL,merge_step,merge_mism,merge_tables,merge_hels,G1H1(14),[G1H1(9)])
  call ol_merge(ntryL,merge_step,merge_mism,merge_tables,merge_hels,G1H1(39),[G1H1(24)])
  call ol_merge(ntryL,merge_step,merge_mism,merge_tables,merge_hels,G1H1(34),[G1H1(29)])
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(3),23,ex4(:),8,G2tensor(1),m3h1x1(:,25),heltab2x1(:,:,25))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(19),23,ex4(:),8,G2tensor(4),m3h1x1(:,26),heltab2x1(:,:,26))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(8),23,ex4(:),8,G2tensor(7),m3h1x1(:,27),heltab2x1(:,:,27))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(14),23,ex4(:),8,G2tensor(10),m3h1x1(:,28),heltab2x1(:,:,28))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(13),23,ex4(:),8,G2tensor(13),m3h1x1(:,29),heltab2x1(:,:,29))
  call Hcheck_last_VT_S(ntryL,l_switch,G1H1(18),23,ex4(:),8,G2tensor(16),m3h1x1(:,30),heltab2x1(:,:,30))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(23),27,ex3(:),4,G2tensor(19),m3h1x1(:,31),heltab2x1(:,:,31))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(39),27,ex3(:),4,G2tensor(22),m3h1x1(:,32),heltab2x1(:,:,32))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(28),27,ex3(:),4,G2tensor(25),m3h1x1(:,33),heltab2x1(:,:,33))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(34),27,ex3(:),4,G2tensor(26),m3h1x1(:,34),heltab2x1(:,:,34))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(33),27,ex3(:),4,G2tensor(27),m3h1x1(:,35),heltab2x1(:,:,35))
  call Hcheck_last_VS_T(ntryL,l_switch,G1H1(38),27,ex3(:),4,G2tensor(28),m3h1x1(:,36),heltab2x1(:,:,36))
  call ol_merge_tensors(G2tensor(14),[G2tensor(2),G1tensor(22),G1tensor(2),G0tensor(22),G0tensor(2)])
  call ol_merge_tensors(G2tensor(3),[G1tensor(5),G0tensor(5)])
  call ol_merge_tensors(G2tensor(17),[G2tensor(5),G1tensor(27),G1tensor(7),G0tensor(27),G0tensor(7)])
  call ol_merge_tensors(G2tensor(6),[G1tensor(10),G0tensor(10)])
  call ol_merge_tensors(G2tensor(20),[G2tensor(8),G1tensor(32),G1tensor(12),G0tensor(32),G0tensor(12)])
  call ol_merge_tensors(G2tensor(9),[G1tensor(15),G0tensor(15)])
  call ol_merge_tensors(G2tensor(23),[G2tensor(11),G1tensor(37),G1tensor(17),G0tensor(37),G0tensor(17)])
  call ol_merge_tensors(G2tensor(12),[G1tensor(20),G0tensor(20)])
  call ol_merge_tensors(G2tensor(15),[G1tensor(25),G0tensor(25)])
  call ol_merge_tensors(G2tensor(18),[G1tensor(30),G0tensor(30)])
  call ol_merge_tensors(G2tensor(21),[G1tensor(35),G0tensor(35)])
  call ol_merge_tensors(G2tensor(24),[G1tensor(40),G0tensor(40)])
  call ol_merge_tensors(G2tensor(1),[G1tensor(3),G0tensor(3)])
  call ol_merge_tensors(G2tensor(4),[G1tensor(19),G1tensor(4),G0tensor(19),G0tensor(4)])
  call ol_merge_tensors(G2tensor(7),[G1tensor(8),G0tensor(8)])
  call ol_merge_tensors(G2tensor(10),[G1tensor(14),G1tensor(9),G0tensor(14),G0tensor(9)])
  call ol_merge_tensors(G2tensor(13),[G1tensor(13),G0tensor(13)])
  call ol_merge_tensors(G2tensor(16),[G1tensor(18),G0tensor(18)])
  call ol_merge_tensors(G2tensor(19),[G1tensor(23),G0tensor(23)])
  call ol_merge_tensors(G2tensor(22),[G1tensor(39),G1tensor(24),G0tensor(39),G0tensor(24)])
  call ol_merge_tensors(G2tensor(25),[G1tensor(28),G0tensor(28)])
  call ol_merge_tensors(G2tensor(26),[G1tensor(34),G1tensor(29),G0tensor(34),G0tensor(29)])
  call ol_merge_tensors(G2tensor(27),[G1tensor(33),G0tensor(33)])
  call ol_merge_tensors(G2tensor(28),[G1tensor(38),G0tensor(38)])
  call TI_triangle_red(G2tensor(14),RedBasis(11),mass3set(:,1),G0tensor(1),G0tensor(6),G0tensor(11), &
    G0tensor(16),M2L1R1,[nMgam1,nMgam2,nME],G0tensor(21),G0tensor(26),G0tensor(31))
  call TI_triangle_red(G2tensor(3),RedBasis(1),mass3set(:,2),G0tensor(36),G0tensor(22),G0tensor(2), &
    G0tensor(5),M2L1R1,[nMPIpm,nMgam2,nME],G0tensor(27),G0tensor(7),G0tensor(10))
  call TI_triangle_red(G2tensor(17),RedBasis(11),mass3set(:,3),G0tensor(32),G0tensor(12),G0tensor(15), &
    G0tensor(37),M2L1R1,[nMgam2,nMgam1,nME],G0tensor(17),G0tensor(20),G0tensor(25))
  call TI_triangle_red(G2tensor(6),RedBasis(1),mass3set(:,4),G0tensor(30),G0tensor(35),G0tensor(40), &
    G0tensor(3),M2L1R1,[nMPIpm,nMgam1,nME],G0tensor(19),G0tensor(4),G0tensor(8))
  call TI_triangle_red(G2tensor(20),RedBasis(12),mass3set(:,3),G0tensor(14),G0tensor(9),G0tensor(13), &
    G0tensor(18),M2L1R1,[nMgam2,nMgam1,nME],G0tensor(23),G0tensor(39),G0tensor(24))
  call TI_triangle_red(G2tensor(9),RedBasis(4),mass3set(:,4),G0tensor(28),G0tensor(34),G0tensor(29), &
    G0tensor(33),M2L1R1,[nMPIpm,nMgam1,nME],G0tensor(38),G0tensor(41),G0tensor(42))
  call TI_triangle_red(G2tensor(23),RedBasis(12),mass3set(:,1),G0tensor(43),G0tensor(44),G0tensor(45), &
    G0tensor(46),M2L1R1,[nMgam1,nMgam2,nME],G0tensor(47),G0tensor(48),G0tensor(49))
  call TI_triangle_red(G2tensor(12),RedBasis(4),mass3set(:,2),G0tensor(50),G0tensor(51),G0tensor(52), &
    G0tensor(53),M2L1R1,[nMPIpm,nMgam2,nME],G0tensor(54),G0tensor(55),G0tensor(56))
  call TI_triangle_red(G2tensor(15),RedBasis(6),mass3set(:,2),G0tensor(57),G0tensor(58),G0tensor(59), &
    G0tensor(60),M2L1R1,[nMPIpm,nMgam2,nME],G0tensor(61),G0tensor(62),G0tensor(63))
  call TI_triangle_red(G2tensor(18),RedBasis(6),mass3set(:,4),G0tensor(64),G0tensor(65),G0tensor(66), &
    G0tensor(67),M2L1R1,[nMPIpm,nMgam1,nME],G0tensor(68),G0tensor(69),G0tensor(70))
  call TI_triangle_red(G2tensor(21),RedBasis(9),mass3set(:,4),G0tensor(71),G0tensor(72),G0tensor(73), &
    G0tensor(74),M2L1R1,[nMPIpm,nMgam1,nME],G0tensor(75),G0tensor(76),G0tensor(77))
  call TI_triangle_red(G2tensor(24),RedBasis(9),mass3set(:,2),G0tensor(78),G0tensor(79),G0tensor(80), &
    G0tensor(81),M2L1R1,[nMPIpm,nMgam2,nME],G0tensor(82),G0tensor(83),G0tensor(84))
  call TI_triangle_red(G2tensor(1),RedBasis(3),mass3set(:,5),G0tensor(85),G0tensor(86),G0tensor(87), &
    G0tensor(88),M2L1R1,[nMPIpm,nME,nMgam1],G0tensor(89),G0tensor(90),G0tensor(91))
  call TI_triangle_red(G2tensor(4),RedBasis(2),mass3set(:,6),G0tensor(92),G0tensor(93),G0tensor(94), &
    G0tensor(95),M2L1R1,[nMPIpm,nMgam2,nMgam1],G0tensor(96),G0tensor(97),G0tensor(98))
  call TI_triangle_red(G2tensor(7),RedBasis(3),mass3set(:,7),G0tensor(99),G0tensor(100),G0tensor(101), &
    G0tensor(102),M2L1R1,[nMPIpm,nME,nMgam2],G0tensor(103),G0tensor(104),G0tensor(105))
  call TI_triangle_red(G2tensor(10),RedBasis(2),mass3set(:,8),G0tensor(106),G0tensor(107),G0tensor(108), &
    G0tensor(109),M2L1R1,[nMPIpm,nMgam1,nMgam2],G0tensor(110),G0tensor(111),G0tensor(112))
  call TI_triangle_red(G2tensor(13),RedBasis(5),mass3set(:,7),G0tensor(113),G0tensor(114),G0tensor(115), &
    G0tensor(116),M2L1R1,[nMPIpm,nME,nMgam2],G0tensor(117),G0tensor(118),G0tensor(119))
  call TI_triangle_red(G2tensor(16),RedBasis(5),mass3set(:,5),G0tensor(120),G0tensor(121),G0tensor(122), &
    G0tensor(123),M2L1R1,[nMPIpm,nME,nMgam1],G0tensor(124),G0tensor(125),G0tensor(126))
  call TI_triangle_red(G2tensor(19),RedBasis(8),mass3set(:,5),G0tensor(127),G0tensor(128),G0tensor(129), &
    G0tensor(130),M2L1R1,[nMPIpm,nME,nMgam1],G0tensor(131),G0tensor(132),G0tensor(133))
  call TI_triangle_red(G2tensor(22),RedBasis(7),mass3set(:,6),G0tensor(134),G0tensor(135),G0tensor(136), &
    G0tensor(137),M2L1R1,[nMPIpm,nMgam2,nMgam1],G0tensor(138),G0tensor(139),G0tensor(140))
  call TI_triangle_red(G2tensor(25),RedBasis(8),mass3set(:,7),G0tensor(141),G0tensor(142),G0tensor(143), &
    G0tensor(144),M2L1R1,[nMPIpm,nME,nMgam2],G0tensor(145),G0tensor(146),G0tensor(147))
  call TI_triangle_red(G2tensor(26),RedBasis(7),mass3set(:,8),G0tensor(148),G0tensor(149),G0tensor(150), &
    G0tensor(151),M2L1R1,[nMPIpm,nMgam1,nMgam2],G0tensor(152),G0tensor(153),G0tensor(154))
  call TI_triangle_red(G2tensor(27),RedBasis(10),mass3set(:,7),G0tensor(155),G0tensor(156),G0tensor(157), &
    G0tensor(158),M2L1R1,[nMPIpm,nME,nMgam2],G0tensor(159),G0tensor(160),G0tensor(161))
  call TI_triangle_red(G2tensor(28),RedBasis(10),mass3set(:,5),G0tensor(162),G0tensor(163),G0tensor(164), &
    G0tensor(165),M2L1R1,[nMPIpm,nME,nMgam1],G0tensor(166),G0tensor(167),G0tensor(168))
  call ol_merge_tensors(T0sum(9),[G0tensor(1)])
  call ol_merge_tensors(T0sum(10),[G0tensor(36)])
  call ol_merge_tensors(T0sum(11),[G0tensor(32)])
  call ol_merge_tensors(T0sum(12),[G0tensor(30)])
  call ol_merge_tensors(T0sum(13),[G0tensor(14)])
  call ol_merge_tensors(T0sum(14),[G0tensor(28)])
  call ol_merge_tensors(T0sum(15),[G0tensor(43)])
  call ol_merge_tensors(T0sum(16),[G0tensor(50)])
  call ol_merge_tensors(T0sum(17),[G0tensor(57)])
  call ol_merge_tensors(T0sum(18),[G0tensor(64)])
  call ol_merge_tensors(T0sum(19),[G0tensor(71)])
  call ol_merge_tensors(T0sum(20),[G0tensor(78)])
  call ol_merge_tensors(T0sum(21),[G0tensor(85)])
  call ol_merge_tensors(T0sum(22),[G0tensor(92)])
  call ol_merge_tensors(T0sum(23),[G0tensor(99)])
  call ol_merge_tensors(T0sum(24),[G0tensor(106)])
  call ol_merge_tensors(T0sum(25),[G0tensor(113)])
  call ol_merge_tensors(T0sum(26),[G0tensor(120)])
  call ol_merge_tensors(T0sum(27),[G0tensor(127)])
  call ol_merge_tensors(T0sum(28),[G0tensor(134)])
  call ol_merge_tensors(T0sum(29),[G0tensor(141)])
  call ol_merge_tensors(T0sum(30),[G0tensor(148)])
  call ol_merge_tensors(T0sum(31),[G0tensor(155)])
  call ol_merge_tensors(T0sum(32),[G0tensor(162)])
  call ol_merge_tensors(G0tensor(58),[G0tensor(22),G0tensor(6)])
  call ol_merge_tensors(G0tensor(128),[G0tensor(86),G0tensor(11)])
  call ol_merge_tensors(G0tensor(135),[G0tensor(93),G0tensor(46),G0tensor(16)])
  call ol_merge_tensors(G0tensor(168),[G0tensor(153),G0tensor(140),G0tensor(133),G0tensor(126),G0tensor(111),G0tensor(98) &
    ,G0tensor(91),G0tensor(76),G0tensor(69),G0tensor(47),G0tensor(41),G0tensor(39),G0tensor(4),G0tensor(20),G0tensor(21)])
  call ol_merge_tensors(G0tensor(161),[G0tensor(154),G0tensor(147),G0tensor(139),G0tensor(119),G0tensor(112),G0tensor(105) &
    ,G0tensor(97),G0tensor(83),G0tensor(62),G0tensor(55),G0tensor(48),G0tensor(23),G0tensor(17),G0tensor(7),G0tensor(26)])
  call ol_merge_tensors(G0tensor(167),[G0tensor(160),G0tensor(146),G0tensor(132),G0tensor(125),G0tensor(118),G0tensor(104) &
    ,G0tensor(90),G0tensor(84),G0tensor(77),G0tensor(70),G0tensor(63),G0tensor(56),G0tensor(49),G0tensor(42),G0tensor(24) &
    ,G0tensor(8),G0tensor(25),G0tensor(10),G0tensor(31)])
  call ol_merge_tensors(G0tensor(102),[G0tensor(88),G0tensor(40),G0tensor(2)])
  call ol_merge_tensors(G0tensor(95),[G0tensor(53),G0tensor(5)])
  call ol_merge_tensors(G0tensor(166),[G0tensor(159),G0tensor(152),G0tensor(145),G0tensor(138),G0tensor(131),G0tensor(124) &
    ,G0tensor(117),G0tensor(110),G0tensor(103),G0tensor(96),G0tensor(89),G0tensor(82),G0tensor(75),G0tensor(68),G0tensor(61) &
    ,G0tensor(54),G0tensor(38),G0tensor(19),G0tensor(27)])
  call ol_merge_tensors(G0tensor(65),[G0tensor(35),G0tensor(12)])
  call ol_merge_tensors(G0tensor(142),[G0tensor(100),G0tensor(15)])
  call ol_merge_tensors(G0tensor(149),[G0tensor(107),G0tensor(18),G0tensor(37)])
  call ol_merge_tensors(G0tensor(109),[G0tensor(33),G0tensor(3)])
  call ol_merge_tensors(G0tensor(72),[G0tensor(34),G0tensor(9)])
  call ol_merge_tensors(G0tensor(156),[G0tensor(114),G0tensor(13)])
  call ol_merge_tensors(G0tensor(123),[G0tensor(116),G0tensor(52),G0tensor(29)])
  call ol_merge_tensors(G0tensor(79),[G0tensor(51),G0tensor(44)])
  call ol_merge_tensors(G0tensor(163),[G0tensor(121),G0tensor(45)])
  call ol_merge_tensors(G0tensor(144),[G0tensor(130),G0tensor(66),G0tensor(59)])
  call ol_merge_tensors(G0tensor(137),[G0tensor(81),G0tensor(60)])
  call ol_merge_tensors(G0tensor(151),[G0tensor(74),G0tensor(67)])
  call ol_merge_tensors(G0tensor(165),[G0tensor(158),G0tensor(80),G0tensor(73)])
  call ol_merge_tensors(G0tensor(122),[G0tensor(94),G0tensor(87)])
  call ol_merge_tensors(G0tensor(115),[G0tensor(108),G0tensor(101)])
  call ol_merge_tensors(G0tensor(164),[G0tensor(136),G0tensor(129)])
  call ol_merge_tensors(G0tensor(157),[G0tensor(150),G0tensor(143)])
  call ol_merge_tensors(T0sum(33),[G0tensor(58)])
  call ol_merge_tensors(T0sum(34),[G0tensor(128)])
  call ol_merge_tensors(T0sum(35),[G0tensor(135)])
  call ol_merge_tensors(T0sum(36),[G0tensor(168)])
  call ol_merge_tensors(T0sum(37),[G0tensor(161)])
  call ol_merge_tensors(T0sum(38),[G0tensor(167)])
  call ol_merge_tensors(T0sum(39),[G0tensor(102)])
  call ol_merge_tensors(T0sum(40),[G0tensor(95)])
  call ol_merge_tensors(T0sum(41),[G0tensor(166)])
  call ol_merge_tensors(T0sum(42),[G0tensor(65)])
  call ol_merge_tensors(T0sum(43),[G0tensor(142)])
  call ol_merge_tensors(T0sum(44),[G0tensor(149)])
  call ol_merge_tensors(T0sum(45),[G0tensor(109)])
  call ol_merge_tensors(T0sum(46),[G0tensor(72)])
  call ol_merge_tensors(T0sum(47),[G0tensor(156)])
  call ol_merge_tensors(T0sum(48),[G0tensor(123)])
  call ol_merge_tensors(T0sum(49),[G0tensor(79)])
  call ol_merge_tensors(T0sum(50),[G0tensor(163)])
  call ol_merge_tensors(T0sum(51),[G0tensor(144)])
  call ol_merge_tensors(T0sum(52),[G0tensor(137)])
  call ol_merge_tensors(T0sum(53),[G0tensor(151)])
  call ol_merge_tensors(T0sum(54),[G0tensor(165)])
  call ol_merge_tensors(T0sum(55),[G0tensor(122)])
  call ol_merge_tensors(T0sum(56),[G0tensor(115)])
  call ol_merge_tensors(T0sum(57),[G0tensor(164)])
  call ol_merge_tensors(T0sum(58),[G0tensor(157)])
! end of process

! end do

end subroutine vamp_1

end module ol_vamp_1_eepipi_qed_nfl1_eexpipixa_32_/**/REALKIND
