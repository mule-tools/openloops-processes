SelectInterference = {qQCD -> 0};

UnitaryGauge = False;
noQCD = True;

(* ew (QED=0) or pure qed (QED=2) or pure weak (QED=3, including Higgs) corrections, no tau*)
(* ew includes photon self-energies *)
(* qed excludes photon self-energies *)
(* other fermion loop diagrams are included, e.g. light-by-light boxes *)

lepew02 = {
  QED -> 0,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}]
};

lepqed02 = {
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}]
};

lepweak02 = {
  QED -> 3,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}]
};

lepew01 = {
  QED -> 0,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

lepqed01 = {
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

lepweak01 = {
  QED -> 3,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

(* e and mu in loop *)

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepew02];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepew02];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepew02];
(* mumuxeexaaa *) AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1], V[1]}, Sequence @@ lepew02];

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepqed02];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepqed02];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepqed02];
(* mumuxeexaaa *) AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1], V[1]}, Sequence @@ lepqed02];

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepweak02];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepweak02];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepweak02];
(* mumuxeexaaa *) AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1], V[1]}, Sequence @@ lepweak02];

(* only e in loop -> electronic corrections *)

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepew01];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepew01];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepew01];
(* mumuxeexaaa *) AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1], V[1]}, Sequence @@ lepew01];

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepqed01];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepqed01];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepqed01];
(* mumuxeexaaa *) AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1], V[1]}, Sequence @@ lepqed01];

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepweak01];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepweak01];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepweak01];
(* mumuxeexaaa *) AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1], V[1]}, Sequence @@ lepweak01];
