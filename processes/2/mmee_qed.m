(* fermion loops are excluded *)

Approximation = "nofermionloops";

SelectInterference = {qQCD -> 0};

qed01 = {
  InsertFieldsOptions -> {Restrictions -> {ExcludeParticles -> {V[2 | 3 | 4 | 5],U[__],S[1 | 2 | 3], F[1, __], F[2, {2}], F[2, {3}], F[4, __], F[3, __]}}},
  SelectLoopDiagrams -> ( Not[ContainsFermionLoop[##] && NLegsOnLoop[2][##]] && ParticlesInLoop[V[1]][##] &), 
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

qed02 = {
  InsertFieldsOptions -> {Restrictions -> {ExcludeParticles -> {V[2 | 3 | 4 | 5],U[__],S[1 | 2 | 3], F[1, __], F[2, {3}], F[4, __], F[3, __]}}},
  SelectLoopDiagrams -> ( Not[ContainsFermionLoop[##] && NLegsOnLoop[2][##]] && ParticlesInLoop[V[1]][##] &), 
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}]
};



(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ qed01];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ qed01];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed01];

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ qed02];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ qed02];
(* mumuxeexaa *)  AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ qed02];
