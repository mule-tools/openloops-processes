(* fermion loops are excluded *)

Approximation = "nofermionloops";

SelectInterference = {qQCD -> 0};

qed01 = {
  InsertFieldsOptions -> {Restrictions -> {ExcludeParticles -> {V[2 | 3 | 4 | 5],U[__],S[1 | 2 | 3], F[1, __], F[2, {2}], F[2, {3}], F[4, __], F[3, __]}}},
  SelectLoopDiagrams -> ( Not[ContainsFermionLoop[##] && NLegsOnLoop[2][##]] && ParticlesInLoop[V[1]][##] &), 
  QED -> 2
};

SetParameters = JoinOptions[{ME -> ME, nf -> 0, nfl -> 1}];

(* eexaa *)    AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1], V[1]}, Sequence @@ qed01];
(* eexaaa *)   AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1], V[1], V[1]}, Sequence @@ qed01];
(* eexaaaa *)  AddProcess[FeynArtsProcess -> {F[2,{1}], -F[2,{1}]} -> {V[1], V[1], V[1], V[1]}, Sequence @@ qed01];
