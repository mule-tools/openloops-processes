
FeynArtsProcess = {F[2, {1}], -F[2, {1}]} -> {-F[2, {2}], F[2, {2}]};

SortExternal = True;

OpenLoopsModel = "SM";

CreateTopologiesOptions = {
  ExcludeTopologies -> {Snails, WFCorrectionCTs, TadpoleCTs},
  Adjacencies -> {3, 4}
};

InsertFieldsOptions = {
  Model -> {"SMQCD", "SMQCDR2"},
  GenericModel -> "Lorentz",
  InsertionLevel -> {Particles},
  Restrictions -> {ExcludeParticles -> {F[3, __], F[4, __], F[2, {3}], F[1, {3}], V[4], V[5], U[5]}, NoQuarkMixing}
};

UnitaryGauge = False;

ColourCorrelations = Automatic;

OTFColourCorrelations = Automatic;

SpinCorrelatedHardFactor = Automatic;

SubProcessName = Automatic;

SelectCoupling = True & ;

SelectInterference = {
  qQCD -> {0}
};

SelectTreeDiagrams = True & ;

SelectLoopDiagrams = True & ;

SelectCTDiagrams = True & ;

ReplaceOSw = False;

SetParameters = {
  nf -> 0,
  nfl -> 2,
  CKMORDER -> 0,
  nc -> 3,
  MU -> 0,
  MD -> 0,
  MS -> 0,
  MC -> 0,
  LeadingColour -> 0,
  POLSEL -> 1
};

ChannelMap = {};

Approximation = "";

QED = 0;

ForceLoops = Automatic;

ForceLoopsInclude = Automatic;

NonZeroHels = Null;

OnTheFlyMode = Automatic;

noQCD = True;

noEW = False;
