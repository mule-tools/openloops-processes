
! **********************************************************************
module ol_tables_storage_mmee_lew_eexmmxa_6_/**/REALKIND
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_data_types_/**/REALKIND, only: hol
  use ol_data_types_/**/REALKIND, only: basis, redset4, redset5
  implicit none

  ! helicity tables for the 1-loop recursion
integer(intkind2), save :: h0tab(32,555)
integer(intkind2), save :: heltab2x2(2,2,34)
integer(intkind2), save :: heltab2x4(2,4,201)
integer(intkind2), save :: heltab2x8(2,8,189)
integer(intkind2), save :: heltab2x16(2,16,221)
integer(intkind2), save :: heltab3x16(3,16,10)
integer(intkind2), save :: heltab2x32(2,32,517)
integer(intkind2), save :: heltab3x32(3,32,38)


  ! number of helicity states for openloops recursion steps
integer(intkind2), save :: m0h(555)
integer(intkind2), save :: m3h2x1(3,34)
integer(intkind2), save :: m3h4x1(3,197)
integer(intkind2), save :: m3h8x1(3,60)
integer(intkind2), save :: m3h2x2(3,4)
integer(intkind2), save :: m3h4x2(3,21)
integer(intkind2), save :: m3h2x4(3,108)
integer(intkind2), save :: m3h4x4(3,104)
integer(intkind2), save :: m3h8x4(3,134)
integer(intkind2), save :: m3h2x8(3,117)
integer(intkind2), save :: m3h4x8(3,98)
integer(intkind2), save :: m3h2x16(3,285)
integer(intkind2), save :: m4h4x4x1(4,10)
integer(intkind2), save :: m4h4x2x4(4,2)
integer(intkind2), save :: m4h2x4x4(4,36)

integer(intkind2), save :: n2h1(129)
integer(intkind2), save :: n2h2(12)
integer(intkind2), save :: n2h4(78)
integer(intkind2), save :: n2h8(131)
integer(intkind2), save :: n2h16(67)


contains

!**********************************************************************
subroutine HOL_m3_init()
!**********************************************************************
! initialize m3 arrays for helicity summation
!**********************************************************************
  use KIND_TYPES, only: REALKIND, intkind2

m3h2x1(1,:)=2
m3h2x1(2,:)=1
m3h2x1(3,:)=2
m3h4x1(1,:)=4
m3h4x1(2,:)=1
m3h4x1(3,:)=4
m3h8x1(1,:)=8
m3h8x1(2,:)=1
m3h8x1(3,:)=8
m3h2x2(1,:)=2
m3h2x2(2,:)=2
m3h2x2(3,:)=4
m3h4x2(1,:)=4
m3h4x2(2,:)=2
m3h4x2(3,:)=8
m3h2x4(1,:)=2
m3h2x4(2,:)=4
m3h2x4(3,:)=8
m3h4x4(1,:)=4
m3h4x4(2,:)=4
m3h4x4(3,:)=16
m3h8x4(1,:)=8
m3h8x4(2,:)=4
m3h8x4(3,:)=32
m3h2x8(1,:)=2
m3h2x8(2,:)=8
m3h2x8(3,:)=16
m3h4x8(1,:)=4
m3h4x8(2,:)=8
m3h4x8(3,:)=32
m3h2x16(1,:)=2
m3h2x16(2,:)=16
m3h2x16(3,:)=32
m4h4x4x1(1,:)=4
m4h4x4x1(2,:)=4
m4h4x4x1(3,:)=1
m4h4x4x1(4,:)=16
m4h4x2x4(1,:)=4
m4h4x2x4(2,:)=2
m4h4x2x4(3,:)=4
m4h4x2x4(4,:)=32
m4h2x4x4(1,:)=2
m4h2x4x4(2,:)=4
m4h2x4x4(3,:)=4
m4h2x4x4(4,:)=32

n2h1(:)=1
n2h2(:)=2
n2h4(:)=4
n2h8(:)=8
n2h16(:)=16


end subroutine HOL_m3_init

!**********************************************************************

end module ol_tables_storage_mmee_lew_eexmmxa_6_/**/REALKIND

! **********************************************************************
module ol_tensor_sum_storage_mmee_lew_eexmmxa_6_/**/REALKIND
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_data_types_/**/REALKIND, only: hol, hcl, met
  use ol_data_types_/**/REALKIND, only: basis, redset4, redset5
  implicit none

  type(met), save :: M2L1R1

  ! Declarations of loop wave function tensors

  type(hol), save :: G0H1(1)
  type(hol), save :: G1H1(1)
  type(hol), save :: G2H1(1)
  type(hol), save :: G0H2(1)
  type(hol), save :: G1H2(25)
  type(hol), save :: G2H2(12)
  type(hol), save :: G0H4(92)
  type(hol), save :: G1H4(166)
  type(hol), save :: G2H4(44)
  type(hol), save :: G0H8(23)
  type(hol), save :: G1H8(54)
  type(hol), save :: G2H8(22)
  type(hol), save :: G0H16(60)
  type(hol), save :: G1H16(136)
  type(hol), save :: G0H32(1)
  type(hcl), save, dimension(222) :: G0tensor
  type(hcl), save, dimension(68) :: G1tensor
  type(hcl), save, dimension(148) :: G2tensor
  type(hcl), save, dimension(16) :: G3tensor



  ! Declarations for on-the-fly tensor reduction
type (basis),      save :: RedBasis(9)
type (redset4),    save :: RedSet_4(3)
integer, save :: mass2set(0:1,11)
integer, save :: mass3set(0:2,18)
integer, save :: mass4set(0:3,8)



  ! Declarations for TI calls

  integer, save :: momenta_1(2)
  integer, save :: momenta_2(2)
  integer, save :: momenta_3(2)
  integer, save :: momenta_4(2)
  integer, save :: momenta_5(2)
  integer, save :: momenta_6(2)
  integer, save :: momenta_7(2)
  integer, save :: momenta_8(2)
  integer, save :: momenta_9(3)
  integer, save :: momenta_10(3)
  integer, save :: momenta_11(3)
  integer, save :: momenta_12(3)
  integer, save :: momenta_13(3)
  integer, save :: momenta_14(3)
  integer, save :: momenta_15(3)
  integer, save :: momenta_16(3)
  integer, save :: momenta_17(3)
  integer, save :: momenta_18(4)
  integer, save :: momenta_19(4)
  integer, save :: momenta_20(4)

  integer, save :: masses2_1(2)
  integer, save :: masses2_2(2)
  integer, save :: masses2_3(2)
  integer, save :: masses2_4(2)
  integer, save :: masses2_5(2)
  integer, save :: masses2_6(2)
  integer, save :: masses2_7(2)
  integer, save :: masses2_8(2)
  integer, save :: masses2_9(2)
  integer, save :: masses2_10(2)
  integer, save :: masses2_11(2)
  integer, save :: masses2_12(2)
  integer, save :: masses2_13(2)
  integer, save :: masses2_14(2)
  integer, save :: masses2_15(2)
  integer, save :: masses2_16(2)
  integer, save :: masses2_17(3)
  integer, save :: masses2_18(3)
  integer, save :: masses2_19(3)
  integer, save :: masses2_20(3)
  integer, save :: masses2_21(3)
  integer, save :: masses2_22(3)
  integer, save :: masses2_23(3)
  integer, save :: masses2_24(3)
  integer, save :: masses2_25(3)
  integer, save :: masses2_26(3)
  integer, save :: masses2_27(3)
  integer, save :: masses2_28(3)
  integer, save :: masses2_29(3)
  integer, save :: masses2_30(3)
  integer, save :: masses2_31(3)
  integer, save :: masses2_32(3)
  integer, save :: masses2_33(3)
  integer, save :: masses2_34(3)
  integer, save :: masses2_35(4)
  integer, save :: masses2_36(4)
  integer, save :: masses2_37(4)
  integer, save :: masses2_38(4)
  integer, save :: masses2_39(4)
  integer, save :: masses2_40(4)
  integer, save :: masses2_41(4)
  integer, save :: masses2_42(4)



  type(hcl), save, dimension(88) :: T0sum


  contains

!**********************************************************************
subroutine HOL_memory_allocation_full()
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_loop_storage_mmee_lew_eexmmxa_6_/**/DREALKIND, only: nhel
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_allocation
  implicit none

    call hol_allocation(4,1,4,1,G0H1,1)
  call hol_allocation(4,5,4,1,G1H1,1)
  call hol_allocation(4,15,4,1,G2H1,1)
  call hol_allocation(4,1,4,2,G0H2,1)
  call hol_allocation(4,5,4,2,G1H2,25)
  call hol_allocation(4,15,4,2,G2H2,12)
  call hol_allocation(4,1,4,4,G0H4,92)
  call hol_allocation(4,5,4,4,G1H4,166)
  call hol_allocation(4,15,4,4,G2H4,44)
  call hol_allocation(4,1,4,8,G0H8,23)
  call hol_allocation(4,5,4,8,G1H8,54)
  call hol_allocation(4,15,4,8,G2H8,22)
  call hol_allocation(4,1,4,16,G0H16,60)
  call hol_allocation(4,5,4,16,G1H16,136)
  call hol_allocation(4,1,4,32,G0H32,1)


end subroutine HOL_memory_allocation_full

!**********************************************************************
subroutine HOL_memory_allocation_optimized()
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_loop_storage_mmee_lew_eexmmxa_6_/**/DREALKIND, only: nhel
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_allocation
  implicit none

    call hol_allocation(4,1,4,min(nhel,1),G0H1,1)
  call hol_allocation(4,5,4,min(nhel,1),G1H1,1)
  call hol_allocation(4,15,4,min(nhel,1),G2H1,1)
  call hol_allocation(4,1,4,min(nhel,2),G0H2,1)
  call hol_allocation(4,5,4,min(nhel,2),G1H2,25)
  call hol_allocation(4,15,4,min(nhel,2),G2H2,12)
  call hol_allocation(4,1,4,min(nhel,4),G0H4,92)
  call hol_allocation(4,5,4,min(nhel,4),G1H4,166)
  call hol_allocation(4,15,4,min(nhel,4),G2H4,44)
  call hol_allocation(4,1,4,min(nhel,8),G0H8,23)
  call hol_allocation(4,5,4,min(nhel,8),G1H8,54)
  call hol_allocation(4,15,4,min(nhel,8),G2H8,22)
  call hol_allocation(4,1,4,min(nhel,16),G0H16,60)
  call hol_allocation(4,5,4,min(nhel,16),G1H16,136)
  call hol_allocation(4,1,4,min(nhel,32),G0H32,1)


end subroutine HOL_memory_allocation_optimized

!**********************************************************************
subroutine HOL_memory_deallocation_/**/REALKIND(dmode)
!**********************************************************************
! allocation of memory for the types hol
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hol
  use hol_initialisation_/**/REALKIND, only: hol_deallocation
  implicit none
  integer,   intent(in)    :: dmode

    call hol_deallocation(G0H1,1,dmode)
  call hol_deallocation(G1H1,1,dmode)
  call hol_deallocation(G2H1,1,dmode)
  call hol_deallocation(G0H2,1,dmode)
  call hol_deallocation(G1H2,25,dmode)
  call hol_deallocation(G2H2,12,dmode)
  call hol_deallocation(G0H4,92,dmode)
  call hol_deallocation(G1H4,166,dmode)
  call hol_deallocation(G2H4,44,dmode)
  call hol_deallocation(G0H8,23,dmode)
  call hol_deallocation(G1H8,54,dmode)
  call hol_deallocation(G2H8,22,dmode)
  call hol_deallocation(G0H16,60,dmode)
  call hol_deallocation(G1H16,136,dmode)
  call hol_deallocation(G0H32,1,dmode)


end subroutine HOL_memory_deallocation_/**/REALKIND

!**********************************************************************
subroutine HCL_memory_allocation()
!**********************************************************************
! allocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  implicit none

  call hcl_allocation(1,G0tensor, 222)
call hcl_allocation(5,G1tensor, 68)
call hcl_allocation(15,G2tensor, 148)
call hcl_allocation(35,G3tensor, 16)


end subroutine HCL_memory_allocation


!**********************************************************************
subroutine HCL_memory_deallocation_/**/REALKIND(dmode)
!**********************************************************************
! deallocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_deallocation
  implicit none
  integer,   intent(in)    :: dmode

  call hcl_deallocation(G0tensor, 222,dmode)
call hcl_deallocation(G1tensor, 68,dmode)
call hcl_deallocation(G2tensor, 148,dmode)
call hcl_deallocation(G3tensor, 16,dmode)

    call hcl_deallocation(T0sum,88,dmode)


end subroutine HCL_memory_deallocation_/**/REALKIND


!**********************************************************************
subroutine Tsum_memory_allocation()
!**********************************************************************
! allocation of memory for the types hcl
!**********************************************************************
  use KIND_TYPES, only: REALKIND
  use ol_data_types_/**/REALKIND, only: hcl
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  implicit none

    call hcl_allocation(1,T0sum,88)


end subroutine Tsum_memory_allocation


#ifdef PRECISION_dp
subroutine max_point(r) &
    & bind(c,name="ol_f_max_point_mmee_lew_eexmmxa_6")
  ! Return the number maximal tensor rank
  implicit none
  integer, intent(out) :: r
  r = 4
end subroutine max_point

subroutine tensor_rank(r) &
    & bind(c,name="ol_f_tensor_rank_mmee_lew_eexmmxa_6")
  ! Return the number maximal tensor rank
  implicit none
  integer, intent(out) :: r
  r = 0
end subroutine tensor_rank
#endif

subroutine reset_tensor_sum()
  use hol_initialisation_/**/REALKIND, only: hcl_allocation
  use ol_parameters_init_/**/REALKIND, only: init_hcl
  implicit none
  integer :: i

  do i = 1,88
    call init_hcl(T0sum(i))
  end do

end subroutine reset_tensor_sum


subroutine scale_one_tsum(tsum, spow)
  use ol_parameters_decl_/**/REALKIND, only: scalefactor
  implicit none
  complex(REALKIND), intent(inout) :: tsum(:)
  integer, intent(in) :: spow ! rank 0 scale power
  real(REALKIND) :: sfinv, sfac
  integer :: sz
  sfinv = 1/scalefactor
  sfac = scalefactor**spow
  sz = size(tsum)
  tsum(1) = sfac*tsum(1)
  if (sz > 1) then ! rank 1
    sfac = sfac*sfinv
    tsum(2:5) = sfac*tsum(2:5)
  end if
  if (sz > 5) then ! rank 2
    sfac = sfac*sfinv
    tsum(6:15) = sfac*tsum(6:15)
  end if
  if (sz > 15) then ! rank 3
    sfac = sfac*sfinv
    tsum(16:35) = sfac*tsum(16:35)
  end if
  if (sz > 35) then ! rank 4
    sfac = sfac*sfinv
    tsum(36:70) = sfac*tsum(36:70)
  end if
  if (sz > 70) then ! rank 5
    sfac = sfac*sfinv
    tsum(71:126) = sfac*tsum(71:126)
  end if
  if (sz > 126) then ! rank 6
    sfac = sfac*sfinv
    tsum(127:210) = sfac*tsum(127:210)
  end if
  if (sz > 210) then ! rank 7
    sfac = sfac*sfinv
    tsum(211:330) = sfac*tsum(211:330)
  end if
end subroutine scale_one_tsum


subroutine scale_tensor_sum()
  implicit none
  call scale_one_tsum(T0sum(1)%cmp, 2)
  call scale_one_tsum(T0sum(2)%cmp, 2)
  call scale_one_tsum(T0sum(3)%cmp, 2)
  call scale_one_tsum(T0sum(4)%cmp, 2)
  call scale_one_tsum(T0sum(5)%cmp, 2)
  call scale_one_tsum(T0sum(6)%cmp, 2)
  call scale_one_tsum(T0sum(7)%cmp, 2)
  call scale_one_tsum(T0sum(8)%cmp, 2)
  call scale_one_tsum(T0sum(9)%cmp, 2)
  call scale_one_tsum(T0sum(10)%cmp, 2)
  call scale_one_tsum(T0sum(11)%cmp, 2)
  call scale_one_tsum(T0sum(12)%cmp, 0)
  call scale_one_tsum(T0sum(13)%cmp, 0)
  call scale_one_tsum(T0sum(14)%cmp, 0)
  call scale_one_tsum(T0sum(15)%cmp, 0)
  call scale_one_tsum(T0sum(16)%cmp, 0)
  call scale_one_tsum(T0sum(17)%cmp, 0)
  call scale_one_tsum(T0sum(18)%cmp, 0)
  call scale_one_tsum(T0sum(19)%cmp, 0)
  call scale_one_tsum(T0sum(20)%cmp, 0)
  call scale_one_tsum(T0sum(21)%cmp, 0)
  call scale_one_tsum(T0sum(22)%cmp, 0)
  call scale_one_tsum(T0sum(23)%cmp, 0)
  call scale_one_tsum(T0sum(24)%cmp, 0)
  call scale_one_tsum(T0sum(25)%cmp, 0)
  call scale_one_tsum(T0sum(26)%cmp, 0)
  call scale_one_tsum(T0sum(27)%cmp, 0)
  call scale_one_tsum(T0sum(28)%cmp, 0)
  call scale_one_tsum(T0sum(29)%cmp, 0)
  call scale_one_tsum(T0sum(30)%cmp, 0)
  call scale_one_tsum(T0sum(31)%cmp, 0)
  call scale_one_tsum(T0sum(32)%cmp, 0)
  call scale_one_tsum(T0sum(33)%cmp, 0)
  call scale_one_tsum(T0sum(34)%cmp, 0)
  call scale_one_tsum(T0sum(35)%cmp, 0)
  call scale_one_tsum(T0sum(36)%cmp, 0)
  call scale_one_tsum(T0sum(37)%cmp, 0)
  call scale_one_tsum(T0sum(38)%cmp, 0)
  call scale_one_tsum(T0sum(39)%cmp, 0)
  call scale_one_tsum(T0sum(40)%cmp, 0)
  call scale_one_tsum(T0sum(41)%cmp, 0)
  call scale_one_tsum(T0sum(42)%cmp, 0)
  call scale_one_tsum(T0sum(43)%cmp, 0)
  call scale_one_tsum(T0sum(44)%cmp, 0)
  call scale_one_tsum(T0sum(45)%cmp, 0)
  call scale_one_tsum(T0sum(46)%cmp, 0)
  call scale_one_tsum(T0sum(47)%cmp, 0)
  call scale_one_tsum(T0sum(48)%cmp, 0)
  call scale_one_tsum(T0sum(49)%cmp, 0)
  call scale_one_tsum(T0sum(50)%cmp, -2)
  call scale_one_tsum(T0sum(51)%cmp, -2)
  call scale_one_tsum(T0sum(52)%cmp, -2)
  call scale_one_tsum(T0sum(53)%cmp, -2)
  call scale_one_tsum(T0sum(54)%cmp, -2)
  call scale_one_tsum(T0sum(55)%cmp, -2)
  call scale_one_tsum(T0sum(56)%cmp, -2)
  call scale_one_tsum(T0sum(57)%cmp, -2)
  call scale_one_tsum(T0sum(58)%cmp, -2)
  call scale_one_tsum(T0sum(59)%cmp, -2)
  call scale_one_tsum(T0sum(60)%cmp, -2)
  call scale_one_tsum(T0sum(61)%cmp, -2)
  call scale_one_tsum(T0sum(62)%cmp, -2)
  call scale_one_tsum(T0sum(63)%cmp, -2)
  call scale_one_tsum(T0sum(64)%cmp, -2)
  call scale_one_tsum(T0sum(65)%cmp, -2)
  call scale_one_tsum(T0sum(66)%cmp, -2)
  call scale_one_tsum(T0sum(67)%cmp, -2)
  call scale_one_tsum(T0sum(68)%cmp, -2)
  call scale_one_tsum(T0sum(69)%cmp, -2)
  call scale_one_tsum(T0sum(70)%cmp, -2)
  call scale_one_tsum(T0sum(71)%cmp, -2)
  call scale_one_tsum(T0sum(72)%cmp, -2)
  call scale_one_tsum(T0sum(73)%cmp, -2)
  call scale_one_tsum(T0sum(74)%cmp, -2)
  call scale_one_tsum(T0sum(75)%cmp, -2)
  call scale_one_tsum(T0sum(76)%cmp, -2)
  call scale_one_tsum(T0sum(77)%cmp, -2)
  call scale_one_tsum(T0sum(78)%cmp, -2)
  call scale_one_tsum(T0sum(79)%cmp, -2)
  call scale_one_tsum(T0sum(80)%cmp, -2)
  call scale_one_tsum(T0sum(81)%cmp, -2)
  call scale_one_tsum(T0sum(82)%cmp, -2)
  call scale_one_tsum(T0sum(83)%cmp, -2)
  call scale_one_tsum(T0sum(84)%cmp, -2)
  call scale_one_tsum(T0sum(85)%cmp, -2)
  call scale_one_tsum(T0sum(86)%cmp, -2)
  call scale_one_tsum(T0sum(87)%cmp, -2)
  call scale_one_tsum(T0sum(88)%cmp, -2)

end subroutine scale_tensor_sum

! **********************************************************************
subroutine set_integral_masses_and_momenta()
! **********************************************************************

  use ol_parameters_decl_/**/REALKIND
  momenta_1 = [ 0, 0 ]
  momenta_2 = [ 16, 15 ]
  momenta_3 = [ 17, 14 ]
  momenta_4 = [ 18, 13 ]
  momenta_5 = [ 19, 12 ]
  momenta_6 = [ 28, 3 ]
  momenta_7 = [ 29, 2 ]
  momenta_8 = [ 30, 1 ]
  momenta_9 = [ 16, 1, 14 ]
  momenta_10 = [ 16, 2, 13 ]
  momenta_11 = [ 16, 3, 12 ]
  momenta_12 = [ 16, 13, 2 ]
  momenta_13 = [ 17, 2, 12 ]
  momenta_14 = [ 17, 12, 2 ]
  momenta_15 = [ 18, 1, 12 ]
  momenta_16 = [ 28, 1, 2 ]
  momenta_17 = [ 28, 2, 1 ]
  momenta_18 = [ 16, 1, 2, 12 ]
  momenta_19 = [ 16, 1, 12, 2 ]
  momenta_20 = [ 16, 2, 1, 12 ]

  masses2_1 = [ 0, 0 ]
  masses2_2 = [ nME, 0 ]
  masses2_3 = [ nMH, 0 ]
  masses2_4 = [ nMW, 0 ]
  masses2_5 = [ nMZ, 0 ]
  masses2_6 = [ nME, nME ]
  masses2_7 = [ nMH, nME ]
  masses2_8 = [ nMZ, nME ]
  masses2_9 = [ nME, nMH ]
  masses2_10 = [ nMH, nMH ]
  masses2_11 = [ nMZ, nMH ]
  masses2_12 = [ 0, nMW ]
  masses2_13 = [ nMW, nMW ]
  masses2_14 = [ nME, nMZ ]
  masses2_15 = [ nMH, nMZ ]
  masses2_16 = [ nMZ, nMZ ]
  masses2_17 = [ nMW, 0, 0 ]
  masses2_18 = [ nMW, 0, nMW ]
  masses2_19 = [ nME, nME, nME ]
  masses2_20 = [ nME, nME, nMH ]
  masses2_21 = [ nMH, nME, nMH ]
  masses2_22 = [ nMZ, nME, nMH ]
  masses2_23 = [ nME, nME, nMZ ]
  masses2_24 = [ nMH, nME, nMZ ]
  masses2_25 = [ nMZ, nME, nMZ ]
  masses2_26 = [ nME, nMH, nME ]
  masses2_27 = [ nME, nMH, nMH ]
  masses2_28 = [ nME, nMH, nMZ ]
  masses2_29 = [ 0, nMW, 0 ]
  masses2_30 = [ nMW, nMW, 0 ]
  masses2_31 = [ nMW, nMW, nMW ]
  masses2_32 = [ nME, nMZ, nME ]
  masses2_33 = [ nME, nMZ, nMH ]
  masses2_34 = [ nME, nMZ, nMZ ]
  masses2_35 = [ nME, nME, nMH, nME ]
  masses2_36 = [ nME, nME, nMH, nMH ]
  masses2_37 = [ nME, nME, nMH, nMZ ]
  masses2_38 = [ nME, nME, nMZ, nME ]
  masses2_39 = [ nME, nME, nMZ, nMH ]
  masses2_40 = [ nME, nME, nMZ, nMZ ]
  masses2_41 = [ nMW, nMW, 0, 0 ]
  masses2_42 = [ nMW, nMW, 0, nMW ]


end subroutine  set_integral_masses_and_momenta

! **********************************************************************
subroutine integrate_tensor_sum(M2out)
! **********************************************************************
  use ol_parameters_decl_/**/REALKIND ! only: ZERO, masses
#ifndef PRECISION_dp
  use ol_parameters_decl_/**/DREALKIND, only: a_switch
#endif
  use ol_parameters_init_/**/REALKIND, only: init_met, add_met, met_to_real
  use ol_loop_routines_/**/REALKIND, only: TI_call_OL
  implicit none
  real(REALKIND), intent(out) :: M2out
  type(met) :: M2
  call init_met(M2)



#ifdef LOOPSQUARED
  if (a_switch == 1 .or. a_switch == 7) then
#endif
  call TI_call_OL(0,0, momenta_18, masses2_42, T0sum(1), M2)
  call TI_call_OL(0,0, momenta_20, masses2_42, T0sum(2), M2)
  call TI_call_OL(0,0, momenta_19, masses2_36, T0sum(3), M2)
  call TI_call_OL(0,0, momenta_19, masses2_41, T0sum(4), M2)
  call TI_call_OL(0,0, momenta_18, masses2_35, T0sum(5), M2)
  call TI_call_OL(0,0, momenta_18, masses2_38, T0sum(6), M2)
  call TI_call_OL(0,0, momenta_20, masses2_35, T0sum(7), M2)
  call TI_call_OL(0,0, momenta_20, masses2_38, T0sum(8), M2)
  call TI_call_OL(0,0, momenta_19, masses2_40, T0sum(9), M2)
  call TI_call_OL(0,0, momenta_19, masses2_37, T0sum(10), M2)
  call TI_call_OL(0,0, momenta_19, masses2_39, T0sum(11), M2)
  call TI_call_OL(0,0, momenta_9, masses2_30, T0sum(12), M2)
  call TI_call_OL(0,0, momenta_16, masses2_20, T0sum(13), M2)
  call TI_call_OL(0,0, momenta_9, masses2_20, T0sum(14), M2)
  call TI_call_OL(0,0, momenta_16, masses2_23, T0sum(15), M2)
  call TI_call_OL(0,0, momenta_9, masses2_23, T0sum(16), M2)
  call TI_call_OL(0,0, momenta_16, masses2_30, T0sum(17), M2)
  call TI_call_OL(0,0, momenta_17, masses2_20, T0sum(18), M2)
  call TI_call_OL(0,0, momenta_10, masses2_20, T0sum(19), M2)
  call TI_call_OL(0,0, momenta_17, masses2_23, T0sum(20), M2)
  call TI_call_OL(0,0, momenta_10, masses2_23, T0sum(21), M2)
  call TI_call_OL(0,0, momenta_17, masses2_30, T0sum(22), M2)
  call TI_call_OL(0,0, momenta_10, masses2_30, T0sum(23), M2)
  call TI_call_OL(0,0, momenta_15, masses2_25, T0sum(24), M2)
  call TI_call_OL(0,0, momenta_15, masses2_22, T0sum(25), M2)
  call TI_call_OL(0,0, momenta_15, masses2_24, T0sum(26), M2)
  call TI_call_OL(0,0, momenta_13, masses2_18, T0sum(27), M2)
  call TI_call_OL(0,0, momenta_11, masses2_31, T0sum(28), M2)
  call TI_call_OL(0,0, momenta_15, masses2_18, T0sum(29), M2)
  call TI_call_OL(0,0, momenta_15, masses2_21, T0sum(30), M2)
  call TI_call_OL(0,0, momenta_14, masses2_27, T0sum(31), M2)
  call TI_call_OL(0,0, momenta_12, masses2_20, T0sum(32), M2)
  call TI_call_OL(0,0, momenta_15, masses2_29, T0sum(33), M2)
  call TI_call_OL(0,0, momenta_14, masses2_17, T0sum(34), M2)
  call TI_call_OL(0,0, momenta_12, masses2_30, T0sum(35), M2)
  call TI_call_OL(0,0, momenta_13, masses2_26, T0sum(36), M2)
  call TI_call_OL(0,0, momenta_11, masses2_19, T0sum(37), M2)
  call TI_call_OL(0,0, momenta_13, masses2_32, T0sum(38), M2)
  call TI_call_OL(0,0, momenta_15, masses2_26, T0sum(39), M2)
  call TI_call_OL(0,0, momenta_15, masses2_32, T0sum(40), M2)
  call TI_call_OL(0,0, momenta_14, masses2_34, T0sum(41), M2)
  call TI_call_OL(0,0, momenta_12, masses2_23, T0sum(42), M2)
  call TI_call_OL(0,0, momenta_14, masses2_28, T0sum(43), M2)
  call TI_call_OL(0,0, momenta_14, masses2_33, T0sum(44), M2)
  call TI_call_OL(0,0, momenta_13, masses2_21, T0sum(45), M2)
  call TI_call_OL(0,0, momenta_13, masses2_25, T0sum(46), M2)
  call TI_call_OL(0,0, momenta_13, masses2_22, T0sum(47), M2)
  call TI_call_OL(0,0, momenta_13, masses2_24, T0sum(48), M2)
  call TI_call_OL(0,0, momenta_13, masses2_29, T0sum(49), M2)
  call TI_call_OL(0,0, momenta_8, masses2_12, T0sum(50), M2)
  call TI_call_OL(0,0, momenta_3, masses2_4, T0sum(51), M2)
  call TI_call_OL(0,0, momenta_2, masses2_13, T0sum(52), M2)
  call TI_call_OL(0,0, momenta_1, masses2_4, T0sum(53), M2)
  call TI_call_OL(0,0, momenta_8, masses2_7, T0sum(54), M2)
  call TI_call_OL(0,0, momenta_7, masses2_9, T0sum(55), M2)
  call TI_call_OL(0,0, momenta_6, masses2_6, T0sum(56), M2)
  call TI_call_OL(0,0, momenta_1, masses2_2, T0sum(57), M2)
  call TI_call_OL(0,0, momenta_1, masses2_3, T0sum(58), M2)
  call TI_call_OL(0,0, momenta_3, masses2_9, T0sum(59), M2)
  call TI_call_OL(0,0, momenta_2, masses2_6, T0sum(60), M2)
  call TI_call_OL(0,0, momenta_8, masses2_8, T0sum(61), M2)
  call TI_call_OL(0,0, momenta_7, masses2_14, T0sum(62), M2)
  call TI_call_OL(0,0, momenta_1, masses2_5, T0sum(63), M2)
  call TI_call_OL(0,0, momenta_3, masses2_14, T0sum(64), M2)
  call TI_call_OL(0,0, momenta_7, masses2_4, T0sum(65), M2)
  call TI_call_OL(0,0, momenta_6, masses2_13, T0sum(66), M2)
  call TI_call_OL(0,0, momenta_7, masses2_7, T0sum(67), M2)
  call TI_call_OL(0,0, momenta_8, masses2_9, T0sum(68), M2)
  call TI_call_OL(0,0, momenta_4, masses2_9, T0sum(69), M2)
  call TI_call_OL(0,0, momenta_7, masses2_8, T0sum(70), M2)
  call TI_call_OL(0,0, momenta_8, masses2_14, T0sum(71), M2)
  call TI_call_OL(0,0, momenta_4, masses2_14, T0sum(72), M2)
  call TI_call_OL(0,0, momenta_7, masses2_12, T0sum(73), M2)
  call TI_call_OL(0,0, momenta_8, masses2_4, T0sum(74), M2)
  call TI_call_OL(0,0, momenta_4, masses2_4, T0sum(75), M2)
  call TI_call_OL(0,0, momenta_5, masses2_16, T0sum(76), M2)
  call TI_call_OL(0,0, momenta_4, masses2_8, T0sum(77), M2)
  call TI_call_OL(0,0, momenta_5, masses2_11, T0sum(78), M2)
  call TI_call_OL(0,0, momenta_5, masses2_15, T0sum(79), M2)
  call TI_call_OL(0,0, momenta_4, masses2_7, T0sum(80), M2)
  call TI_call_OL(0,0, momenta_5, masses2_13, T0sum(81), M2)
  call TI_call_OL(0,0, momenta_5, masses2_1, T0sum(82), M2)
  call TI_call_OL(0,0, momenta_4, masses2_12, T0sum(83), M2)
  call TI_call_OL(0,0, momenta_5, masses2_6, T0sum(84), M2)
  call TI_call_OL(0,0, momenta_3, masses2_8, T0sum(85), M2)
  call TI_call_OL(0,0, momenta_3, masses2_7, T0sum(86), M2)
  call TI_call_OL(0,0, momenta_3, masses2_12, T0sum(87), M2)
  call TI_call_OL(0,0, momenta_5, masses2_10, T0sum(88), M2)

  call add_met(M2,M2L1R1)

#ifdef LOOPSQUARED
  end if
#endif

  call met_to_real(M2out,M2)

#ifdef PRECISION_dp
  call HOL_memory_deallocation_/**/REALKIND(1)
  call HCL_memory_deallocation_/**/REALKIND(1)
#endif

end subroutine integrate_tensor_sum

end module ol_tensor_sum_storage_mmee_lew_eexmmxa_6_/**/REALKIND
