(* leptonic tau decay *)

SelectInterference = {qQCD -> 0};

UnitaryGauge = False;
noQCD = True;

(* ew (QED=0) corrections, no quarks in loop *)

lepew03 = {
  QED -> 0,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 3}]
};
lepew02 = {
  QED -> 0,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}]
};

(*
lepew03nofermionloops = {
  QED -> 0,
  Approximation -> "nofermionloops",
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 3}],
  SelectLoopDiagrams -> ( Not[ContainsFermionLoop[##]] &),
  SelectCTDiagrams -> (True &)
};

lepew03fermionloops = {
  QED -> 0,
  Approximation -> "fermionloops",
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 3}],
  SelectLoopDiagrams -> ( ContainsFermionLoop[##] &),
  SelectCTDiagrams -> (True &)
};
*)

(* tau -> ntau e nex      *)  AddProcess[FeynArtsProcess -> {F[2,{3}]} -> {F[1,{3}], F[2,{1}], -F[1,{1}]}, Sequence @@ lepew03];
(* tau -> ntau mu nmux    *)  AddProcess[FeynArtsProcess -> {F[2,{3}]} -> {F[1,{3}], F[2,{2}], -F[1,{2}]}, Sequence @@ lepew03];

(* tau -> ntau e nex a    *)  AddReal[FeynArtsProcess -> {F[2,{3}]} -> {F[1,{3}], F[2,{1}], -F[1,{1}], V[1]}, Sequence @@ lepew03];
(* tau -> ntau mu nmux a  *)  AddReal[FeynArtsProcess -> {F[2,{3}]} -> {F[1,{3}], F[2,{2}], -F[1,{2}], V[1]}, Sequence @@ lepew03];

(* mu -> nmu e nex  nfl=3    *)  AddProcess[FeynArtsProcess -> {F[2,{2}]} -> {F[1,{2}], F[2,{1}], -F[1,{1}]}, Sequence @@ lepew03];
(* mu -> nmu e nex  nfl=2    *)  AddProcess[FeynArtsProcess -> {F[2,{2}]} -> {F[1,{2}], F[2,{1}], -F[1,{1}]}, Sequence @@ lepew02];

(* mu -> nmu e nex a  nfl=3  *)  AddReal[FeynArtsProcess -> {F[2,{2}]} -> {F[1,{2}], F[2,{1}], -F[1,{1}], V[1]}, Sequence @@ lepew03];
(* mu -> nmu e nex a  nfl=2  *)  AddReal[FeynArtsProcess -> {F[2,{2}]} -> {F[1,{2}], F[2,{1}], -F[1,{1}], V[1]}, Sequence @@ lepew02];
