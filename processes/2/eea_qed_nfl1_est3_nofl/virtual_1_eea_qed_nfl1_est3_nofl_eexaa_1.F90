
module ol_vamp_1_eea_qed_nfl1_est3_nofl_eexaa_1_/**/REALKIND
contains

! **********************************************************************
subroutine vamp_1(M)
! P(0:3,nlegs) = incoming external momenta
! Uses tree structures 'wf', factors 'c', and denominators 'den' from loop_eea_qed_nfl1_est3_nofl_eexaa_1.
! Sets colour stripped amplitudes A from the module loop_amplitudes_eea_qed_nfl1_est3_nofl_eexaa_1.
! **********************************************************************
  use KIND_TYPES, only: REALKIND, QREALKIND, intkind2
  use ol_parameters_decl_/**/DREALKIND, only: l_switch !, kloopmax
  use ol_parameters_decl_/**/QREALKIND ! masses
  use ol_vert_interface_/**/REALKIND
  use ol_prop_interface_/**/REALKIND
  use ol_last_step_/**/REALKIND
  use ol_tables_storage_eea_qed_nfl1_est3_nofl_eexaa_1_/**/DREALKIND
  use ol_tensor_sum_storage_eea_qed_nfl1_est3_nofl_eexaa_1_/**/REALKIND
  use ol_loop_handling_/**/REALKIND
  use ofred_reduction_/**/REALKIND, only: Hotf_4pt_reduction, Hotf_4pt_reduction_last
  use ofred_reduction_/**/REALKIND, only: Hotf_5pt_reduction, Hotf_5pt_reduction_last
  use ol_loop_reduction_/**/REALKIND, only: TI_bubble_red, TI_triangle_red

  use ol_loop_storage_eea_qed_nfl1_est3_nofl_eexaa_1_/**/REALKIND
#ifndef PRECISION_dp
  use ol_loop_storage_eea_qed_nfl1_est3_nofl_eexaa_1_/**/DREALKIND, only: &
    & p_switch, Hel, merge_step, merge_mism, merge_tables, merge_hels, ntryL, hel_states
#endif
  use hol_initialisation_/**/REALKIND, only: G0_hol_initialisation
  use ol_h_vert_interface_/**/REALKIND
  use ol_h_prop_interface_/**/REALKIND
  use ol_h_last_step_/**/REALKIND
  use ol_merging_/**/REALKIND, only: ol_merge, ol_merge_tensors, ol_merge_last

  implicit none

  type(Hpolcont) :: Gcoeff(hel_states)
  type(Hpolcont), intent(in) :: M(1,hel_states)
  integer :: kloop


#ifndef PRECISION_dp
  if (ntryL==1 .OR. p_switch == 1) Gcoeff(:)%hf = Hel(1:hel_states)
#else
  if (ntryL==1 .OR. p_switch == 2) Gcoeff(:)%hf = Hel(1:hel_states)
#endif

! do kloop = 1, kloopmax
  ! =============================================


! Dressing, otf merging and otf reduction calls to build loop structures

  Gcoeff(:)%j = (c(1)*M(1,:)%j)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(1),h0tab(:,1),[8,2,1,4],[nME,0,nME,nME],4,0)
  call Hloop_QV_A(ntryL,G0H8(1),ex4(:),G0H4(1),m3h2x4(:,1),heltab2x8(:,:,1))
  call Hloop_Q_A(ntryL,G0H4(1),8,nME,G1H4(1),n2h4(1))
  Gcoeff(:)%j = (c(1)*M(1,:)%j)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(2),h0tab(:,2),[8,1,2,4],[nME,0,nME,nME],4,0)
  call Hloop_AV_Q(ntryL,G0H8(1),ex4(:),G0H4(1),m3h2x4(:,2),heltab2x8(:,:,2))
  call Hloop_A_Q(ntryL,G0H4(1),8,nME,G1H4(2),n2h4(2))
  call Hloop_QA_V(ntryL,G1H4(1),ex2(:),G1H2(1),m3h2x2(:,1),heltab2x4(:,:,1))
  call Hloop_AQ_V(ntryL,G1H4(2),ex1(:),G1H2(2),m3h2x2(:,2),heltab2x4(:,:,2))
  call Hloop_VQ_A(ntryL,G1H2(1),ex1(:),G1H1(1),m3h2x1(:,1),heltab2x2(:,:,1))
  call Hloop_Q_A(ntryL,G1H1(1),11,nME,G2H1(1),n2h1(1))
  call Hloop_VA_Q(ntryL,G1H2(2),ex2(:),G1H1(1),m3h2x1(:,2),heltab2x2(:,:,2))
  call Hloop_A_Q(ntryL,G1H1(1),11,nME,G2H1(2),n2h1(2))
  call Hotf_4pt_reduction(G2H1(1),RedSet_4(1),mass4set(:,1),  & 
G1H1(1),G1H1(2),G1H1(3),G1H1(4),G1H1(5),1)
  call HG1shiftOLR(G1H1(2),4,1)
  call Hotf_4pt_reduction(G2H1(2),RedSet_4(2),mass4set(:,1),  & 
G1H1(6),G1H1(7),G1H1(8),G1H1(9),G1H1(10),1)
  call HG1shiftOLR(G1H1(7),4,1)
  call Hloop_QV_A(ntryL,G1H1(1),ex3(:),G1H1(11),m3h1x1(:,1),heltab2x1(:,:,1))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(11),15,nME,G2tensor(1),n2h1(3))
  call Hloop_QV_A(ntryL,G1H1(2),ex3(:),G1H1(1),m3h1x1(:,2),heltab2x1(:,:,2))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(1),4,nME,G2tensor(2),n2h1(4))
  call Hloop_QV_A(ntryL,G1H1(5),ex3(:),G1H1(11),m3h1x1(:,3),heltab2x1(:,:,3))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(11),15,nME,G2tensor(3),n2h1(5))
  call Hloop_AV_Q(ntryL,G1H1(6),ex3(:),G1H1(2),m3h1x1(:,4),heltab2x1(:,:,4))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(2),15,nME,G2tensor(4),n2h1(6))
  call Hloop_AV_Q(ntryL,G1H1(7),ex3(:),G1H1(1),m3h1x1(:,5),heltab2x1(:,:,5))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(1),4,nME,G2tensor(5),n2h1(7))
  call Hloop_AV_Q(ntryL,G1H1(10),ex3(:),G1H1(5),m3h1x1(:,6),heltab2x1(:,:,6))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(5),15,nME,G2tensor(6),n2h1(8))
  call Hotf_4pt_reduction_last(G2tensor(1),RedSet_4(1),mass4set(:,1),  & 
G1tensor(1),G1tensor(2),G1tensor(3),G1tensor(4),G1tensor(5))
  call G_TensorShift(G1tensor(2),4)
  call Hotf_4pt_reduction_last(G2tensor(4),RedSet_4(2),mass4set(:,1),  & 
G1tensor(6),G1tensor(7),G1tensor(8),G1tensor(9),G1tensor(10))
  call G_TensorShift(G1tensor(7),4)
  call Hotf_4pt_reduction_last(G1tensor(1),RedSet_4(1),mass4set(:,1),  & 
G0tensor(1),G0tensor(2),G0tensor(3),G0tensor(4),G0tensor(5))
  call Hotf_4pt_reduction_last(G1tensor(6),RedSet_4(2),mass4set(:,1),  & 
G0tensor(6),G0tensor(7),G0tensor(8),G0tensor(9),G0tensor(10))
  call ol_merge_tensors(T0sum(1),[G0tensor(1)])
  call ol_merge_tensors(T0sum(2),[G0tensor(6)])
  Gcoeff(:)%j = (c(1)*M(1,:)%j) * den(1)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(3),h0tab(:,3),[8,2,5],[nME,0,nME],3,1,wf2(:,2))
  call Hloop_QV_A(ntryL,G0H8(1),ex4(:),G0H4(1),m3h2x4(:,3),heltab2x8(:,:,3))
  call Hloop_Q_A(ntryL,G0H4(1),8,nME,G1H4(1),n2h4(3))
  Gcoeff(:)%j = (c(1)*M(1,:)%j) * den(3)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(4),h0tab(:,4),[8,1,6],[nME,0,nME],3,1,wf2(:,5))
  call Hloop_AV_Q(ntryL,G0H8(1),ex4(:),G0H4(1),m3h2x4(:,4),heltab2x8(:,:,4))
  call Hloop_A_Q(ntryL,G0H4(1),8,nME,G1H4(2),n2h4(4))
  Gcoeff(:)%j = (c(1)*M(1,:)%j) * den(2)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(5),h0tab(:,5),[9,2,4],[0,nME,nME],3,1,wf4(:,3))
  call Hloop_AQ_V(ntryL,G0H8(1),wf4(:,3),G0H2(1),m3h4x2(:,1),heltab2x8(:,:,5))
  Gcoeff(:)%j = (c(1)*M(1,:)%j) * den(4)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(6),h0tab(:,6),[10,1,4],[0,nME,nME],3,1,wf4(:,6))
  call Hloop_QA_V(ntryL,G0H8(1),wf4(:,6),G0H2(2),m3h4x2(:,2),heltab2x8(:,:,6))
  call Hloop_QA_V(ntryL,G1H4(1),ex2(:),G1H2(1),m3h2x2(:,3),heltab2x4(:,:,3))
  call Hloop_AQ_V(ntryL,G1H4(2),ex1(:),G1H2(2),m3h2x2(:,4),heltab2x4(:,:,4))
  call Hloop_VA_Q(ntryL,G0H2(1),ex2(:),G0H1(1),m3h2x1(:,3),heltab2x2(:,:,3))
  call Hloop_A_Q(ntryL,G0H1(1),11,nME,G1H1(11),n2h1(9))
  call Hloop_VQ_A(ntryL,G0H2(2),ex1(:),G0H1(1),m3h2x1(:,4),heltab2x2(:,:,4))
  call Hloop_Q_A(ntryL,G0H1(1),11,nME,G1H1(6),n2h1(10))
  call ol_merge(ntryL,merge_step,merge_mism,merge_tables,merge_hels,G1H1(6),[G1H1(3)])
  call ol_merge(ntryL,merge_step,merge_mism,merge_tables,merge_hels,G1H1(11),[G1H1(8)])
  call Hloop_QV_A(ntryL,G1H1(6),ex3(:),G1H1(2),m3h1x1(:,7),heltab2x1(:,:,7))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(2),15,nME,G2tensor(1),n2h1(11))
  call Hloop_QV_A(ntryL,G1H1(4),ex3(:),G1H1(7),m3h1x1(:,8),heltab2x1(:,:,8))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(7),15,nME,G2tensor(4),n2h1(12))
  call Hloop_AV_Q(ntryL,G1H1(11),ex3(:),G1H1(1),m3h1x1(:,9),heltab2x1(:,:,9))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(1),15,nME,G2tensor(7),n2h1(13))
  call Hloop_AV_Q(ntryL,G1H1(9),ex3(:),G1H1(10),m3h1x1(:,10),heltab2x1(:,:,10))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(10),15,nME,G2tensor(8),n2h1(14))
  call Hloop_VQ_A(ntryL,G1H2(1),wf2(:,2),G1H1(5),m3h2x1(:,5),heltab2x2(:,:,5))
  call Hcheck_last_Q_A(ntryL,l_switch,G1H1(5),15,nME,G2tensor(9),n2h1(15))
  call Hloop_VA_Q(ntryL,G1H2(2),wf2(:,5),G1H1(3),m3h2x1(:,6),heltab2x2(:,:,6))
  call Hcheck_last_A_Q(ntryL,l_switch,G1H1(3),15,nME,G2tensor(10),n2h1(16))
  call ol_merge_tensors(G2tensor(2),[G1tensor(2),G0tensor(2)])
  call ol_merge_tensors(G2tensor(9),[G2tensor(3),G1tensor(5),G0tensor(5)])
  call ol_merge_tensors(G2tensor(5),[G1tensor(7),G0tensor(7)])
  call ol_merge_tensors(G2tensor(10),[G2tensor(6),G1tensor(10),G0tensor(10)])
  call ol_merge_tensors(G2tensor(1),[G1tensor(3),G0tensor(3)])
  call ol_merge_tensors(G2tensor(8),[G2tensor(4),G1tensor(9),G1tensor(4),G0tensor(9),G0tensor(4)])
  call ol_merge_tensors(G2tensor(7),[G1tensor(8),G0tensor(8)])
  call TI_triangle_red(G2tensor(2),RedBasis(6),mass3set(:,1),G0tensor(1),G0tensor(6),G0tensor(2),G0tensor(5),M2L1R1,[nME], &
    G0tensor(7))
  call TI_triangle_red(G2tensor(9),RedBasis(1),mass3set(:,1),G0tensor(10),G0tensor(3),G0tensor(9),G0tensor(4),M2L1R1,[nME], &
    G0tensor(8))
  call TI_triangle_red(G2tensor(5),RedBasis(7),mass3set(:,1),G0tensor(11),G0tensor(12),G0tensor(13),G0tensor(14),M2L1R1,[nME], &
    G0tensor(15))
  call TI_triangle_red(G2tensor(10),RedBasis(4),mass3set(:,1),G0tensor(16),G0tensor(17),G0tensor(18),G0tensor(19),M2L1R1,[nME], &
    G0tensor(20))
  call TI_triangle_red(G2tensor(1),RedBasis(3),mass3set(:,2),G0tensor(21),G0tensor(22),G0tensor(23),G0tensor(24),M2L1R1,[nME], &
    G0tensor(25))
  call TI_triangle_red(G2tensor(8),RedBasis(2),mass3set(:,3),G0tensor(26),G0tensor(27),G0tensor(28),G0tensor(29),M2L1R1,[nME], &
    G0tensor(30))
  call TI_triangle_red(G2tensor(7),RedBasis(5),mass3set(:,2),G0tensor(31),G0tensor(32),G0tensor(33),G0tensor(34),M2L1R1,[nME], &
    G0tensor(35))
  call ol_merge_tensors(T0sum(3),[G0tensor(1)])
  call ol_merge_tensors(T0sum(4),[G0tensor(10)])
  call ol_merge_tensors(T0sum(5),[G0tensor(11)])
  call ol_merge_tensors(T0sum(6),[G0tensor(16)])
  call ol_merge_tensors(T0sum(7),[G0tensor(21)])
  call ol_merge_tensors(T0sum(8),[G0tensor(26)])
  call ol_merge_tensors(T0sum(9),[G0tensor(31)])
  Gcoeff(:)%j = (c(1)*M(1,:)%j) * den(5)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(7),h0tab(:,7),[10,5],[nME,0],2,2,wf4(:,6),wf2(:,2))
  call Hloop_VA_Q(ntryL,G0H8(1),wf4(:,6),G0H2(1),m3h4x2(:,3),heltab2x8(:,:,7))
  call Hloop_A_Q(ntryL,G0H2(1),10,nME,G1H2(1),n2h2(1))
  Gcoeff(:)%j = (c(1)*M(1,:)%j) * den(6)
  call G0_hol_initialisation(ntryL,Gcoeff,G0H8(1),m0h(8),h0tab(:,8),[9,6],[0,nME],2,2,wf4(:,3),wf2(:,5))
  call Hloop_AQ_V(ntryL,G0H8(1),wf4(:,3),G0H2(2),m3h4x2(:,4),heltab2x8(:,:,8))
  call Hcheck_last_AQ_V(ntryL,l_switch,G1H2(1),wf2(:,2),G1tensor(1),m3h2x1(:,7),heltab2x2(:,:,7))
  call Hloop_VA_Q(ntryL,G0H2(2),wf2(:,5),G0H1(1),m3h2x1(:,8),heltab2x2(:,:,8))
  call Hcheck_last_A_Q(ntryL,l_switch,G0H1(1),15,nME,G1tensor(6),n2h1(17))
  call ol_merge_tensors(G0tensor(3),[G0tensor(6)])
  call ol_merge_tensors(G0tensor(22),[G0tensor(2)])
  call ol_merge_tensors(G0tensor(27),[G0tensor(14),G0tensor(5)])
  call ol_merge_tensors(G0tensor(35),[G0tensor(30),G0tensor(25),G0tensor(20),G0tensor(15),G0tensor(8),G0tensor(7)])
  call ol_merge_tensors(G0tensor(24),[G0tensor(9)])
  call ol_merge_tensors(G0tensor(29),[G0tensor(19),G0tensor(4)])
  call ol_merge_tensors(G0tensor(17),[G0tensor(12)])
  call ol_merge_tensors(G0tensor(32),[G0tensor(13)])
  call ol_merge_tensors(G1tensor(6),[G0tensor(34),G0tensor(18)])
  call ol_merge_tensors(G0tensor(33),[G0tensor(28),G0tensor(23)])
call TI_bubble_red(G1tensor(6),9,mass2set(:,1),G0tensor(1),M2L1R1,G0tensor(10))
call TI_bubble_red(G1tensor(1),10,mass2set(:,2),G0tensor(11),M2L1R1,G0tensor(16))
  call ol_merge_tensors(G0tensor(16),[G0tensor(10),G0tensor(35)])
  call ol_merge_tensors(T0sum(10),[G0tensor(3)])
  call ol_merge_tensors(T0sum(11),[G0tensor(22)])
  call ol_merge_tensors(T0sum(12),[G0tensor(27)])
  call ol_merge_tensors(T0sum(13),[G0tensor(16)])
  call ol_merge_tensors(T0sum(14),[G0tensor(24)])
  call ol_merge_tensors(T0sum(15),[G0tensor(29)])
  call ol_merge_tensors(T0sum(16),[G0tensor(17)])
  call ol_merge_tensors(T0sum(17),[G0tensor(32)])
  call ol_merge_tensors(T0sum(18),[G0tensor(1)])
  call ol_merge_tensors(T0sum(19),[G0tensor(33)])
  call ol_merge_tensors(T0sum(20),[G0tensor(11)])
! end of process

! end do

end subroutine vamp_1

end module ol_vamp_1_eea_qed_nfl1_est3_nofl_eexaa_1_/**/REALKIND
