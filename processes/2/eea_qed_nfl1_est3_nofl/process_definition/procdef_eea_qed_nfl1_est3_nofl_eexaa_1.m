
FeynArtsProcess = {F[2, {1}], -F[2, {1}]} -> {V[1], V[1]};

SortExternal = True;

OpenLoopsModel = "SM";

CreateTopologiesOptions = {
  ExcludeTopologies -> {Snails, WFCorrectionCTs, TadpoleCTs},
  Adjacencies -> {3, 4}
};

InsertFieldsOptions = {
  Model -> {"SMQCD", "SMQCDR2"},
  GenericModel -> "Lorentz",
  InsertionLevel -> {Particles},
  Restrictions -> {ExcludeParticles -> {F[3, __], F[4, __], F[2, {3}], F[2, {2}], F[1, {3}], F[1, {2}], V[2 | 3 | 4], U[1 | 2 | 3 | 4], S[1 | 2 | 3]}, NoQuarkMixing}
};

UnitaryGauge = True;

ColourCorrelations = Automatic;

OTFColourCorrelations = Automatic;

SpinCorrelatedHardFactor = Automatic;

SubProcessName = Automatic;

SelectCoupling = MemberQ[{0}, Exponent[#1, gQCD]] & ;

SelectInterference = {
  gQCD -> {0}
};

SelectTreeDiagrams = True & ;

SelectLoopDiagrams =  !(ContainsFermionLoop[##1] && NLegsOnLoop[2][##1]) && ParticlesInLoop[V[1]][##1] & ;

SelectCTDiagrams = True & ;

ReplaceOSw = False;

SetParameters = {
  nf -> 0,
  nfl -> 1,
  CKMORDER -> 0,
  nc -> 3,
  MU -> 0,
  MD -> 0,
  MS -> 0,
  MC -> 0,
  LeadingColour -> 0,
  POLSEL -> 1
};

ChannelMap = {};

Approximation = "nofermionloops";

QED = 2;

ForceLoops = Automatic;

ForceLoopsInclude = Automatic;

NonZeroHels = Null;

OnTheFlyMode = 3;

noQCD = False;

noEW = False;

SetExternalSubtrees = {
  3
};
