SelectInterference = {qQCD -> 0};

UnitaryGauge = False;
noQCD = True;

(* ew (QED=0) or pure qed (QED=2) corrections, no tau*)
(* ew includes photon self-energies *)
(* qed excludes photon self-energies *)
(* other fermion loop diagrams are included, e.g. light-by-light boxes *)

lepew02 = {
  QED -> 0,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}]
};

lepqed02 = {
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 2}]
};


lepew01 = {
  QED -> 0,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};

lepqed01 = {
  QED -> 2,
  SetParameters -> JoinOptions[{MM -> MM, ME -> ME, nf -> 0, nfl -> 1}]
};


(* e and mu in loop *)

(* mumuxeex *)    AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}]}, Sequence @@ lepew02];
(* mumuxeexa *)   AddProcess[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1]}, Sequence @@ lepew02];
(* mumuxeexaa *)  AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1]}, Sequence @@ lepew02];
(*
(* mumuxeexaaa *) AddReal[FeynArtsProcess -> {F[2,{2}], -F[2,{2}]} -> {F[2,{1}], -F[2,{1}], V[1], V[1], V[1]}, Sequence @@ lepew02];
*)
