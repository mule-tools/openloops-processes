import re
import requests
import tarfile
import time
import os
import subprocess
import hashlib


root = "https://www.physik.uzh.ch/data/openloops/repositories/McMule/"


def stage(file):
    print(f"[GIT]     -> staging {file}")
    subprocess.Popen([
        'git', 'add', file
    ]).wait()


def commit(date, msg):
    date = time.strftime("%Y-%m-%dT%H:%M:%S", date)
    env = os.environ
    env['GIT_AUTHOR_DATE'] = date
    env['GIT_COMMITTER_DATE'] = date
    env['GIT_AUTHOR_NAME'] = "McMule Bot"
    env['GIT_AUTHOR_EMAIL'] = "bot@mule-tools.gitlab.io"
    subprocess.Popen(
        ['git', 'commit', '-m', msg],
        env=env
    ).wait()
    print(f"[GIT]  -> commiting `{msg}'")


def rm(file):
    print("[GIT]     -> removing", file)
    subprocess.Popen([
        'git', 'rm', '-r', file
    ]).wait()


def download_and_stage(name, date, hash, process_api_version=2, msg=None):
    with open('processes/hashes', 'r') as fp:
        if hash + '\n' in fp.readlines():
            print(f"[SKIP] hash {hash} already known")
            return

    with open('processes/hashes', 'a') as fp:
        fp.write(hash + '\n')

    t = time.strftime("%Y-%m-%d-%H-%M-%S", date)
    process_filename = f'processes/{process_api_version}/{name}'

    updateQ = os.path.isdir(process_filename)

    if os.path.isdir(process_filename):
        rm(process_filename)

    print(f"[GET] Fetching {name} ({t}, hash={hash})")

    filename = f'deprecated/{process_api_version}_{name}_{t}_{hash}'
    r = requests.get(f'{root}{filename}.tar.gz', stream=True)
    if not r.ok:
        filename = f'processes/{process_api_version}/{name}'
        r = requests.get(f'{root}{filename}.tar.gz', stream=True)

    tar = tarfile.open(fileobj=r.raw, mode="r|gz")
    tar.extractall(f'processes/{process_api_version}')

    r = requests.get(f'{root}{filename}.m')
    with open(f'{process_filename}.m', 'wb') as fp:
        fp.write(r.content)

    stage(f'{process_filename}.m')
    stage(process_filename)
    stage('processes/hashes')

    return updateQ


def list_processes(api=2):
    r = requests.get(f'{root}/processes/{api}/')
    matches = re.findall(
        r'<a href="([a-z_\d]*)\.m">[a-z_\d]*\.m</a> *([\d-]* \d\d:\d\d)',
        r.text
    )
    for name, date in matches:
        r = requests.get(f'{root}/processes/{api}/{name}.tar.gz')
        yield (
            name, time.strptime(date, "%Y-%m-%d %H:%M"),
            hashlib.md5(r.content).hexdigest(),
            api
        )


def list_deprecated():
    r = requests.get(f'{root}/deprecated/')
    matches = re.findall(
        r'<a href="(\d)_([a-z_]*)_'
        r'(\d\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d)'
        r'_([a-f\d]*)\.m',
        r.text
    )
    for process_api_version, name, date, hash in matches:
        yield (
            name,
            time.strptime(date, "%Y-%m-%d-%H-%M-%S"),
            hash,
            int(process_api_version)
        )


def delta_time(a, b):
    return time.mktime(b) - time.mktime(a)


def main(dt=30., full=False):
    ls = list(list_deprecated())
    if full:
        hashes = [h for n, d, h, v in ls]
        ls += [
            (n, d, h, v)
            for n, d, h, v in list_processes()
            if h not in hashes
        ]

    ls = sorted(ls, key=lambda a: a[1])
    groups = []
    for args in ls:
        if len(groups) == 0:
            groups.append([args])
            continue

        if delta_time(groups[-1][-1][1], args[1]) < dt:
            groups[-1].append(args)
        else:
            groups.append([args])

    for group in groups:
        names = []
        for args in group:
            names.append(args[0])
            updatedQ = download_and_stage(*args)

        if updatedQ is None:
            continue

        if updatedQ:
            msg = "Updated " + ", ".join(names)
        else:
            msg = "Added " + ", ".join(names)

        commit(group[-1][1], msg)


main(full=True)
